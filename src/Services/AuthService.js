import {
    get
} from '../NetworkLayer/Index';
import {URL} from '../Utils/Config';

// let user = JSON.parse(localStorage.getItem('user'));
// const initialState = user ? {
//     loggedIn: true,
//     user
// } : {};

export default class PropertiesService {
    static login(param: {
        login: number,
        password: number,
        type: string
    }): Promise < {
        success: boolean,
        token: string
    } > {
        return get(URL.user.login, param)
            .then((res: any) => {
                const {
                    result,
                    token,
                    msg,
                    id
                } = res.data;
                const response = {
                    success: false,
                    error: msg,
                    token: null
                };
                if (result === "OK") {
                    localStorage.setItem('token', token);
                    localStorage.setItem('id', param.login.toString(10));
                    localStorage.setItem('user_id', id);
                    response.success = true;
                    response.token = token;
                    delete response.error;
                }
                return response;
            })
            .catch(error => {
                console.error(error);
            });
    }
}
