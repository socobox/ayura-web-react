import {
  get,
  post
} from '../NetworkLayer/Index';

export default class PropertiesService {
  static loadProperties(params: any) {
    params.size = 6;
    return get('/property/search', params)
      .then(res => {
        return {
          list: res.data.properties,
          pages: res.data.total,
          params: params
        };
      })
      .catch(error => {
        console.error(error);
      });
  }

  static getProperty(id: number) {
    return get('/property/search', {
        code: id
      })
      .then(res => {
        return get('/property/photos', {
          property: id
        }).then(data => {
          const {
            properties
          } = res.data;
          properties[0].photos = data.data.keys;
          return properties[0]
        }).catch(error => {
          console.error(error);
        })
      }).catch(error => {
        console.error(error)
      });
  }
  static loadPropertiesTypes() {
    return get('/property/type/options?')
      .then(res => {
        return res.data.options;
      })
      .catch(error => {
        console.error(error);
      });
  }
  static loadPropertiesRentPrices() {
    return get('/property/rent/price/options?')
      .then(res => {
        return res.data.options;

      })
      .catch(error => {
        console.error(error);
      });
  }
  static loadPropertiesSellPrices() {
    return get('/property/sell/price/options?')
      .then(res => {
        return res.data.options;

      })
      .catch(error => {
        console.error(error);
      });
  }
  static loadPropertiesAreas() {
    return get('/property/area/options?')
      .then(res => {
        return res.data.options;

      })
      .catch(error => {
        console.error(error);
      });
  }

  static registerProperty(registerData) {
    return post('/property/register', registerData).then(res => {
      if (res.result === "OK") {
        return {
          success: true,
          res: {
            msg: 'Formulario enviado con exito !!!'
          }
        }
      }
      if (res.result === "ERROR") {
        return {
          success: true,
          res: {
            msg: res.msg
          }
        }
      }
    })
  }
}