// @flow
import {get, post} from '../NetworkLayer/Index';
import {URL} from '../Utils/Config';

    export default class SharedService {
        static sendContactUs(params: any) {
            return get(URL.contactRequest.add, params)
                .then(res => {
                    const {
                        data
                    } = res;
                    if (data.result === "OK") {
                        return {
                            success: true,
                            res: {
                                msg: "Su solicitud ha sido enviada, prontamente lo estaremos contactando."
                            }
                        }
                    } else {
                        return {
                            success: false,
                            res: {
                                msg: data.msg
                            }
                        }
                    }
                })
                .catch(error => {
                    console.error(error);
                });
        }
        static sendInterestAlert(params: any) {
            return get(URL.contactRequest.add, params)
                .then(res => {
                    const {
                        data
                    } = res;
                    if (data.result === "OK") {
                        return {
                            success: true,
                            res: {
                                msg: "Su solicitud ha sido enviada, prontamente lo estaremos contactando."
                            }
                        }
                    } else {
                        return {
                            success: false,
                            res: {
                                msg: data.msg
                            }
                        }
                    }
                })
                .catch(error => {
                    console.error(error);
                });
        }
        static sendPaymentInfo(params: any) {
            return post(URL.payment, params)
                .then(res => {
                    const {
                        data
                    } = res;
                    if (data.result === "OK") {
                        return {
                            success: true,
                            res: {
                                msg: "Su solicitud ha sido enviada, prontamente lo estaremos contactando."
                            }
                        }
                    } else {
                        return {
                            success: false,
                            res: {
                                msg: data.msg
                            }
                        }
                    }
                })
                .catch(error => {
                    console.error(error);
                });
        }
    }
