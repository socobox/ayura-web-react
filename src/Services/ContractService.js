import {
    get
} from '../NetworkLayer/Index';

// let user = JSON.parse(localStorage.getItem('user'));
// const initialState = user ? {
//     loggedIn: true,
//     user
// } : {};

export default class ContractService {
    static getContracts(token: ? string, id : ? string, owner ? : boolean = false, size ? : number = 10): Promise < Array < any >> {
        const params = {
            token,
            size: 10
        };
        if(owner) {
          params.ownerDocFilter = id;
        } else {
          params.documentFilter = id;
        }

        return get('/tenant/contracts/search', params).then(data => {
            const {
                result,
                contracts,
            } = data.data;
            if (result === "OK") {

                return contracts;

            } else {

                return []

            }
        }).catch(error => {
            console.error(error)
        });
    }

    static getDocs(token: ? string, id : string, docTypeId: number = 0, page: number) {
      const params = {
        ENTITY_ID: id,
        FOLDER_ID: 2,
        token: token,
        size: 30
      };
      if (docTypeId) {
        params.DOC_TYPE_ID = docTypeId
      }
      if (page) {
        params.page = page;
      }
        return get('/content/search', params).then(res => {
            const {
                result,
                items,
                totalPages
            } = res.data;
            if (result === 'OK') {
                return {items, totalPages};
            } else {
                return [];
            }
        });
    }
}