import {
  get, post
} from '../NetworkLayer/Index';
import {URL} from '../Utils/Config';

export default class RequestsService{
  static getInfo(token: ? string, id: string, reqId: number){
    const params = {
      token,
      docFilter: id,
      requestFilter: reqId
    };
    return get(URL.propertyRequest.list, params).then(data => {
      const {
        result,
        requests,
      } = data.data;
      if(result === "OK"){

        return requests[0];

      } else{

        return []

      }
    }).catch(error => {
      console.error(error)
    });
  }

  static getRequestDocs(token: ? string, id: string, reqId: number, docTypeId: number = 0, size: number = 21, page: number = 1){
    const params = {
      token,
      ENTITY_ID: reqId,
      FOLDER_ID: 1,
      size,
      page
    };
    if(docTypeId){
      params.DOC_TYPE_ID = docTypeId
    }
    return get(URL.content.search, params).then(res => {
      const {
        result,
        items,
      } = res.data;
      if(result === "OK"){

        return items;

      } else{

        return []

      }
    }).catch(error => {
      console.error(error)
    });
  }

  static getRequestComments(reqId: number){
    const params = {
      request: reqId
    };
    return get(URL.propertyRequest.comment.list, params).then(data => {
      const {
        result,
        updates,
      } = data.data;
      if(result === "OK"){

        return updates;

      } else{

        return []

      }
    }).catch(error => {
      console.error(error)
    });
  }

  static addRequestComment(token: string, reqId: number, comment: string) {
    const params = {
      token,
      request: reqId,
      comment
    };
    return get(URL.propertyRequest.comment.add, params).then(data => {
      const {
        result,
        requests,
      } = data.data;
      if(result === "OK"){

        return requests[0];

      } else{

        return []

      }
    }).catch(error => {
      console.error(error)
    });
  }

  static uploadFile(params){
    return post(URL.content.upload, params).then(data => {
      const {
        result,
        requests,
      } = data.data;
      if(result === "OK"){

        return requests[0];

      } else{

        return []

      }
    }).catch(error => {
      console.error(error)
    });
  }

  static sendRepairRequest(params) {
    const subject = "Solicitud de reparacion para contrato #" + params.contract;
    const html = `<body> 
      <br>
      <br> 
      Nombre: ${params.name}
      <br>
      Contrato: ${params.contract}
      <br>
      Correo Electronico: ${params.email}
      <br>
      Telefono: ${params.phone}
      <br>
      ${params.cellphone ? "Celular: " + params.cellphone + "<br>" : ""  }
      ${params.availability ? "Horario de disponibilidad: " + params.availability + "<br>" : ""}
      ${params.description ? "Detalles del daño: " + params.description + "<br>" : ""}
      <br><br>
      </body>`;
    return post(URL.repair, {html, subject, files: params.files}).then(res => {
      if (res.result === "OK") {
        return {
          success: true,
          res: {
            msg: "Su solicitud ha sido enviada, prontamente lo estaremos contactando."
          }
        }
      } else {
        return {
          success: false,
        }
      }
    })
        .catch(error => {
          console.error(error);
        });

  }
}
