import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import CodeIcon from '../../Assets/Images/code-icon.png';
import BathIcon from '../../Assets/Images/bathroom-icon.png';
import AreaIcon from '../../Assets/Images/area-icon.png';
import RoomIcon from '../../Assets/Images/room-icon.png';
import PlaceholderImage from '../../Assets/Images/noDisponible.jpg';
import { imageUrl } from '../../Utils/Config';
import { formatPrice } from '../../Utils/Utilities';

type Props = {
    property: Object
};
class PropertyCard extends Component<Props, {}> {
    render() {
        const { property } = this.props;
        if(property.COVER){
        property.COVER = property.COVER.replace('_s', '_l');
        }
        return <Link style={{ textDecoration: 'none' }} to={{ pathname: '/property', search: '?id=' + property.ID }}>
                <div className="card card-property clicka">
                    <div className="card-property-image-container">
                        <div className="cart-property-image d-flex align-items-end" style={{ width: '100%', height: '100%', backgroundImage: `url(${imageUrl}${property.COVER}), url(${PlaceholderImage})`, backgroundSize: 'cover', backgroundRepeat: 'no-repeat' }} >

                        <div className="card-property-name">
                            {property.NEIGHBORHOOD} - {property.CITY}
                        </div>
                        </div>
                    </div>
                    <div className="card-body card-property-body">
                        <h3 className="text-primary mb-2">
                            <b>
                                ${formatPrice(property.PRICE)}
                            </b>
                        </h3>
                        <div className="container-flush">
                            <div className="row">
                                <div className="col-6">
                                    <img className="mr-1" src={CodeIcon} alt="" />
                                    <span className="">
                                        <b>Código:</b> #{property.ID}
                                    </span>
                                </div>
                                <div className="col-6">
                                    <img className="mr-1" src={RoomIcon} alt="" />
                                    <span className="">
                                        <b>Habitaciones:</b>
                                        {property['N. Alcobas']}
                                    </span>
                                </div>
                                <div className="col-6">
                                    <img className="mr-1" src={AreaIcon} alt="" />
                                    <span className="">
                                        <b>Área:</b> {property.AREA}m<sup>
                                            2
                                        </sup>
                                    </span>
                                </div>
                                <div className="col-6">
                                    <img className="mr-1" src={BathIcon} alt="" />
                                    <span className="">
                                        <b>Baños:</b>
                                        {property['Baño Cabina'] || 1}
                                    </span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Link>;
    }
}

export default PropertyCard;
