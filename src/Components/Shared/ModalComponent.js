import React, {Component} from 'react';
import {Modal, ModalBody, ModalFooter, ModalHeader} from 'reactstrap';

class ContactMeModalComponent extends Component <{}, {}>{

  render(){
    const {header, body, onClose, footer} = this.props;

    return (
        <div>
          <Modal isOpen = {true} toggle = {onClose}>
            <ModalHeader toggle = {onClose}> {header}</ModalHeader>
            <ModalBody>
              {body}
            </ModalBody>
            <ModalFooter>
              {footer}
            </ModalFooter>
          </Modal>
        </div>
    );
  }

}

export default ContactMeModalComponent;
