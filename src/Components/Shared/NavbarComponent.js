import React, {Component, Fragment} from 'react';
import {
  Navbar,
  NavbarBrand,
  NavbarToggler,
  Collapse,
  Nav,
  NavItem,
  NavLink
} from 'reactstrap';
import {NavLink as RRNavLink, Link} from 'react-router-dom';
import SocialMediaIcons from './SocialMediaIconsComponent';
import Logo from '../../Assets/Images/ayura-logo@2x.png';
import HomeIcon from '../../Assets/Images/home-white.png';
import RegisterIcon from '../../Assets/Images/inmueble.png';
import LoginIcon from '../../Assets/Images/login.png';
import heart from "../../Assets/Images/heart-icon@2x.png";

type State = {
  isOpen: boolean
};

class NavbarComponent extends Component<{}, State>{
  state = {
    isOpen: false
  };

  toggle = () => {
    this.setState(prev => ({
      isOpen: !prev.isOpen
    }));
  };

  render(){
    return <Fragment>
      <Navbar color = "dark" dark fixed = "top" expand = "sm">
        <Nav className = "ml-auto flex-row" navbar>
          <NavItem className = "mr-2 mr-md-0">
            <NavLink className = "d-flex align-items-center justify-content-center" to = "/registrar_inmueble"
                     activeClassName = "active" tag = {RRNavLink}>
              <img className = "mr-2" width="18px" height="17px" src = {RegisterIcon} alt = "..."/>
              <span>Registrar Inmueble</span>
            </NavLink>
          </NavItem>
          <NavItem className = "mr-2 mr-md-0">
            <NavLink className = "d-flex align-items-center justify-content-center" to = "/consultar_solicitud"
                     activeClassName = "active" tag = {RRNavLink}>
              <img className = "mr-2" src = {HomeIcon} alt = "..."/>
              <span>Consultar Solicitud</span>
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink className = "d-flex align-items-center justify-content-center" to = "/login_clients"
                     activeClassName = "active" tag = {RRNavLink}>
              <img className = "mr-2" src = {LoginIcon} alt = "..."/>
              <span>Ingreso clientes</span>
            </NavLink>
          </NavItem>
        </Nav>
      </Navbar>

      <Navbar className = "ayura-main-navbar" dark fixed = "top" expand = "md">
        <NavbarBrand tag = {Link} to = "/">
          <div className = "pt-md-5">
            <img src = {Logo} className = "ayura-logo" alt = "ayura logo"/>
            <span className="text-ayu-size-md ayura-slogan">
              Estamos en el <img height = '22px' src = {heart} alt = "Corazón"/> de Envigado
            </span>
          </div>
        </NavbarBrand>
        <NavbarToggler onClick = {this.toggle}/>
        <Collapse isOpen = {this.state.isOpen} className = "bg-primary" navbar>
          <Nav className = "ml-auto d-flex align-items-center" navbar>
            <NavItem className="text-ayu-size-md">
              <NavLink to = "/contactanos" activeClassName = "active" tag = {RRNavLink}>
                Contáctanos
              </NavLink>
            </NavItem>
            <NavItem className="text-ayu-size-md">
              <NavLink to = "/servicios" activeClassName = "active" tag = {RRNavLink}>
                Servicios
              </NavLink>
            </NavItem>
            <NavItem className="text-ayu-size-md">
              <NavLink to = "/requisitos" activeClassName = "active" tag = {RRNavLink}>
                Requisitos
              </NavLink>
            </NavItem>
            <NavItem className="text-ayu-size-md">
              <NavLink to = "/sobre_ayura" activeClassName = "active" tag = {RRNavLink}>
                Sobre Ayurá
              </NavLink>
            </NavItem>
            <NavItem className="text-ayu-size-md">
              <NavLink to = "/valores" activeClassName = "active" tag = {RRNavLink}>
                Nuestros Valores
              </NavLink>
            </NavItem>
            <NavItem className = "my-3 my-md-0">
              <SocialMediaIcons/>
            </NavItem>
          </Nav>
        </Collapse>
      </Navbar>
    </Fragment>;
  }
}

export default NavbarComponent;
