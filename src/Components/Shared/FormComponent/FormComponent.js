import React, {Component} from 'react';
import './FormComponent.scss';
import {toast} from '../../../Services/AlertService';

type Props = {
  logo: string,
  title: string,
  click: any
};
type State = {
  id: number,
  password: number
};

class FormComponent extends Component<Props, State> {
  state = {
    id: null,
    password: null
  };
  handleChange = (event: SyntheticEvent<HTMLButtonElement>) => {
    const {value, name} = event.currentTarget;
    this.setState({[name]: value});
  };
  
  handleSubmit = (event: SyntheticEvent<HTMLButtonElement>) => {
    event.preventDefault();
    const {id, password} = this.state;
    if((!id || !password) || (id.length === 0 || password === 0)){
      toast('Por favor llene los campos', 'error');
      return;
    }
    this.props.click(id, password);
  };
  
  render() {
    return (
      <div className="card card-login">
        <div className="card-body">
          <div className="d-flex mb-3 ">
            <img
              width="33"
              src={this.props.logo}
              alt="..."
              className="mr-2"
            />
            <h3>{this.props.title}</h3>
          </div>
          
          <form onSubmit={this.handleSubmit}>
            <div className="form-group">
              <input
                type="number"
                name="id"
                className="form-control"
                aria-describedby="emailHelp"
                placeholder="Cédula"
                onChange={this.handleChange}
              />
            </div>
            <div className="form-group">
              <input
                type="password"
                name="password"
                onChange={this.handleChange}
                className="form-control"
                placeholder="Contraseña"
              />
            </div>
            <button
              type="submit"
              className="btn btn-secondary mb-3">
              Entrar
            </button>
          </form>
        </div>
      </div>
    );
  }
}

export default FormComponent;
