import React, {Component, Fragment} from 'react';
import './Documents.scss'
import Dropzone from "react-dropzone";
import {format} from 'date-fns'
import {Pagination, PaginationItem, PaginationLink} from "reactstrap";

type state = {
  tab: string,
  message: string,
  files: Array<any>
}

class Documents extends Component<{}, state>{

  state = {
    tab: 'docs',
    message: '',
    files: [],
  };
  docIds = this.props.owner ? [18, 19, 20, 21, 23, 24, 30, 44, 22, 130] : [2, 19, 20, 23, 30, 41, 42, 125, 22, 130];


  onDrop(files){
    this.setState({files});
  }

  sendComment(){
    this.props.sendComment(this.state.message);
    this.setState({...this.state, message: ''});
  }

  uploadFile(){
    this.props.uploadFile(this.state.files[0],);
    this.setState({...this.state, files: []});
  }

  onCancel(){
    this.setState({
      files: []
    });
  }

  handleChange = event => {
    const {value, name} = event.target;
    this.setState({[name]: value});
  };

  toggleTab(tab){
    if(this.state.tab !== tab){
      this.setState({...this.state, tab});
    }
  }

  render(){
    const files = this.state.files.map(file => (
        <li key = {file.name}>
          {file.name} - {file.size} bytes
        </li>
    ));
    const {tab, message} = this.state;
    const {contract, request, mainLoad, loadRoot, folder, documents,
      openDoc, pages, comments, currentPage, nextPage} = this.props;
    return (
        <div className = "container py-4">
          <div className = "row">
            <div onClick = {() => this.toggleTab('docs')}
                 className = {`col-6 col-md-4 doc-container-header pt-2 clickable ${tab === 'docs' ? "tab-active" : ""}`}>
              <span className = "text-white">Documentos ({contract})</span>
            </div>
            {request && (
                <div onClick = {() => this.toggleTab('msg')}
                     className = {`col-4 doc-container-header pt-2 ml-2 clickable ${tab === 'msg' ? "tab-active" : ""}`}>
                  <span className = "text-white">Mensajes</span>
                </div>)}
          </div>
          <div className = "row">
            <div className = "container doc-container">
              {tab === 'docs' && (
                  <Fragment>
                    {!mainLoad && (
                        <div className = "row">
                          <div className = "col-12 py-1 text-center bg-secondary text-white doc-clickable"
                               onClick = {() => loadRoot()}> {folder} <i
                              className = "fa fa-level-up fa-lg"/></div>
                        </div>)}
                    {!mainLoad && request && (
                        <Fragment>
                          <div className = "row mt-3">
                            <div className = "col-1"/>
                            < Dropzone onDrop = {this.onDrop.bind(this)}
                                       onFileDialogCancel = {this.onCancel.bind(this)}>
                              {({getRootProps, getInputProps}) => (
                                  <div {...getRootProps()} className = "col-10 doc-uploader">
                                    <input {...getInputProps()} />
                                    {this.state.files.length ?
                                        files :
                                        (<span>Arrastre archivos nuevos aquí, o presione click para seleccionarlos</span>)

                                    }
                                  </div>
                              )}
                            </Dropzone>
                          </div>
                          {this.state.files.length > 0 && (
                              <div className = "row">
                                <div className = "col-11 pt-2 d-flex justify-content-end">
                                  <button className = "btn btn-sm btn-secondary"
                                          onClick = {() => this.uploadFile()}>
                                    Subir
                                  </button>
                                </div>
                              </div>)}

                        </Fragment>
                    )}
                    <div className = "row text-center">
                      {documents.length ?
                          documents.map(i => (
                              (this.docIds.includes(i.DOC_TYPE_ID) || request) ?
                                  <div key = {i.ID || i.DOC_TYPE_ID} className = "col-md-3 my-4 doc-clickable">
                              <span onClick = {() => openDoc(i)}>
                                <i className = {`fa fa-2x d-flex justify-content-center ${i.ICON_TYPE === "Folder" ? "fa-folder" : "fa-file-text-o"}`}/>
                                <span className = "text-primary">{i.FOLDER_NAME.toLowerCase()}</span>
                              </span>
                                  </div> : null)) :
                          <div className = "col-12 my-4"> No se han entrado Documentos Existentes</div>
                      }
                    </div>
                    {!mainLoad && !!documents.length && (
                    <div className="row d-flex justify-content-center">
                      <Pagination
                          aria-label = "Page navigation example"
                          className = "d-flex justify-content-center">
                        <PaginationItem>
                          <PaginationLink
                              className = "border-0 bg-transparent"
                              onClick = {() => nextPage(-1)}>
                            <i className = "fa fa-arrow-left"/>
                          </PaginationLink>
                        </PaginationItem>
                        <div className = "d-flex justify-content-center align-items-center">
                          {currentPage} de {pages}
                        </div>
                        <PaginationItem>
                          <PaginationLink
                              className = "border-0 bg-transparent"
                              onClick = {() => nextPage(1)}

                          >
                            <i className = "fa fa-arrow-right"/>
                          </PaginationLink>
                        </PaginationItem>
                      </Pagination>
                    </div>
                    )}
                  </Fragment>)}
              {tab === 'msg' && (
                  <div className = "row">
                    <div className = "col-6">
                      <ul className = "list-unstyled comment-list p-3">
                        {comments.map(i => (
                            <li key = {i.id}>
                              [{format(i.created, 'DD/MM/YYYY hh:mm A')}] {i.name} Escribió:<br/>
                              {i.comment}
                            </li>
                        ))}
                      </ul>
                    </div>
                    <div className = "col-6">
                      <label className = "text-ayu-bold">
                        Enviar Mensaje:
                      </label>
                      <textarea name = "message"
                                value = {message}
                                onChange = {this.handleChange}
                                className = "form-control mb-3 no-resize"
                                placeholder = "Mensaje" rows = "3"/>
                      <div className = "d-flex justify-content-end">
                        <button className = "btn btn-secondary send-button"
                                onClick = {() => this.sendComment()}>
                          Enviar
                        </button>
                      </div>
                    </div>
                  </div>
              )}
            </div>
          </div>
        </div>
    );
  }
}

export default Documents;