import React, {Component} from 'react';

class FAB extends Component <{}, {}> {
  
  showZendesk(): void {
    window.$zopim.livechat.window.show()
  }
  
  
  render() {
    
    return <div onClick={this.showZendesk} className="fab clickable"/>;
  }
}

export default FAB;
