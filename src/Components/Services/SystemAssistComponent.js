import React, {Component} from 'react';
import MainImage from '../../Assets/Images/DSC_3412.jpg';

class SystemAssist extends Component <{},{}>{
  render(){
    return (
        <div className="container-fluid">
          <div className="row">
            <div className="col-12 col-lg-6">
              <ul>
                <li className="text-justify">Contamos con un excelente servicio al momento de ingresar y promocionar sus inmuebles en los diferentes medios de comunicación como pagina web, Facebook, twitter, medio impreso y asesoría personalizada dentro de la agencia.</li>
                <li className="text-justify">contamos con los equipos adecuados para el buen rendimiento de la empresa y mayor agilidad para atender nuestros clientes.</li>
              </ul>
            </div>
            <div className="col-12 col-lg-6">
              <img className = "services-image img-fluid mb-5" src = {MainImage} alt = "not found"/>
            </div>
          </div>
        </div>
    );
  }
}

export default SystemAssist;