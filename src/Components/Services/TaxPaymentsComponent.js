import React, {Component, Fragment} from 'react';
import MainImage from '../../Assets/Images/DSC_3462.jpg';

class TaxPayments extends Component <{},{}>{
  render(){
    return (
        <Fragment>
          <img className = "services-image img-fluid mb-5" src = {MainImage} alt = "not found"/>
          <span className = "col-sm">
            <p>
              Nos encargamos del pago de prediales durante el contrato que se tenga con su inmueble.
            </p>
            <p>
              Contamos con personal capacitado en el campo para asesorarlo en todo lo que usted necesite referente a su inmueble.
            </p>
          </span>


        </Fragment>
    );
  }
}

export default TaxPayments;
