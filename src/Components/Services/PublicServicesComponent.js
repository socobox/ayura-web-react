import React, {Component, Fragment} from 'react';
import MainImage from '../../Assets/Images/DSC_3396.jpg';

class PublicServices extends Component <{},{}>{
  render(){
    return (
        <Fragment>
          <img className = "services-image img-fluid mb-5" src = {MainImage} alt = "not found"/>
          <span className = "col-sm">
            <p>
              Nos encargamos del pago de servicios públicos y los faltantes causados por inicio o terminación de contrato en su inmueble, por los descuentos generados por seguros, asistencias, financiaciones, reconexiones etc...
            </p>
              <p>
                Contamos con personal capacitado en el campo para asesorarlo en todo lo que usted necesite referente a su inmueble.
              </p>
          </span>


        </Fragment>
    );
  }
}

export default PublicServices;
