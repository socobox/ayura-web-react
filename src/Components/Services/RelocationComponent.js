import React, {Component} from 'react';
import MainImage from '../../Assets/Images/DSC_3525.jpg';

class Relocation extends Component <{},{}>{
  render(){
    return (
        <div className="container-fluid">
          <div className="row">
            <div className="col-12  col-lg-6">
              <ul>
                <li className="text-justify">Atender en forma oportuna y correcta los clientes que desean ser reubicados por medio de la empresa, buscando suplir cada una de sus necesidades en el menor tiempo posible.</li>
                <li className="text-justify">Asesora Ana María Molina tel: <a href = "tel:4+2701463">2701463</a></li>
              </ul>
            </div>
            <div className="col-12 col-lg-6">
              <img className = "services-image img-fluid mb-5" src = {MainImage} alt = "not found"/>
            </div>
          </div>
        </div>
    );
  }
}

export default Relocation;