import React, {Component, Fragment} from 'react';
import MainImage from '../../Assets/Images/DSC_3545.jpg';

class Leasing extends Component <{},{}>{
  render(){
    return (
        <Fragment>
          <img className = "services-image img-fluid mb-5" src = {MainImage} alt = "not found"/>
          <span className = "col-sm">
            <p className="text-ayu-size-sm">
              <b>Arrendamos sus inmuebles en tiempo record</b>, realizamos el pago de servicios públicos,
              impuestos, obligaciones y administración.
            </p>
            <p className="text-ayu-size-sm">
              <b>Contamos con el mejor equipo humano</b> especializado en la promoción de inmuebles, donde
              establecemos los planes de comercialización.
            </p>
            <p className="text-ayu-size-sm">
              <b>Velamos por la buena conservación y prevención</b> de los inmuebles realizando inventario
              antes y después de arrendarlos.
            </p>
            <p className="text-ayu-size-sm">
              <b>Además en nuestro sitio web promocionamos sus inmuebles</b> con toma fotográfica.
              Consígnenos ya su inmueble, estaremos orgullosos de servirle.
            </p>
          </span>
        </Fragment>
    );
  }
}

export default Leasing;
