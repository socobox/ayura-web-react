import React, { Component, type Node } from 'react';

class TenantErrorBoundaryComponent extends Component<
    { children: Node },
    { hasError: boolean, error: any, info: any }
> {

        state = {
            hasError: false,
            error: null,
            info: null
        };
    

    componentDidCatch(error: any, info: any) {
        this.setState({
            hasError: true,
            error: error,
            info: info
        });
    }

    render() {
        if (this.state.hasError) {
            return (
                <div>
                    <h1>Oops!!! Something went wrong</h1>
                    <p><span>The error: </span> {this.state.error.toString()}</p>
                </div>
            );
        } else {
            return this.props.children;
        }
    }
}

export default TenantErrorBoundaryComponent;
