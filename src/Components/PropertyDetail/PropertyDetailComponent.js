// @flow
import React, {Component, Fragment} from 'react';
import PropertyService from '../../Services/PropertiesService';
import queryString from 'query-string';
import {List} from 'react-content-loader';
import {imageUrl} from '../../Utils/Config';
import Lightbox from 'react-image-lightbox';
import hotWater from '../../Assets/Images/icons-8-room-sound@3x.png';
import backyard from '../../Assets/Images/patios@3x.png';
import sauna from '../../Assets/Images/sauna@3x.png';
import library from '../../Assets/Images/biblioteca@3x.png';
import dining from '../../Assets/Images/comedor.png'
import phone from '../../Assets/Images/icons8-phone.png'
import elevator from '../../Assets/Images/ascensor@3x.png';
import indoorParking from '../../Assets/Images/icons-8-indoor-parking@3x.png';
import level from '../../Assets/Images/icons-8-stairs-up@3x.png';
import gate from '../../Assets/Images/porteria@3x.png';
import frontyard from '../../Assets/Images/terraza@3x.png';
import sports from '../../Assets/Images/zona-deportiva@3x.png';
import bathCabin from '../../Assets/Images/icons-8-bidet@3x.png';
import hall from '../../Assets/Images/hall@3x.png';
import tv from '../../Assets/Images/hd-tv@3x.png';
import closedUnit from '../../Assets/Images/unidad-cerrada@3x.png';
import pool from '../../Assets/Images/piscina@3x.png';
import salon from '../../Assets/Images/salon@3x.png';
import park from '../../Assets/Images/juegos@3x.png';
import vestier from '../../Assets/Images/vestier@3x.png';
import bath from '../../Assets/Images/bathroom-icon@3x.png';
import room from '../../Assets/Images/room-icon@3x.png';
import bidet from '../../Assets/Images/icons-8-bidet@3x.png';
import balcony from '../../Assets/Images/icons-8-balcony@3x.png';
import closet from '../../Assets/Images/icons-8-closet@3x.png';
import coniferous from '../../Assets/Images/icons-8-coniferous-tree@3x.png';
import cushion from '../../Assets/Images/icons-8-cushion@3x.png';
import fridge from '../../Assets/Images/icons-8-fridge@3x.png';
import gas from '../../Assets/Images/icons-8-gas-industry@3x.png';
import living from '../../Assets/Images/icons-8-living-room@3x.png';

import stairs from '../../Assets/Images/icons-8-stairs-up@3x.png';
import {formatPrice} from '../../Utils/Utilities';
import ModalComponent from '../Shared/ModalComponent';
import {Pannellum} from 'pannellum-react';
import {toast} from "../../Services/AlertService";
import SharedService from "../../Services/SharedService";

type state = {
  photoIndex: number,
  isOpen: boolean,
  modalOpen: boolean,
  info: any
};

class PropertyDetailComponent extends Component <{}, state>{
  constructor(props){
    super(props);
    this.state = {
      photoIndex: 0,
      isOpen: false,
      modalOpen: false,
      info: {
        title: 'Por favor ingrese sus datos',
        body: {}
      },
      inputs: [
        {
          type: 'text',
          placeHolder: 'Nombre',
          name: 'name'
        },
        {
          type: 'number',
          placeHolder: 'Teléfono',
          name: 'phone'
        },
        {
          type: 'email',
          placeHolder: 'Correo Electrónico',
          name: 'email'
        }
      ]
    };

  }

  componentDidMount(){
    const {id} = queryString.parse(this.props.location.search);

    PropertyService.getProperty(id).then(res => {
      this.setState(previousState => {
        return {...previousState, property: res};
      });
    });

  }

  sendInterestAlert = e => {
    e.preventDefault();
    const clone = Object.assign({}, this.state);
    if(clone.name && clone.phone){
      const params = {name: clone.name, phone: clone.phone, email: clone.email};
      if(params.email){
        if(!params.email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)){
          return toast('El correo electrónico es inválido', 'error');
        }
      }
      params.msg = "Deseo que me contacten acerca de la propiedad con consecutivo: # " + this.state.property.ID + " cod: " + this.state.property.CODE;
      SharedService.sendInterestAlert(params);
      this.setState({...this.state, modalOpen: false})
    } else{
      return toast('Ingrese los campos requeridos', 'error');
    }
  };

  handleChange = event => {
    let {value, name} = event.target;
    if(event.target.type === 'checkbox'){
      value = event.target.checked ? 'si' : 'no';
    }
    this.setState({[name]: value});
  };

  render(){
    const {photoIndex, property, isOpen, isOpenModal360, inputs} = this.state;
    if(!property){
      return <List/>;
    } else{
      property.details = property.details.filter(i => !['Administracion valor', 'Número telefónico', 'Tipo Inmueble', 'Nombre Urbanización'].includes(i.name));
    }

    let images = this.state.property.photos.reduce((photos, i) => {
      if(i.split('_')[1] === 's'){
        const large = i.replace('_s', '_l');
        if(this.state.property.photos.includes(large)){
          photos.push(`${imageUrl}${i}`);
        }
      }
      return photos;
    }, []);

    const icons = {
      'Sala': living,
      'Sala Comedor': living,
      'Comedor': dining,
      'Comedor Auxiliar': dining,
      'Cocina': fridge,
      'Estufa': gas,
      'Horno': gas,
      'Gas': gas,
      'N. Patios': backyard,
      'N. Vestier': vestier,
      'N. Alcobas': room,
      'N. Baños': bath,
      'N. Garajes': indoorParking,
      'N. Closets': closet,
      'Nivel #': stairs,
      'Lineas Telefónicas': phone,
      'Alcoba Servicio': room,
      'Biblioteca': library,
      'Hall': hall,
      'Mezanine': level,
      'Mirador': balcony,
      'Balcon': balcony,
      'Terraza': frontyard,
      'Zona Ropas': closet,
      'Baño Servicio': bath,
      'Baño Cabina': bathCabin,
      'Cuarto Útil': room,
      'Ascensor': elevator,
      'Nombre Urbanización': coniferous,
      'Juegos': park,
      'Jacuzzi': bidet,
      'Agua caliente': hotWater,
      'Parqueadero': indoorParking,
      'Parqueadero Cubierto': indoorParking,
      'Portería': gate,
      'Portería 24 Horas': gate,
      'Piscina': pool,
      'Pisos': cushion,
      'Salón Social': salon,
      'Sauna': sauna,
      'Turco': bidet,
      'Zonas Verdes': coniferous,
      'Zona Deportiva': sports,
      'TV por cable': tv,
      'Unidad Cerrada': closedUnit,
      'SALON': salon,
    };

    const images360 = property.photos.reduce((photos, i) => {
      if(i.split('_')[1] === '360'){
        photos.push(`${imageUrl}${i}`);
      }
      return photos;
    }, []);

    return (
        <Fragment>
          <div className = "container mb-4">
            <div className = "row pt-5">
              <div className = "col-2 d-flex align-items-center">
                <button className = "btn btn-secondary px-3"
                        onClick = {this.props.history.goBack}>
                  <i className = "fa fa-arrow-left mr-2"/>Regresar
                </button>
              </div>
              <div className = "col-6">
                <h2>
                  {property.NEIGHBORHOOD} - {property.CITY}
                </h2>
                <h3 className = "text-primary">
                  <b>${formatPrice(property.PRICE)}</b>
                </h3>
              </div>
              <div className = "col-4 d-flex justify-content-around align-items-center">
              <span className = "py-2 px-4 border border-dark rounded">
                {property.AREA}mt<sup>2</sup>
              </span>
                <button onClick = {() => this.setState({modalOpen: true})} className = "btn btn-secondary px-5">
                  Me interesa
                </button>
              </div>
            </div>
            <div className = "row mb-4 pt-3">
              <div className = "">
                {images.map(i => (
                    <img className = "p-1 clickable"
                         onClick = {() => this.setState({isOpen: true, photoIndex: images.indexOf(i)})}
                         width = {100}
                         height = {140} src = {i} alt = "..."/>))}
              </div>
              <div className = "">
                {images360.map(i => (
                    <img className = "p-1 clickable"
                         onClick = {() => this.setState({isOpenModal360: true, photoIndex: images360.indexOf(i)})}
                         width = {100}
                         height = {140} src = {i} alt = "..."/>))}
              </div>
              <br/>

            </div>

            <div className = "card border bg-white">
              <div className = "card-body">
                <div className = "row">
                  {property.details.map(i => (
                      <div className = "col-3">
                        {i.name}: <br/>
                        <img src = {icons[i.name]} height={14} alt = ""/> {i.type === 3 ? (i.value === '1' ? 'Si' : 'No') : i.value}
                      </div>
                  ))}
                </div>
                {!!property.OBS && <div className="row p-3">
                  Observaciones: <br/>
                  {property.OBS}
                </div>}
              </div>
            </div>
          </div>
          {isOpen && (
              <Lightbox reactModalStyle = {{overlay: {'zIndex': 2000}}}
                        mainSrc = {images[photoIndex].replace('_s', '_l')}
                        nextSrc = {images[(photoIndex + 1) % images.length]}
                        prevSrc = {images[(photoIndex + images.length - 1) % images.length]}
                        onCloseRequest = {() => this.setState({isOpen: false})}

                        onMovePrevRequest = {() =>
                            this.setState({
                              photoIndex: (photoIndex + images.length - 1) % images.length
                            })
                        }
                        onMoveNextRequest = {() =>
                            this.setState({
                              photoIndex: (photoIndex + 1) % images.length
                            })
                        }
              />
          )}
          {isOpenModal360 && (
              <ModalComponent footer = {<Fragment>
                <button type = "button" className = "btn btn-dark"
                        onClick = {() => this.setState({...this.state, isOpenModal360: false})}>Cancel
                </button>
              </Fragment>} onClose = {() => this.setState({...this.state, isOpenModal360: false})} body = {<Fragment>
                <Pannellum
                    width = "100%"
                    height = "500px"
                    image = {images360[photoIndex]}
                    pitch = {10}
                    yaw = {180}
                    hfov = {110}
                    autoLoad
                    onLoad = {() => {
                      console.log('panorama loaded');
                    }}
                >
                </Pannellum>
              </Fragment>}
              />
          )}
          {this.state.modalOpen &&
          <ModalComponent header = "Ingrese sus datos"
                          footer = {<Fragment>
                            <button type = "button" className = "btn btn-primary"
                                    onClick = {this.sendInterestAlert}>Enviar
                            </button>
                            <button type = "button" className = "btn btn-dark"
                                    onClick = {() => this.setState({...this.state, modalOpen: false})}>Cancel
                            </button>
                          </Fragment>}
                          body = {inputs.map(i => (
                              <div className = "">
                                <label className = "text-ayu-bold">
                                  {i.placeHolder}
                                </label>
                                <input
                                    className = "form-control mb-3"
                                    type = {i.type}
                                    placeholder = {i.placeHolder}
                                    name = {i.name}
                                    key = {i.name}
                                    value = {this.state[i.name]}
                                    onChange = {this.handleChange}
                                />
                              </div>))}
                          onClose = {() => this.setState({...this.state, modalOpen: false})}
                          id = {this.state.property.ID}
                          cod = {this.state.property.CODE}/>}
        </Fragment>
    );
  }
}

export default PropertyDetailComponent;
