// @flow
import React, {Component} from 'react';
import LogoHouse from '../../Assets/Images/home.png';
import FormComponent from '../Shared/FormComponent/FormComponent';
import AuthService from '../../Services/AuthService';
import {toast} from '../../Services/AlertService';
import type {RouterHistory} from 'react-router-dom';

type Props = {
  history: RouterHistory
};

class LoginOwnerComponent extends Component<Props, {}>{
  login = (login: number, password: number) => {
    AuthService.login({login, password, type: 'CUSTOMER'}).then((res: any) => {
      let type, message;
      if(res.success){
        type = 'success';
        message = 'Login Successfull';
        this.props.history.push({
          pathname: '/contratos'
        });
      } else{
        type = 'error';
        message = res.error;
      }
      toast(message, type);
    });
  };

  render(){
    return (
        <div className = "container p-5">
          <div className = "d-flex justify-content-center">
            <FormComponent
                title = "Propietarios"
                logo = {LogoHouse}
                click = {this.login}
            />
          </div>
        </div>
    );
  }
}

export default LoginOwnerComponent;
