// @flow
import React, {Component} from 'react';
import LogoUser from '../../Assets/Images/user.png';
import FormComponent from '../Shared/FormComponent/FormComponent';
import AuthService from '../../Services/AuthService';
import {toast} from '../../Services/AlertService';
import type {RouterHistory} from 'react-router-dom';
import LogoHouse from "../../Assets/Images/home.png";

type Props = {
  history: RouterHistory
};

class LoginClientComponent extends Component<Props>{
  loginTennant = (id: number, password: number) => {
    AuthService.login({login: id, password, type: 'TENANT'}).then((res: any) => {
      let type, message;
      if(res.success){
        type = 'success';
        message = 'Login Successfull';
        this.props.history.push({
          pathname: '/inquilino'
        });
      } else{
        type = 'error';
        message = res.error;
      }
      toast(message, type);
    });
  };

  loginOwner = (login: number, password: number) => {
    AuthService.login({login, password, type: 'CUSTOMER'}).then((res: any) => {
      let type, message;
      if(res.success){
        type = 'success';
        message = 'Login Successfull';
        this.props.history.push({
          pathname: '/contratos'
        });
      } else{
        type = 'error';
        message = res.error;
      }
      toast(message, type);
    });
  };

  render(){
    return (
        <div className = "container">
          <div className = "row p-5">
            <div className = "col-12 col-md-6 mb-4 mb-md-0 d-flex justify-content-center align-items-center">
              <FormComponent
                  title = "Clientes"
                  logo = {LogoUser}
                  click = {this.loginTennant}
              />
            </div>

            <div className = "col-12 col-md-6 d-flex justify-content-center">
              <FormComponent
                  title = "Propietarios"
                  logo = {LogoHouse}
                  click = {this.loginOwner}
              />
            </div>
          </div>
        </div>

    );
  }
}

export default LoginClientComponent;
