import React, {Component} from 'react';
import Dropzone from "react-dropzone";
import {toast} from "../../../Services/AlertService";
import RequestsService from "../../../Services/RequestsService";

class RepairComponent extends Component{
  componentDidMount(){
    this.setState({name: this.props.tenantName})
  }

  state = {
    info: [
      {
        type: 'email',
        placeHolder: '*Correo Electrónico',
        name: 'email'
      },
      {
        type: 'number',
        placeHolder: '*Teléfono',
        name: 'phone'
      },
      {
        type: 'number',
        placeHolder: 'Celular',
        name: 'cellphone'
      },
      {
        type: 'text',
        placeHolder: 'Horario de disponibilidad',
        name: 'availability'
      },
    ],
    files: [],
    sum: [this.randomNum(10), this.randomNum(10)]
  };


  thumbsContainer = {
    display: 'flex',
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginTop: 20
  };

  thumb = {
    display: 'inline-flex',
    borderRadius: 2,
    border: '1px solid #eaeaea',
    marginBottom: 8,
    marginRight: 8,
    width: 100,
    height: 100,
    padding: 4,
    boxSizing: 'border-box',
    position: "relative",
    listStyle: "none",
    float: "left"
  };

  thumbInner = {
    display: 'flex',
    minWidth: 0,
    overflow: 'hidden'
  };

  img = {
    display: 'block',
    width: 'auto',
    height: '100%'
  };
  cancel = {
    cursor: "pointer",
    position: "absolute",
    top: "-20px",
    right: "0",
    display: "block"
  };

  getBase64(file){
    return new Promise((resolve, reject) => {
        const reader = new FileReader();
        reader.readAsDataURL(file);
        reader.onload = () => resolve(reader.result);
        reader.onerror = error => reject(error);
    });
  }

  onDrop(files){
    files = files.map(file => Object.assign(file, {
      preview: URL.createObjectURL(file)
    }));
    const newFiles = this.state.files;
    newFiles.push(files[0]);
    this.setState({files: newFiles});
  }


  handleChange = event => {
    const {value, name} = event.target;
    this.setState({[name]: value});
  };

  randomNum(max){
    return Math.floor(Math.random() * Math.floor(max - 1)) + 1;
  }

  onCancel(){
    this.setState({
      files: []
    });
  }

  deleteImage(index){
    const files = this.state.files;
    files.splice(index, 1);
    this.setState({files});
  }

  onSubmitForm = () => {
    const {total, sum, name, email, phone, cellphone, availability, description} = this.state;

    if(parseInt(total) === sum.reduce((a, b) => a + b, 0)){
      if(parseInt(total) === sum.reduce((a, b) => a + b, 0)){
        if(!name || !email || !phone){
          toast('Ingrese los campos requeridos.', 'error');
        } else{
          if(!email.match(/^([\w.%+-]+)@([\w-]+\.)+([\w]{2,})$/i)){
            toast('Ingrese un correo electrónico válido', 'error');
          } else{
            if(!this.state.files.length){
              toast('Por favor adjunte un archivo.', 'error');
              return
            }
            const newFiles = this.state.files.map(file => this.getBase64(file));
            Promise.all(newFiles).then(data => {
              RequestsService.sendRepairRequest({
                    name, contract: this.props.contract, email, phone, cellphone, availability, description, files: data
                  }
              ).then(res => {
                if(res.success){
                  toast(res.res.msg, 'success')
                } else{
                  toast('Un error ha ocurrido, intentelo más tarde', 'error')
                }
              }).catch(err => {
                console.error(err);
                toast('Un error ha ocurrido, intentelo más tarde', 'error')
              });
            });
          }
        }
      }
    } else{
      toast('Realice la suma de verificación.', 'error')
    }
  };

  render(){
    const files = this.state.files.map(file => (
        <li key = {file.name}>
          {file.name} - {file.size} bytes
        </li>
    ));
    const thumbs = this.state.files.map((file, index) => (
        <div style = {this.thumb} key = {file.name}>
          <div style = {this.thumbInner}>
            <span style = {this.cancel} onClick = {() => this.deleteImage(index)}>x</span>
            <img
                src = {file.preview}
                style = {this.img}
                alt=""
            />
          </div>
        </div>
    ));
    return (
        <div className = "container bg-white py-3">
          <div className = "row mt-3 mb-5">
            <div className = "col-2 d-flex justify-content-center">
              <button className = "btn btn-secondary px-3"
                      onClick = {this.props.cancelRepair}>
                <i className = "fa fa-arrow-left mr-2"/>Regresar
              </button>
            </div>
            <div className = "col-12 col-md-10">
              <h2 className = "text-primary text-left text-ayu-bold">
                Mantenimiento de inmueble
              </h2>
            </div>
          </div>
          <div className = "row p-4">
            <div className = "col-12">
              <div className = "row">
                <div className = "col-12 col-md-6 form-check mb-2">
                  <label className = "text-ayu-bold">
                    *Nombre
                  </label>
                  <input
                      className = "form-control mb-3"
                      type = 'text'
                      name = 'name'
                      value = {this.state.name}
                      onChange = {this.handleChange}
                  />
                </div>
                <div className = "col-12 col-md-6 form-check mb-2">
                  <label className = "text-ayu-bold">
                    Contrato
                  </label>
                  <input
                      className = "form-control mb-3"
                      type = 'text'
                      name = 'contract'
                      value = {this.props.contract}
                      disabled = {true}
                  />
                </div>
                {this.state.info.map(i => (
                    <div className = "col-6 col-md-3 form-check mb-2" key = {i.name}>
                      <label className = "text-ayu-bold">
                        {i.placeHolder}
                      </label>
                      <input
                          className = "form-control mb-3"
                          type = {i.type}
                          placeholder = {i.placeHolder}
                          name = {i.name}
                          onChange = {this.handleChange}
                      />
                    </div>
                ))}
                <div className = "col-12 col-md-6">
                  <textarea name = "description"
                            onChange = {this.handleChange}
                            style = {{resize: 'none'}}
                            className = "form-control mb-3"
                            placeholder = "Descripción detallada del daño" rows = "3"/>
                </div>
                <div className = "col-12 col-md-6">
                  < Dropzone onDrop = {this.onDrop.bind(this)}
                             onFileDialogCancel = {this.onCancel.bind(this)}>
                    {({getRootProps, getInputProps}) => (
                        <div {...getRootProps()}
                             className = "doc-uploader w-100 mb-3">
                          <input  {...getInputProps()} />
                          {this.state.files.length ?
                              <ul>
                                {files.map(file => file)}
                              </ul>
                              :
                              (<span>Arrastre aquí evidencias o presione click para buscarlas en su computador</span>)
                          }
                        </div>
                    )}
                  </Dropzone>
                  <aside style = {this.thumbsContainer}>
                    {thumbs}
                  </aside>
                </div>
                <div className = "col-6 col-md-3 form-check mb-2">
                  <label className = "text-ayu-bold">
                    Cuanto es {this.state.sum[0]} + {this.state.sum[1]}?
                  </label>
                  <input
                      className = "form-control mb-3"
                      type = 'number'
                      name = 'total'
                      placeholder = 'Responda para continuar'
                      onChange = {this.handleChange}
                  />
                </div>
              </div>
              <div className = "row">
                <div className = "col">
                  <button className = "btn btn-secondary send-button"
                          disabled = {!this.state.total}
                          onClick = {this.onSubmitForm}
                  >
                    Enviar
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
    );
  }
}

export default RepairComponent;