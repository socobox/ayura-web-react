import React, {Component} from 'react';
import './Contracts.scss'


class ContractsComponent extends Component<{}, {}>{

  render(){
    const {owner, contracts, select, repair, contractKey} = this.props;
    return (
        <div className = "container">
          <div className = "row py-5">
            <div className = "col-12">
              <h2 className = "text-primary text-center text-ayu-bold">
                Sus Contratos
              </h2>
            </div>
          </div>
          <div className = "row text-center">
            <div className = "container">
              <div className = "row info-table p-1 info-table-header">
                <div className = "col-3 p-1">Contrato</div>
                <div className = "col-3 ">Cons. Propiedad</div>
                <div className = "col-3 ">{owner ? "Inquilino" : "Propietario"}</div>
                <div className = "col-3 ">{owner ? "CC. Inquilino" : "CC. Propietario"}</div>
              </div>
              {contracts.map(i => (
                  <div key = {i.CONTRACT_ID} onClick={() => select(i.CONTRACT_ID, i.CONTRACT_CODE, i.TENANT_NAME)} className = {`row info-table info-table-item clickable ${i.CONTRACT_ID === contractKey ? "item-active":""}`}>
                    <div className = "col-3 ">{!owner && <button className="btn btn-secondary btn-sm" onClick={repair}><i className="fa fa-wrench" /></button>} {i.CONTRACT_CODE}</div>
                    <div className = "col-3 ">#{i.PROPERTY_ID}</div>
                    <div className = "col-3 ">{owner ? i.TENANT_NAME : i.OWNER_NAME}</div>
                    <div className = "col-3 ">{owner ? i.TENANT_DOCUMENT : i.OWNER_DOCUMENT}</div>
                  </div>
              ))}

            </div>
          </div>
        </div>
    );
  }
}


export default ContractsComponent;
