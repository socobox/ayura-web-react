import React, {Component, Fragment} from 'react';
import Contracts from './ContractsComponent/ContractsComponent';
import Documents from '../Shared/DocumentsComponent/DocumentsComponent';
import Navbar from '../Shared/LoggedNavbarComponent';
import {bindActionCreators} from "redux";
import {loadContracts, loadDocuments} from "../../Redux/ActionTypes";
import {connect} from "react-redux";
import type {RouterHistory} from "react-router-dom";
import {withRouter} from 'react-router-dom';
import Repair from "./RepairComponent/RepairComponent";

type state = {
  mainLoad: boolean,
  currentContract: number,
  contractId: number,
  fatherFolder: string,
  currentPage: number
}

type Props = {
  history: RouterHistory
};

class DirectoryComponent extends Component<Props, state>{

  state = {
    mainLoad: true,
    currentContract: 0,
    fatherFolder: '',
    contractId: 0,
    repairRequest: false,
    tenatName: '',
    currentPage: 1
  };

  owner = this.props.owner;

  logOut(){
    localStorage.setItem('token', '');
    localStorage.setItem('id', '');
    this.props.history.push({
      pathname: '/login_clients'
    });
  }

  componentDidMount(){
    const token = localStorage.getItem('token');
    const id = localStorage.getItem('id');
    this.props.loadContracts(token, id, this.props.owner);
  }

  loadRoot(){
    const token = localStorage.getItem('token');
    this.setState(previousState => {
      return {...previousState, mainLoad: true, currentPage: 1};
    });
    this.props.loadDocuments(token, this.state.currentContract);
  }

  nextPage = add => {
    this.setState((prev, actualProps) => {
      if( prev.currentPage + add <= 0 || prev.currentPage + add > this.props.pages){
        return {...prev};
      }
      this.loadFromFolder(prev.currentFolder, prev.fatherFolder, prev.currentPage + add);
      return {...prev, currentPage: prev.currentPage + add};
    });
  };

  componentDidUpdate(prevProps: Readonly<P>, prevState: Readonly<S>, snapshot: SS): void{
    if(!this.state.currentContract){
      this.setState({
        ...this.state,
        currentContract: this.props.contracts[0].CONTRACT_ID,
        tenantName: this.props.contracts[0].TENANT_NAME,
        contractId: this.props.contracts[0].CONTRACT_CODE
      }, () => {
        this.loadRoot();
      });
    }
  }

  loadFromFolder(folderId, name, page){
    const token = localStorage.getItem('token');
    this.setState(previousState => {
      return {...previousState, mainLoad: false, fatherFolder: name, currentFolder: folderId};
    }, () => {
      this.props.loadDocuments(token, this.state.currentContract, folderId, page);
    });
  }

  openDoc(file){
    if(file.ICON_TYPE === "Folder"){
      return this.loadFromFolder(file.DOC_TYPE_ID, file.FOLDER_NAME, 1)
    } else{
      const link = document.createElement('a');
      link.href = file.CONTENT_PATH;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  selectContract(item, contract, tenantName){
    if(item !== this.state.currentContract){
      this.setState({...this.state, currentContract: item, contractId: contract, tenantName}, () => {
        this.loadRoot()
      });
    }
  }

  repair = () => {
    this.setState({repairRequest: true})
  };
  cancelRepair = () => {
    this.setState({repairRequest: false})
  };
  render(){
    const {currentContract, repairRequest, fatherFolder, contractId, mainLoad, tenantName, currentPage} = this.state;
    const {contracts, documents, pages} = this.props;
    return (
        <Fragment>
          <Navbar action = {this.logOut.bind(this)}/>
          <div className = "container">
            <div className = "row pt-2">
              <Contracts contractKey = {currentContract}
                         owner = {this.owner}
                         contracts = {contracts}
                         repair = {this.repair}
                         select = {this.selectContract.bind(this)}/>
            </div>
            <div className = "row">
              {!repairRequest &&
              <Documents owner = {this.owner}
                         folder = {fatherFolder}
                         documents = {documents}
                         pages={pages}
                         contract = {contractId}
                         currentPage = {currentPage}
                         nextPage={this.nextPage}
                         loadRoot = {this.loadRoot.bind(this)}
                         openDoc = {this.openDoc.bind(this)}
                         mainLoad = {mainLoad}/>
              }
              {repairRequest &&
                  <Repair cancelRepair={this.cancelRepair}
                          contract={contractId}
                          tenantName={tenantName}/>
              }

            </div>
          </div>
        </Fragment>
    );
  }
}

function mapDispatchToProps(dispatch){
  return bindActionCreators({loadDocuments, loadContracts}, dispatch);
}

function mapStateToProps(state){
  const {documents, contracts, pages} = state.InformationReducer;
  return {documents, contracts, pages};
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(DirectoryComponent));
