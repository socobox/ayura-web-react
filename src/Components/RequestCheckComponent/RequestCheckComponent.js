import React, {Component, Fragment} from 'react';
import Navbar from '../Shared/LoggedNavbarComponent';
import type {RouterHistory} from "react-router-dom";
import {withRouter} from "react-router-dom";
import Info from './RequestInfoComponent';
import Documents from '../Shared/DocumentsComponent/DocumentsComponent';
import {bindActionCreators} from "redux";
import {loadReqComments, loadReqDocuments, loadReqInfo} from "../../Redux/ActionTypes";
import {connect} from "react-redux";
import RequestsService from "../../Services/RequestsService";
import {toast} from "../../Services/AlertService";


type Props = {
  history: RouterHistory
};

type state = {
  mainLoad: boolean,
  currentContract: number,
  contractId: number,
  userId: number,
  fatherFolder: string,
  fatherFolderId: number,
  token: string
}

class RequestCheckComponent extends Component<Props, state>{

  state = {
    mainLoad: true,
    currentContract: 0,
    fatherFolder: '',
    fatherFolderId: 0,
    contractId: 0,
    userId: localStorage.getItem('user_id'),
    token: localStorage.getItem('token')
  };

  logOut(){
    localStorage.setItem('token', '');
    localStorage.setItem('id', '');
    localStorage.setItem('user_id', '');
    this.props.history.push({
      pathname: '/consultar_solicitud'
    });
  }

  uploadFile(file){
    const formData = new FormData();
    formData.append('file', file);
    formData.append('model', JSON.stringify({
      DOC_TYPE_ID: this.state.fatherFolderId, ENTITY_ID: this.state.userId, FOLDER_ID: 1, token: this.state.token
    }));
    RequestsService.uploadFile(formData).then(res => {
      this.props.loadReqDocuments(this.state.token, this.state.currentContract, this.state.userId, this.state.fatherFolderId);
      toast('Documento agregado exitosamente.', 'success');
    }).catch(() => {
      toast('Intentelo nuevamente.', 'error');
    });
  }

  sendRequestComment(comment){
    if(comment.length){
      RequestsService.addRequestComment(this.state.token, this.state.userId, comment).then(res => {
        this.props.loadReqComments(this.state.userId);
        toast('Comentario agregado exitosamente.', 'success');
      }).catch(() => {
        toast('Intentelo nuevamente.', 'error');
      });

    }
  }

  loadRoot(){
    this.setState(previousState => {
      return {...previousState, mainLoad: true};
    });
    this.props.loadReqDocuments(this.state.token, this.state.currentContract, this.state.userId);
  }

  loadFromFolder(folderId, name){
    this.setState(previousState => {
      return {...previousState, mainLoad: false, fatherFolder: name, fatherFolderId: folderId};
    }, () => {
      this.props.loadReqDocuments(this.state.token, this.state.currentContract, this.state.userId, folderId);
    });
  }

  openDoc(file){
    if(file.ICON_TYPE === "Folder"){
      return this.loadFromFolder(file.DOC_TYPE_ID, file.FOLDER_NAME)
    } else{
      const link = document.createElement('a');
      link.href = file.CONTENT_PATH;
      document.body.appendChild(link);
      link.click();
      document.body.removeChild(link);
    }
  }

  componentDidMount(){
    const id = localStorage.getItem('id');
    this.props.loadReqInfo(this.state.token, id, this.state.userId);
    this.props.loadReqDocuments(this.state.token, id, this.state.userId);
    this.props.loadReqComments(this.state.userId);
  }

  render(){
    return (
        <Fragment>
          <Navbar action = {this.logOut.bind(this)}/>
          <div className = "container">
            <div className = "row pt-2">
              <Info info = {this.props.information}
                    comments = {this.props.comments}/>
            </div>
            <div className = "row pt-2">
              <Documents request = {true}
                         folder = {this.state.fatherFolder}
                         documents = {this.props.documents}
                         contract = {this.state.userId}
                         loadRoot = {this.loadRoot.bind(this)}
                         openDoc = {this.openDoc.bind(this)}
                         mainLoad = {this.state.mainLoad}
                         comments = {this.props.comments}
                         sendComment = {this.sendRequestComment.bind(this)}
                         uploadFile = {this.uploadFile.bind(this)}/>
            </div>
          </div>
        </Fragment>
    );
  }
}


function mapDispatchToProps(dispatch){
  return bindActionCreators({loadReqInfo, loadReqDocuments, loadReqComments}, dispatch);
}

function mapStateToProps(state){
  const {information, documents, comments} = state.InformationReducer;
  return {information, documents, comments};
}

export default withRouter(connect(
    mapStateToProps,
    mapDispatchToProps
)(RequestCheckComponent));
