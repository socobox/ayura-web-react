import PropertiesService from '../Services/PropertiesService';
import AddressService from '../Services/AddressService';
import ContractService from '../Services/ContractService';
import RequestsService from '../Services/RequestsService';

export const LOAD_PROPERTIES = 'LOAD_PROPERTIES';
export const LOAD_PROPERTIES_TYPES = 'LOAD_PROPERTIES_TYPES';
export const LOAD_PROPERTIES_RENT_PRICES = 'LOAD_PROPERTIES_RENT_PRICES';
export const LOAD_PROPERTIES_SELL_PRICES = 'LOAD_PROPERTIES_SELL_PRICES';
export const LOAD_PROPERTIES_AREAS = 'LOAD_PROPERTIES_AREAS';
export const LOAD_CITIES = 'LOAD_CITIES';
export const LOADING_PROPERTIES = 'LOADING_PROPERTIES';
export const GET_PROPERTY = 'LOAD_PROGET_PROPERTYPERTIES';
export const LOAD_CONTRACTS = 'LOAD_CONTRACTS';
export const LOAD_DOCUMENTS = 'LOAD_DOCUMENTS';
export const LOAD_REQUEST_INFO = 'LOAD_REQUEST_INFO';
export const LOAD_REQUEST_DOCUMENTS = 'LOAD_REQUEST_DOCUMENTS';
export const LOAD_REQUEST_COMMENTS = 'LOAD_REQUEST_COMMENTS';


export const loadProperties = (param: any) => ({
  type: LOAD_PROPERTIES,
  payload: PropertiesService.loadProperties(param)
});
export const loadFinish = () => ({
  type: LOADING_PROPERTIES,
  payload: {}
});
export const loadPropertiesTypes = () => ({
  type: LOAD_PROPERTIES_TYPES,
  payload: PropertiesService.loadPropertiesTypes()
});
export const loadPropertiesRentPrices = () => ({
  type: LOAD_PROPERTIES_RENT_PRICES,
  payload: PropertiesService.loadPropertiesRentPrices()
});
export const loadPropertiesSellPrices = () => ({
  type: LOAD_PROPERTIES_SELL_PRICES,
  payload: PropertiesService.loadPropertiesSellPrices()
});
export const loadPropertiesAreas = () => ({
  type: LOAD_PROPERTIES_AREAS,
  payload: PropertiesService.loadPropertiesAreas()
});
export const loadCities = () => ({
  type: LOAD_CITIES,
  payload: AddressService.loadCitys()
});


export const getProperty = (id: number) => ({
  type: GET_PROPERTY,
  payload: PropertiesService.getProperty(id)
});
export const loadContracts = (token: string, id: string, owner ?: boolean, size ?: number = 10) => ({
  type: LOAD_CONTRACTS,
  payload: ContractService.getContracts(token, id, owner)
});

export const loadDocuments = (token: string, id: string, docTypeId ?: number, page ?: number) => ({
  type: LOAD_DOCUMENTS,
  payload: ContractService.getDocs(token, id, docTypeId, page)
});

export const loadReqInfo = (token: string, id: string, reqId ?: number) => ({
  type: LOAD_REQUEST_INFO,
  payload: RequestsService.getInfo(token, id, reqId)
});

export const loadReqDocuments = (token: string, id: string, reqId ?: number, docTypeId ?: number, size ?: number) => ({
  type: LOAD_REQUEST_DOCUMENTS,
  payload: RequestsService.getRequestDocs(token, id, reqId, docTypeId)
});

export const loadReqComments = (reqId ?: number) => ({
  type: LOAD_REQUEST_COMMENTS,
  payload: RequestsService.getRequestComments(reqId)
});