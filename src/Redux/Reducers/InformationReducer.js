import {
  LOAD_CONTRACTS, LOAD_DOCUMENTS, LOAD_REQUEST_INFO, LOAD_REQUEST_DOCUMENTS, LOAD_REQUEST_COMMENTS
} from '../ActionTypes';
import {
  sortArray
} from '../../Utils/Utilities';

const initialState = {
  contracts: [],
  documents: [],
  comments: [],
  pages: 0
};

export default function InformationReducer(state: any = initialState, action: {
  type: string,
  payload: any
}){
  switch(action.type){
    case LOAD_CONTRACTS:

      return Object.assign({}, state, {
        contracts: sortArray(action.payload, 'label')
      });

    case LOAD_DOCUMENTS:

      return Object.assign({}, state, {
        documents: action.payload.items, pages: action.payload.totalPages
      });

    case LOAD_REQUEST_INFO:
      return Object.assign({}, state, {
        information: action.payload
      });

    case LOAD_REQUEST_DOCUMENTS:
      return Object.assign({}, state, {
        documents: action.payload
      });

    case LOAD_REQUEST_COMMENTS:
      return Object.assign({}, state, {
        comments: action.payload
      });

    default:
      return state;
  }
}