export function sortArray(array: Array<any>, key: string, asc ?: boolean = true): Array<any>{
  const tmpArray = array.slice()
  tmpArray.sort((a, b) => {
    let tmp = a - b;
    if(key){
      const type = typeof a[key];
      if(type === "string"){
        if(a[key] < b[key]){
          tmp = -1;

        } else{
          if(a[key] > b[key]){
            tmp = 1;

          } else{
            tmp = 0;
          }

        }
      } else{
        tmp = a[key] - b[key];
      }
    }
    return asc ? tmp : -tmp
  });
  return tmpArray;

}

export function formatPrice(price: number){
  return new Intl.NumberFormat('es-419', {
    maximumFractionDigits: 0
  }).format(price);
}