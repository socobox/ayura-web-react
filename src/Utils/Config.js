export const baseUrl: string = process.env.REACT_APP_DOMAIN || '';
export const imageUrl: string = `${baseUrl}/property/photo?key=`;
export const URL: any = {
  address: {
    cities: '/address/city/options'
  },
  user: {
    login: '/user/login'
  },
  contactRequest: {
    add: '/contact-request/add'
  },
  propertyRequest:{
    list: '/prop-request/list',
    comment: {
      list: '/prop-request/comments',
      add: '/prop-request/comments/add'
    }
  },
  payment: '/payment',
  content: {
    search: '/content/search',
    upload: '/content/upload'
  },
  repair: '/repair/'
};
