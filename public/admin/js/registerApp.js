var registerApp = angular.module('adminayuraRegister', ["services", "ngAlertify"]);

registerApp.directive('fileUpload', function () {
  return {
    scope: true,        //create a new scope
    link: function (scope, el, attrs) {
      el.bind('change', function (event) {
        var files = event.target.files;
        //iterate files since 'multiple' may be specified on the element
        for (var i = 0; i < files.length; i++) {
          //emit event upward
          scope.$emit("fileSelected", { file: files[i], control: event.target});

        }
      });
    }
  };
});

registerApp.controller("registerCtrl", function($scope, RequestServices, AdminServices, DateServices, logoServices, alertify){
  var mainPath = AdminServices.getPath(), post_url = mainPath+"property/register";
  
 



  $scope.options = [{
    label: "Agua Caliente",
    field:"aguaCaliente",
    model:false
  },{
    label: "Parqueadero",
    field:"parqueadero",
    model:false
  },{
    label: "Sala Comedor",
    field:"salaComedor",
    model:false
  },{
    label: "Baño Cabina",
    field:"banoCabina",
    model:false
  },{
    label: "Zona Ropas",
    field:"zonaRopas",
    model:false
  },{
    label: "Cuarto de servicio",
    field:"cuartoServicio",
    model:false
  },{
    label: "Baño servicio",
    field:"banoServicio",
    model:false
  },{
    label: "Cuarto útil",
    field:"cuartoUtil",
    model:false
  },{
    label: "Línea teléfono",
    field:"lineaTelefono",
    model:false
  },{
    label: "Biblioteca",
    field:"biblioteca",
    model:false
  },{
    label: "Vestier",
    field:"vestier",
    model:false
  },{
    label: "Balcón",
    field:"balcon",
    model:false
  },{
    label: "Hall",
    field:"hall",
    model:false
  },{
    label: "Terraza",
    field:"terraza",
    model:false
  }];

  $scope.unidadCerradaOptions = [{
    label: "Juegos",
    field:"juegos",
    model:false
  },{
    label: "Piscina",
    field:"piscina",
    model:false
  },{
    label: "Salón Social",
    field:"salonSocial",
    model:false
  },{
    label: "Porteria 24hrs",
    field:"porteria24h",
    model:false
  },{
    label: "Ascensor",
    field:"ascensor",
    model:false
  }];

  $scope.registerData = {
    name: "",
    phone: "",
    email: "",
    property_info:{
      options:[],
      unidadCerradaOptions:[],
      tipoServicio:"",
      propietario:"",
      direccion:"",
      telefonoPropietario:"",
      tipoPropiedad:"",
      barrio:"",
      precioInmueble:"",
      area:"",
      estrato:"",
      unidadCerrada:"",
      nombreUnidad:"",
      valorAdmon:"",
      numeroAlcobas:"",
      numeroClosets:"",
      numeroBanos:"",
      cocina:"",
      gas:"",
      numGarajes:"",
      pisos:"",
      nivel:"",
      tipoInmueble:"",
      numPatios:"",
    }

  }

  function isEmail(email) {
    var regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    return regex.test(email);
  }

  $scope.sendData = function(){
    var valid =  true;
    var registerData = {
    name: $scope.nombre,
    phone: $scope.telefono,
    email: $scope.email,
    property_info:{
      tipoServicio:$scope.tipoServicio,
      propietario:$scope.propietario,
      direccion:$scope.direccion,
      telefonoPropietario:$scope.telefonoPropietario,
      tipoPropiedad:$scope.tipoPropiedad,
      barrio:$scope.barrio,
      precioInmueble:$scope.precioInmueble,
      area:$scope.area,
      estrato:$scope.estrato,
      unidadCerrada:$scope.unidadCerrada,
      nombreUnidad:$scope.nombreUnidad,
      valorAdmon:$scope.valorAdmon,
      numeroAlcobas:$scope.numeroAlcobas,
      numeroClosets:$scope.numeroClosets,
      numeroBanos:$scope.numeroBanos,
      cocina:$scope.cocina,
      gas:$scope.gas,
      numGarajes:$scope.numGarajes,
      pisos:$scope.pisos,
      nivel:$scope.numNivel,
      tipoInmueble:$scope.tipoInmueble,
      numPatios:$scope.numPatios
    }

  }

    registerData.property_info.options = [];

    for (var i = 0; i < $scope.options.length; i++) {
     var obj = {};
     var opt = "";
     if ($scope.options[i].model) {opt = "Si";} else{opt="No";};
     //obj[$scope.options[i].field] = opt;
     //registerData.property_info.push(JSON.stringify(obj));
     registerData.property_info[$scope.options[i].field] = opt;
      
    };

   for (var i = 0; i < $scope.unidadCerradaOptions.length; i++) {
      var obj = {};
      var opt = "";
      if ($scope.unidadCerradaOptions[i].model) {opt = "Si";} else{opt="No";};
       // obj[$scope.unidadCerradaOptions[i].field] = opt;
       //  registerData.property_info.push(JSON.stringify(obj));
       registerData.property_info[$scope.unidadCerradaOptions[i].field] = opt;
     
     
    };

    $('.js-required').each(function(){

      if($(this).val() == ""){
        valid = false;
      }
      if($(this).hasClass('email')){
        if(!isEmail($(this).val())){valid=false;}
      }
    });

    if(valid==true){
      RequestServices.postRequest( post_url, registerData )
      .success(function(data){
        console.log(data)
        if(data.result=="OK"){
          alertify
              .maxLogItems(1)
              .logPosition("bottom right")
              .alert("Formulario enviado con exito !!!");
          $('.js-type-text').val('');
          $('.js-type-select').val('.js-type-select option:first').val();
          $('.js-type-check').attr('checked', false); 
        }
        if(data.result=="ERROR"){
          alertify
              .maxLogItems(1)
              .logPosition("bottom right")
              .error(data.msg);
         
              
        }
      });
    }else{
       alertify
              .maxLogItems(1)
              .logPosition("bottom right")
              .error('Compruebe los campos obligatorios: Nombre - Telefono - Email, esten correctos.');
    }
    
  }

  $scope.logo = logoServices.getLogoResource();
  $scope.styleLogo = logoServices.getStyle();

  function init(){
    console.log($scope['parqueadero'] = true);
    console.log('-----');
    for (var i = 0; i < $scope.options.length; i++) {
      console.log($scope[$scope.options[i].model])
    };

  }
  init();

  $scope.sendForm = function(){
    $scope.formData = {
      
    }
  }





})