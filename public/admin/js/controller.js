var Controllers = angular.module('controllers', ['ngCookies', "angucomplete", "services", "digitalArchive", "digitalArchiveUpload",]);

Controllers.controller('loginCtrl', function ($scope, $cookies, $location, AdminServices, RequestServices, EmployeeServices) {

  $scope._loginForm = {
    // MODEL
    login: null,
    password: null,
    errors: [],

    // METHODS
    validate: function (cb) {

      this.errors = [];

      if (!this.login) {
        this.errors.push("Correo electrónico inválido/requerido.");
      }

      if (!this.password) {
        this.errors.push("Clave inválida/requerida");
      }

      cb(this.errors.length == 0);
      return this;
    },
    clear: function (cb) {
      this.errors = [];
      if (cb) cb();
      return this;
    }
  };

  $scope.doLogin = function () {
    $scope._loginForm.validate(function (valid) {
      if (valid) {
        var url = AdminServices.getPath() + "user/login";
        var params = {
          login: $scope._loginForm.login,
          password: $scope._loginForm.password
        };
        RequestServices.getRequest(url, params)
            .success(function (data) {

              if (data.result == "OK") {
                AdminServices.setSessionToken(data.token);
                AdminServices.setSessionRole(data.role);
                AdminServices.setSessionId(data.id);
                AdminServices.validateSession(function () {
                  $cookies.email = $scope._loginForm.login;
                  $location.path("/dashboard");
                });
                // $scope.init();
              } else {
                $scope._loginForm.errors.push(data.msg);
              }

            });
      }
    });
  };

  function init() {
    if ($cookies.email) {
      $scope._loginForm.login = $cookies.email;
    }
    AdminServices.loadFeatures();
    AdminServices.loadCities();
    AdminServices.loadListUser();
    AdminServices.loadPropertyTypes();
    AdminServices.loadAgents();
    EmployeeServices.load();
  }

  init();
});
// Comepleted!
Controllers.controller('dashboardCtrl', function ($scope, $location, AdminServices, RequestServices) {
  $scope.asesorasData = [];
  $scope.tipoData = [];

  var mainPath = AdminServices.getPath(),
      urlSales = mainPath + "contact-request/stats/sales",
      urlType = mainPath + "contact-request/stats/type",


      loadCharts = function () {

        RequestServices.getRequest(urlSales)
            .success(function (data) {
              $scope.asesorasData = data.sales;
            });

        RequestServices.getRequest(urlType)
            .success(function (data) {
              $scope.tipoData = data.sales;
            });
      };

  init = function () {
    AdminServices.validateSession();
    loadCharts();
    AdminServices.setUrlBack($location.url());
    AdminServices.setPropertiesViews([]);
  };

  init();
});

Controllers.controller('accountCtrl', function ($scope, $location, AdminServices, RequestServices) {
  $scope._session = null;
  var mainPath = AdminServices.getPath(),
      urlPassword = mainPath + "user/password/change";

  $scope._settingsSection = {

    newPassword: null,
    confirmNewPassword: null,
    currentPassword: null,
    changePassword: function () {
      var self = this;
      if (!self.currentPassword || !self.newPassword || !self.confirmNewPassword) {
        alertify.alert("Todos los campos son requeridos");
        return;
      }
      if (self.newPassword != self.confirmNewPassword) {
        alertify.alert("Las claves no coinciden");
        return;
      }
      var params = {
        token: $scope._session.token,
        ID: $scope._session.id,
        PASSWORD: self.newPassword,
        CURRENT: self.currentPassword
      };
      RequestServices.getRequest(urlPassword, params)
          .success(function (data) {
            if (data.result == "OK") {
              alertify.alert("Password changed successfully!");
            } else {
              alertify.alert(data.msg);
            }
            self.newPassword = null;
            self.confirmNewPassword = null;
            self.currentPassword = null;
          });
    }
  };

  init = function () {
    AdminServices.validateSession();
    $scope._session = AdminServices.getSession();
    AdminServices.setUrlBack($location.url());
    AdminServices.setPropertiesViews([]);
  };

  init();
});

// Contacto completed!

Controllers.controller('ContactCtrl', function ($scope, $routeParams, $location, AdminServices, RequestServices) {
  var mainPath = AdminServices.getPath(),
      urlContactList = mainPath + "contact-request/list";

  $scope._session = null;
  $scope._requestList = [];
  $scope.urlPath = $location.path() + "?";
  $scope.urlSearchPath = $location.path() + "?";
  $scope.contSymbol = "";

  $scope._contactSection = {
    errors: [],
    userList: [],
    typeList: [
      { value: "TODOS", label: "Todos" },
      { value: "WEB", label: "Página Web" },
      { value: "EMAIL", label: "Correo Electrónico" },
      { value: "TEL", label: "Llamada" },
      { value: "PERSONAL", label: "Visita" }
    ],
    typeMap: {
      TODOS: 0,
      WEB: 1,
      EMAIL: 2,
      TEL: 3,
      PERSONAL: 4
    },
    statusList: [
      { value: "PENDIENTE-PROCESANDO", label: "Pendiente/Procesando" },
      { value: "PENDIENTE", label: "Pendiente" },
      { value: "FINALIZADO", label: "Finalizado" },
      { value: "PROCESANDO", label: "Procesando" }
    ],
    statusMap: {
      'PENDIENTE-PROCESANDO': 0,
      PENDIENTE: 1,
      FINALIZADO: 2,
      PROCESANDO: 3
    },
    clear: function () {
      $scope._requestFilters.type = this.typeList[0];
      $scope._requestFilters.status = $scope._contactSection.statusList[0];
      $scope._requestFilters.contactName = null;
      $scope._requestFilters.userName = null;
      $scope._requestFilters.contactId = null;
      $scope._requestManager.search();
    },
    init: function () {
      if ($routeParams.page) {
        $scope._requestManager.page = parseInt($routeParams.page);
      }
      if ($routeParams.fStatus) {
        $scope._requestFilters.status = this.statusList[this.statusMap[$routeParams.fStatus]];
        $scope.urlPath += "fStatus=" + $scope._requestFilters.status.value;
        $scope.urlPath += "&";
      }
      if ($routeParams.fType) {
        $scope._requestFilters.type = this.typeList[this.typeMap[$routeParams.fType]];
        $scope.urlPath += "fType=" + $scope._requestFilters.type.value;
        $scope.urlPath += "&";
      } else {
        $scope._requestFilters.type = this.typeList[0];
      }
      if ($routeParams.fContactName) {
        $scope._requestFilters.contactName = $routeParams.fContactName;
        $scope.urlPath += "fContactName=" + $scope._requestFilters.contactName;
        $scope.urlPath += "&";
      }
      if ($routeParams.fAdviserName) {
        $scope._requestFilters.userName = $routeParams.fAdviserName;
        $scope.urlPath += "fAdviserName=" + $scope._requestFilters.userName;
        $scope.urlPath += "&";
      }
      if ($routeParams.fContactId) {
        $scope._requestFilters.contactId = $routeParams.fContactId;
        $scope.urlPath += "fContactId=" + $scope._requestFilters.contactId;
        $scope.contSymbol = "&";
      }
    }
  };

  $scope._requestFilters = {
    type: $scope._contactSection.typeList[0],
    status: $scope._contactSection.statusList[0],
    user: 0,
    contactName: null,
    userName: null,
    contactId: null
  };

  $scope._requestManager = {
    page: 1,
    totalPages: 1,

    loadPage: function () {
      $scope.loadContactRequests();
    },
    search: function () {
      $scope.urlSearchPath = $location.path() + "?";

      if ($scope._requestFilters.status.value && $scope._requestFilters.status.value != "") {
        $scope.urlSearchPath += "fStatus=" + $scope._requestFilters.status.value;
        $scope.urlSearchPath += "&";
      }
      if ($scope._requestFilters.type.value && $scope._requestFilters.type.value != "") {
        $scope.urlSearchPath += "fType=" + $scope._requestFilters.type.value;
        $scope.urlSearchPath += "&";
      }
      if ($scope._requestFilters.contactName && $scope._requestFilters.contactName != "") {
        $scope.urlSearchPath += "fContactName=" + $scope._requestFilters.contactName;
        $scope.urlSearchPath += "&";
      }
      if ($scope._requestFilters.userName && $scope._requestFilters.userName != "") {
        $scope.urlSearchPath += "fAdviserName=" + $scope._requestFilters.userName;
        $scope.urlSearchPath += "&";
      }
      if ($scope._requestFilters.contactId && $scope._requestFilters.contactId != "") {
        $scope.urlSearchPath += "fContactId=" + $scope._requestFilters.contactId;
      }
      $location.url($scope.urlSearchPath);
    }
  };

  $scope.loadContactRequests = function () {
    var params = {
      token: $scope._session.token,
      status: $scope._requestFilters.status.value,
      type: $scope._requestFilters.type.value,
      contact: $scope._requestFilters.contactName,
      ownerName: $scope._requestFilters.userName,
      contact_id: $scope._requestFilters.contactId,
      size: 10,
      page: $scope._requestManager.page
    };
    RequestServices.getRequest(urlContactList, params)
        .success(function (data) {
              if (data.result == "OK") {
                angular.forEach(data.requestList, function (value, key) {
                  value.status = $scope._contactSection.statusList[$scope._contactSection.statusMap[value.status]];
                  value.created = new Date(value.created);
                  value.updated = new Date(value.updated);
                });
                $scope._requestList = data.requestList;
                $scope._requestManager.totalPages = data.total;
              } else {
                alertify.alert(data.msg);
              }

            }
        );
  };

  init = function () {
    AdminServices.validateSession();
    $scope._session = AdminServices.getSession();
    $scope._contactSection.init();
    $scope.loadContactRequests();
    AdminServices.setUrlBack($location.url());
    AdminServices.setPropertiesViews([]);
  };

  init();
});

Controllers.controller('ContactDetailCtrl', function ($scope, $sce, $location, $routeParams, AdminServices, RequestServices, DateServices) {

  var mainPath = AdminServices.getPath(),
      urlFindRequest = mainPath + "agenda/findAllByRequest",
      urlAddRequest = mainPath + "prop-request/add",
      // url's contact Request
      urlSaveContact = mainPath + "contact-request/add",
      urlTransferContact = mainPath + "contact-request/assign",
      urlLoadRequest = mainPath + "contact-request/updates",
      urlUpdateRequest = mainPath + "contact-request/update",
      urlGetRequest = mainPath + "contact-request/newGet",
      urlGetReviews = mainPath + "contact-request/review/list",
      urlgetProperty = mainPath + "property/get";

  $scope._session = null;
  $scope.documentToRequest = null;
  $scope._requestComments = [];

  $scope._userList = [];
  $scope._userMap = [];
  $scope.requestError = [];

  $scope._requestUpdateForm = {
    comment: null
  };

  var validateRequest = function () {
    if (!$scope.documentToRequest) {
      $scope.requestError.push("El Documento es requerido");
      $scope.createMsg = true;
    }
    // if(!$scope.propToRequest){
    //   $scope.requestError.push("La Propiedad es requerida")
    //   $scope.createMsg = true;
    // }
  };

  $scope.clearRequest = function () {
    $scope.requestInfo = "";
    $scope.documentToRequest = null;
    $scope.propToRequest = null;
    $scope.requestError = [];
  };

  $scope.createRequest = function () {
    $scope.requestInfo = "";
    $scope.requestError = [];
    $scope.createMsg = false;
    validateRequest();
    if ($scope.createMsg) {
      return;
    }
    var params = {
      token: $scope._session.token,
      id: $scope.propToRequest
    };
    var paramsAddRequest = {
      token: $scope._session.token,
      contact: $scope._contactSection.request.id,
      document: $scope.documentToRequest,
      property: $scope.propToRequest,
      employee: $scope._session.token
    };
    var addRequest = function () {
      RequestServices.getRequest(urlAddRequest, paramsAddRequest)
          .success(function (data) {
            // console.info(data)
            if (data.result == "OK") {
              $scope.createMsg = true;
              $scope.requestInfo = "Se ha comenzado un proceso de solicitud, su número de radicado es: " + data.request_identifer + " por favor diligencie la documentación asociada al proceso.";
            }
          });
    };
    if ($scope.propToRequest) {
      RequestServices.getRequest(urlgetProperty, params)
          .success(function (data) {
            console.warn(data)
            if (data.result == "ERROR") {
              $scope.requestError.push("La Propiedad no es valida");
              $scope.createMsg = true;
            } else {
              addRequest();
            }
          });
    } else {
      addRequest();
    }

  };

  $scope.detailReview = function (review) {
    $scope.reviewSelected = review;
    $('#reviewsDetail').foundation('reveal', 'open');
  };

  function loadReviews() {
    params = {
      token: $scope._session.token,
      CONTACT_ID: $routeParams.contactId
    };
    RequestServices.getRequest(urlGetReviews, params)
        .success(function (data) {
          if (data.result == "OK") {
            angular.forEach(data.results, function (review) {
              var i = 1;
              var aux = 0;
              while (review['P' + i]) {
                aux = aux + review['P' + i];
                i++;
              }
              review.RATING = aux / (i - 1);
              console.log("i--->" + i);
              console.log(review.RATING);

            })
            $scope._requestReviews = data.results;
          } else {
            alertify.alert(data.msg);
          }

        });

  }

  $scope.loadRequestUpdates = function () {
    params = {
      token: $scope._session.token,
      request: $scope._contactSection.request.id
    }
    RequestServices.getRequest(urlLoadRequest, params)
        .success(function (data) {

              if (data.result == "OK") {
                angular.forEach(data.updates, function (comment, key) {
                  if (comment.comment) {
                    var strInit = comment.comment;
                    if (strInit.indexOf("#") != -1) {
                      var startCode = strInit.indexOf("#");
                      var str = strInit.substring(startCode);
                      var strToE = str.substring(1);
                      var endCode = 0;
                      if (strToE.search(/(\s|\D)/) != -1) {
                        endCode = strToE.search(/(\s|\D)/) + 1;
                      } else {
                        endCode = str.length;
                      }
                      var strToReplace = str.substring(0, endCode);
                      var newMessage = strInit.replace(strToReplace, "<a href='#/Properties/" + str.substring(1, endCode) + "'>" + strToReplace + "</a>");
                      comment.comment = $sce.trustAsHtml(newMessage.split("\n").join("<br>"));
                    } else {
                      comment.comment = $sce.trustAsHtml(comment.comment.split("\n").join("<br>"));
                    }
                  }
                })
                $scope._requestComments = data.updates;
              } else {
                alertify.alert(data.msg);
              }

            }
        );
  };

  $scope.updateRequest = function (state) {
    params = {
      request: $scope._contactSection.request.id,
      status: state,
      token: $scope._session.token,
      comment: $scope._requestUpdateForm.comment ? $scope._requestUpdateForm.comment : ""
    }

    RequestServices.getRequest(urlUpdateRequest, params)
        .success(function (data) {
              if (data.result == "OK") {
                $scope.loadRequestUpdates();
                $scope._contactSection.request.status = state;
                $scope._requestUpdateForm.comment = null;
              } else {
                alertify.alert(data.msg);
              }

            }
        );
  };

  $scope.getContactRequest = function () {
    var params = {
      token: $scope._session.token,
      id: $routeParams.contactId
    }
    RequestServices.getRequest(urlGetRequest, params)
        .success(function (data) {
          if (data.result == "OK") {
            $scope._contactSection.request = data.contact;
            $scope._contactSection.requestDetail();
          } else {
            alertify.alert(data.msg);
          }
        });
  };

  $scope._contactSection = {
    errors: [],
    userList: [],
    typeList: [
      { value: null, label: "Todos" },
      { value: "WEB", label: "Página Web" },
      { value: "EMAIL", label: "Correo Electrónico" },
      { value: "TEL", label: "Llamada" },
      { value: "PERSONAL", label: "Visita" }
    ],
    newList: [
      { value: "EMAIL", label: "Correo Electrónico" },
      { value: "TEL", label: "Llamada" },
      { value: "PERSONAL", label: "Visita" }
    ],
    heardOptions: [
      "AGENCIA",
      "AVISOS",
      "PAGINA WEB",
      "FINCA RAIZ",
      "CIENCUADRAS",
      "REFERIDO",
      "FACEBOOK",
      "INSTAGRAM",
      "REUBICADOS"
    ],
    typeMap: {
      WEB: "Página Web",
      EMAIL: "Correo Electrónico",
      TEL: "Llamada",
      PERSONAL: "Visita"
    },
    statusList: [
      { value: "TODOS", label: "Todos" },
      { value: "PENDIENTE", label: "Pendiente" },
      { value: "FINALIZADO", label: "Finalizado" },
      { value: "PROCESANDO", label: "Procesando" }
    ],
    statusMap: {
      PENDIENTE: 1,
      FINALIZADO: 2,
      PROCESANDO: 3
    },
    request: null,
    readOnly: true,
    assignTo: null,
    isType: function (type) {
      if (!this.request) {
        return false;
      }
      var tmp = this.request.type;
      if (tmp.value) {
        tmp = tmp.value;
      }
      return (tmp == type);
    },
    transferRequest: function () {
      var self = this;
      //owner, id, token
      var params = {
        token: $scope._session.token,
        id: self.request.id,
        owner: self.assignTo.ID
      };

      RequestServices.getRequest(urlTransferContact, params)
          .success(function (data) {
                if (data.result == "OK") {
                  alertify.alert("Solicitud Asignada.");
                  $location.path("/Contact");
                } else {
                  alertify.alert(data.msg);
                }

              }
          );
    },
    requestDetail: function () {
      var self = this;
      var newMessage = "";
      this.readOnly = true;
      this.assignTo = $scope._userMap[$scope._session.id];
      if (this.request.message) {
        var strInit = this.request.message;
        if (strInit.indexOf("#") != -1) {
          var startCode = strInit.indexOf("#");
          var str = strInit.substring(startCode);
          var strToE = str.substring(1);
          var endCode = 0;
          if (strToE.search(/(\s|\D)/) != -1) {
            endCode = strToE.search(/(\s|\D)/) + 1;
            console.log(str.substring(0, endCode))
          } else {
            endCode = str.length;
          }
          var strToReplace = str.substring(0, endCode);
          newMessage = strInit.replace(strToReplace, "<a href='#/Properties/" + str.substring(1, endCode) + "'>" + strToReplace + "</a>");
        } else {
          newMessage = this.request.message;
        }
      }
      this.request.htmlMessage = this.request.message ? ($sce.trustAsHtml("<p>" + newMessage.split("\n").join("<br>") + "</p>")) : "";
      $scope.loadRequestUpdates();
      loadReviews();

      var params = {
        contact: self.request.id
      }

      RequestServices.getRequest(urlFindRequest, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.request.appointments = [];
              angular.forEach(data.appointments, function (app, key) {
                app.SCHEDULED_LABEL = app.SCHEDULED.split("-").join("/")
                app.CREATED = new Date(app.CREATED);
                app.TIME_FROM = app.TIME_FROM.substring(0, 5);
                app.TIME_TO = app.TIME_TO.substring(0, 5);
                self.request.appointments.push(app);
              });
            } else {
              alertify.alert(data.msg);
            }
          });
    },
    init: function () {
      if ($routeParams.contactId) {
        $scope.getContactRequest();
      } else {
        this.readOnly = false;
        this.request = {
          type: this.newList[2],
          created: new Date(),
          status: this.statusList[1]
        };
      }
    },
    save: function () {
      var self = this;
      self.errors = [];
      if (!self.request.name) {
        self.errors.push("El nombre es requerido")
      }
      if (!self.request.phone) {
        self.errors.push("El Teléfono es requerido")
      }
      if (!self.request.medio) {
        self.errors.push("Por qué medio se enteró es requerido")
      }
      if (self.request.type.value == "EMAIL" && !self.request.email) {
        self.errors.push("El email es requerido")
      }
      if (self.errors.length) {
        return;
      }
      params = {
        name: self.request.name,
        email: self.request.email,
        phone: self.request.phone,
        msg: self.request.message,
        type: self.request.type.value,
        user: $scope._session.id,
        medio: self.request.medio
      }

      RequestServices.getRequest(urlSaveContact, params)
          .success(function (data) {
            if (data.result == "OK") {
              $location.path("/Contact/" + data.id);
            } else {
              alertify.alert(data.msg);
            }
          });
    }
  };

  $scope.hourBasicFromText = function (milHour) {
    return DateServices.hourBasicFromText(milHour);
  }

  function init() {
    $(document).foundation();
    AdminServices.validateSession();
    $scope._session = AdminServices.getSession();
    $scope._userList = AdminServices.getListUser();
    $scope._userMap = AdminServices.getUserMap();
    $scope._contactSection.init();
    AdminServices.setUrlBack($location.url());
    AdminServices.setPropertiesViews([]);
  }

  init();
});

// Agenda completed

Controllers.controller('agendaCtrl', function ($scope, $location, AdminServices, RequestServices, DateServices) {
  var mainPath = AdminServices.getPath(),
      urlOverview = mainPath + "agenda/overview";
  $scope._session = null;

  $scope._agenda = {

    date: null,
    overviewDate: null,
    dayAppointments: [],
    overviewAppointments: [],

    overview: function () {
      var self = this;

      if (self.overviewDate == null) {
        self.overviewDate = DateServices.formatDate(new Date());
      }

      params = {
        scheduled: DateServices.dateAsText(DateServices.parseDate(self.overviewDate))
      }

      RequestServices.getRequest(urlOverview, params)
          .success(function (data) {
            if (data.result == "OK") {

              angular.forEach(data.appointments, function (appointment, idx) {
                var sch = DateServices.parseDate(appointment.SCHEDULED);
                var tmp = appointment.TIME_FROM.split(":");
                var fromTime = new Date(sch.getTime());
                fromTime.setHours(parseInt(tmp[0]));
                fromTime.setMinutes(parseInt(tmp[1]));
                var suffix = "am";
                if (parseInt(tmp[0]) > 12) {
                  tmp[0] = parseInt(tmp[0]) - 12;
                  suffix = "pm";
                }
                appointment.from = DateServices.zeroPad(tmp[0], 2) + ":" + DateServices.zeroPad(tmp[1], 2) + " " + suffix;

                tmp = appointment.TIME_TO.split(":");
                var endTime = new Date(sch.getTime());
                endTime.setHours(parseInt(tmp[0]));
                endTime.setMinutes(parseInt(tmp[1]));

                suffix = "am";

                if (parseInt(tmp[0]) > 12) {
                  tmp[0] = parseInt(tmp[0]) - 12;
                  suffix = "pm";
                }

                appointment.to = DateServices.zeroPad(tmp[0], 2) + ":" + DateServices.zeroPad(tmp[1], 2) + " " + suffix;
              });

              self.overviewAppointments = data.appointments;
            }
          });
    },

    init: function () {
      this.overview()
    }
  };

  function init() {
    AdminServices.validateSession();
    $(".date").fdatepicker({ language: "es" });
    $scope._session = AdminServices.getSession();
    $scope._agenda.init();
    AdminServices.setUrlBack($location.url());
    AdminServices.setPropertiesViews([]);
  }

  init();
});

Controllers.controller('agendaMasterCtrl', function ($scope, $sce, $routeParams, $location, AdminServices, RequestServices, DateServices) {
  var mainPath = AdminServices.getPath(),
      // url's
      urlOwnerList = mainPath + "owner/list",
      urlGetAgenda = mainPath + "agenda/list",
      urlGetRequest = mainPath + "contact-request/newGet",
      // url's appoiments
      urlSearchCustomer = mainPath + "contact-request/get",
      urlSearchListCustomer = mainPath + "contact-request/list",
      urlSearchProperty = mainPath + "property/list",
      urlCancelAppoiments = mainPath + "agenda/cancel",
      urlConfirmAppoiments = mainPath + "agenda/confirm",
      urlSaveAppoiments = mainPath + "agenda/add";

  $scope._session = null;

  $scope._agenda = {

    date: null,
    initDay: null,
    overviewDate: null,
    grid: [],
    agents: [],
    dates: {},
    agentsMap: {},
    agent: null,
    currentCell: null,
    currentContact: null,
    dayAppointments: [],

    appointment: {
      toOptions: [],
      customers: [],
      properties: [],
      fromTime: null,
      keysOptions: ["AGENCIA", "OTRO"],
      endTime: null,
      endTimeVal: null,
      comments: null,
      customerId: null,
      customerError: null,
      propertyCode: null,
      property: null,
      propertyError: null,
      place: "AGENCIA",
      places: [
        "AGENCIA"
      ],
      codeTypes: [
        "CITA CONTACTO",
        "BÚSQUEDA PROPIEDADES",
        "NO DISPONIBLE",
        "DILIGENCIAS ADMINISTRATIVAS"
      ],

      endTimeText: function () {

        if (this.endTime == null) {
          return "";
        }

        var suffix = " am";
        var end = this.endTime.getHours();
        if (end > 12) {
          end = end - 12;
          suffix = " pm";
        }

        return DateServices.zeroPad(end, 2)
            + ":" + DateServices.zeroPad(this.endTime.getMinutes(), 2) + suffix
      },
      addCustomer: function () {
        var self = this;
        self.customer.NAME = self.customer.name;
        self.customer.PHONE = self.customer.phone;
        self.customer.EMAIL = self.customer.email;
        self.customer.ID = self.customer.id;
        self.customers.push(self.customer);
        self.customer = null;
        self.customerList = null;
        self.customerId = null;
        self.customError = null;

        if ($scope._agenda.currentContact && $scope._agenda.currentContact == self.customerId) {
          $scope._agenda.currentContact = null;
        }
      },
      searchCustomer: function () {

        var self = this;

        self.customerError = "";
        self.customer = null;

        if (!self.customerId) {
          this.customerError = "Por favor ingrese el código de la solicitud.";
          return;
        }

        for (var i = 0; i < self.customers.length; i++) {
          if (self.customers[i].ID == self.customerId) {
            self.customerError = "El contacto de la solicitud " + self.customerId + " ya fue citado";
            return;
          }
        }
        var code = self.customerId, str = parseInt(code);

        if (isNaN(str)) {
          var params = {
            contact: code
          }
        } else {
          var params = {
            contact_id: code
          }
        }

        RequestServices.getRequest(urlSearchListCustomer, params)
            .success(function (data) {
              if (data.result == "OK") {

                if (data.requestList) {
                  self.customerList = data.requestList;
                  self.customer = self.customerList[0];
                } else {
                  self.customer = null;
                  self.customerError = "Código de solicitud inválido, intente de nuevo.";
                }
              }
            });
      },
      removeCustomer: function (idx) {
        this.customers.splice(idx, 1);
      },
      clearSearchCustomer: function () {
        var self = this;
        self.customer = null;
        self.customerId = null;
        self.customError = null;
        if ($scope._agenda.currentContact) {
          self.customerId = $scope._agenda.currentContact.id;
          self.searchCustomer();
        }
      },
      addProperty: function () {
        var self = this;
        self.places.push(self.property.CODE);
        self.properties.push(self.property);
        self.property = null;
        self.propertyCode = null;
        self.keys = "AGENCIA";
        self.propertyError = null;
      },
      removeProperty: function (idx) {
        var self = this;
        self.properties.splice(idx, 1);
        self.places.splice(idx + 1, 1);
        self.place = "AGENCIA";
      },
      clearSearchProperty: function () {
        var self = this;
        self.propertyError = null;
        self.property = null;
        self.keys = "AGENCIA";
        self.propertyCode = null;
      },
      searchProperty: function () {
        var self = this;
        self.propertyError = null;
        self.property = null;
        if (!self.propertyCode) {
          self.propertyError = "Por favor ingrese el código a buscar.";
          return;
        }
        self.propertyCode = self.propertyCode.toUpperCase();
        for (var i = 0; i < self.properties.length; i++) {
          console.log(self.properties[i].CODE + "==" + self.propertyCode);
          if (self.properties[i].CODE == self.propertyCode) {
            self.propertyError = "El código " + self.propertyCode + " ya fue asignado";
            return;
          }
        }
        var params = {
          internal: isNaN(self.propertyCode) ? self.propertyCode : null,
          code: isNaN(self.propertyCode) ? null : self.propertyCode,
          page: 1,
          size: 1
        }

        RequestServices.getRequest(urlSearchProperty, params)
            .success(function (data) {
              if (data.result == "OK") {
                if (data.properties.length) {
                  self.property = data.properties[0];
                  self.property.keys = "AGENCIA";
                } else {
                  self.property = null;
                  self.propertyError = "Código inválido, intente de nuevo.";
                }
              }
            });
      },
      cancelAppointment: function () {

        var self = this,
            params = {
              appointment: $scope._agenda.currentCell.appointment.ID
            }


        if ($scope._agenda.currentCell.appointment.CONSULTANT_ID == $scope._session.id) {
          RequestServices.getRequest(urlCancelAppoiments, params)
              .success(function (data) {
                if (data.result == "OK") {
                  $scope._agenda.rebuildGrid();
                } else {
                  alertify.alert("ERROR:" + data.msg);
                }
              });
        } else {
          alertify.alert("ERROR: No esta autorizado para cancelar esta cita.");
        }


      },
      confirmAppointment: function () {
        var self = this,
            params = {
              appointment: $scope._agenda.currentCell.appointment.ID
            }
        RequestServices.getRequest(urlConfirmAppoiments, params)
            .success(function (data) {
              if (data.result == "OK") {
                $scope._agenda.rebuildGrid();
              } else {
                alertify.alert("ERROR:" + data.msg);
              }
            });
      },
      saveAppointment: function () {
        var self = this;
        var contacts = [];
        // join contacts if any
        for (var i = 0; i < self.customers.length; i++) {
          contacts.push(self.customers[i].ID);
        }
        var properties = [];
        // join properties if any
        for (var i = 0; i < self.properties.length; i++) {
          properties.push(self.properties[i].ID + "-" + self.properties[i].keys);
        }
        var params = {
          id: $scope._agenda.currentCell.appointment ? $scope._agenda.currentCell.appointment.ID : null,
          promoter: $scope._agenda.agent.ID,
          consultant: $scope._session.id,
          scheduled: DateServices.dateAsText(self.fromTime),
          time_from: DateServices.getTime(self.fromTime),
          time_to: DateServices.getTime(self.endTime),
          comments: self.comments,
          place: self.place,
          contacts: contacts.join(","),
          properties: properties.join(",")
        };

        if (!$scope._agenda.currentCell.appointment) {
          // NUEVO APPOIMENT
          saveAppointment();
        } else if ($scope._agenda.currentCell.appointment.CONSULTANT_ID == $scope._session.id) {
          // UPDATE APPOIMENT
          saveAppointment();
        } else {
          alertify.alert("ERROR: No esta autorizado para Guardar esta cita.");
        }

        function saveAppointment() {
          RequestServices.getRequest(urlSaveAppoiments, params)
              .success(function (data) {
                if (data.result == "OK") {
                  self.places =
                      $scope._agenda.rebuildGrid();
                } else {
                  alertify.alert("ERROR:" + data.msg);
                }
              });
        }
      }
    },
    getContactRequest: function () {
      var params = {
            token: $scope._session.token,
            id: $routeParams.contactId
          },
          self = this;
      RequestServices.getRequest(urlGetRequest, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.currentContact = data.contact;
            } else {
              alertify.alert(data.msg);
            }
          });
    },
    initClear: function () {
      this.currentContact = null;
      this.init();
    },
    init: function () {
      if ($routeParams.contactId) {
        this.getContactRequest();
      } else {
        this.currentContact = null;
      }
      var self = this;
      var day = new Date();
      if ($routeParams.fDay) {
        day.setDate($routeParams.fDay)
      };
      if ($routeParams.fMonth) {
        var month = $routeParams.fMonth - 1;
        day.setMonth(month)
      };
      if ($routeParams.fYear) {
        day.setFullYear($routeParams.fYear);
      };

      this.initDay = day;
      self.date = (DateServices.zeroPad(day.getMonth() + 1, 2) + "/" + DateServices.zeroPad(day.getDate(), 2) + "/" + DateServices.zeroPad(day.getFullYear()));
      self.loadAgents();
    },
    cellClicked: function (cell) {

      var self = this;
      //alertify.alert($scope.getDayName(cell.from)+"["+cell.from.toLocaleString()+"]" + "->" + cell.from.getHours() + ":" + cell.from.getMinutes() + "-" + cell.to.getHours() + ":" + cell.to.getMinutes());
      self.currentCell = cell;

      // build timeTo options from starting time and default to now + 1 delta
      var endDate = new Date(cell.to.getTime());
      var fromDate = new Date(cell.to.getTime());
      var currentEnd = new Date(cell.to.getTime());

      if (cell.appointment) {
        currentEnd.setHours(cell.appointment.TIME_TO.split(":")[0])
        currentEnd.setMinutes(cell.appointment.TIME_TO.split(":")[1])

        self.appointment.fromTime = new Date(cell.from);
        self.appointment.fromTime.setHours(cell.appointment.TIME_FROM.split(":")[0]);
        self.appointment.fromTime.setMinutes(cell.appointment.TIME_FROM.split(":")[1]);
      } else {
        self.appointment.fromTime = new Date(cell.from);
      }

      self.appointment.toOptions = [];
      self.appointment.customers = [];
      self.appointment.properties = [];
      self.appointment.comments = self.currentCell.appointment ? self.currentCell.appointment.COMMENTS : null;
      self.appointment.place = self.currentCell.appointment ? self.currentCell.appointment.place : "AGENCIA";

      endDate.setHours(18);
      endDate.setMinutes(0);

      self.appointment.places = ["AGENCIA"];

      self.appointment.endTime = null;

      while (fromDate.getHours() < endDate.getHours() || ((fromDate.getHours() == endDate.getHours()) && fromDate.getMinutes() <= endDate.getMinutes())) {

        self.appointment.toOptions.push(fromDate);

        if (fromDate.getHours() == currentEnd.getHours() && fromDate.getMinutes() == currentEnd.getMinutes()) {
          console.log(fromDate + " is equal" + currentEnd);
          self.appointment.endTime = fromDate;
        }

        // fi there is an app starting on the endtime then break the cycle;
        if ($scope._agenda.dates[DateServices.dateAsText(fromDate)] && $scope._agenda.dates[DateServices.dateAsText(fromDate)][DateServices.timeText(fromDate)]) {
          var tmpCell = $scope._agenda.dates[DateServices.dateAsText(fromDate)][DateServices.timeText(fromDate)];

          console.log(tmpCell ? (tmpCell.appointment ? (self.currentCell.appointment ? (tmpCell.appointment.ID != self.currentCell.appointment.ID ? "DIFFERENT" : "SAME") : "NO APP CURRENT") : "NO APP") : "NO CELL");

          if (tmpCell && tmpCell.appointment && (!self.currentCell.appointment || (self.currentCell.appointment && tmpCell.appointment.ID != self.currentCell.appointment.ID))) {
            break;
          }

        }

        fromDate = DateServices.addMinutes(fromDate, 15);

      }

      if (self.appointment.endTime == null) {
        self.appointment.endTime = self.appointment.toOptions[0];
      }

      if (self.currentCell.appointment) {
        self.appointment.customers = self.currentCell.appointment.contacts;
        self.appointment.properties = self.currentCell.appointment.properties;
        self.appointment
        self.appointment.places = ["AGENCIA"];
        angular.forEach(self.appointment.properties, function (prop, idx) {
          self.appointment.places.push(prop.CODE);
        });
        self.appointment.place = self.currentCell.appointment.PLACE
      } // if
    },
    rebuildGrid: function () {
      var self = this;
      var columns = 1;
      self.setupGrid(columns, 15);
    },
    setupGrid: function (extraDays, delta) {
      var self = this;
      if (self.date == null) {
        return;
      }
      self.grid.splice(0, self.grid.length);
      var fromDate = DateServices.parseDate(self.date);
      var maxDelta = 10 * (60 / delta);
      var day = new Date(+fromDate);

      for (var k = 0; k < extraDays; k++) {
        day.setDate(fromDate.getDate() + k);
        day.setHours(8, 0, 0, 0);
        // console.log(k + ":Day:" + DateServices.getDayName(day));
        var column = {
          cells: [],
          dayName: DateServices.getDayName(day),
          date: new Date(+day),
          dateText: function () {
            return DateServices.dateAsText(this.date)
          }
        };
        self.grid.push(column);
        self.dates[column.dateText()] = {};
        for (var i = 0; i < maxDelta; i++) {
          var from = new Date(day.getTime() + (i * delta) * 60000);
          var to = new Date(day.getTime() + ((i + 1) * delta) * 60000);
          var cell = {
            from: from,
            to: to,
            status: 0,
            patient: null,
            fromText: function () {
              return DateServices.zeroPad(this.from.getHours(), 2)
                  + ":" + DateServices.zeroPad(this.from.getMinutes(), 2);
            },
            toText: function () {
              return DateServices.zeroPad(this.to.getHours(), 2)
                  + ":" + DateServices.zeroPad(this.to.getMinutes(), 2)
            },
            dateText: function () {
              return DateServices.zeroPad(this.from.getFullYear(), 4)
                  + "-" + DateServices.zeroPad(this.from.getMonth() + 1, 2)
                  + "-" + DateServices.zeroPad(this.from.getDate(), 2)
            }
          };
          self.grid[k].cells.push(cell);
          if (cell.to.getTime() < new Date().getTime()) {
            cell.editable = false;
          } else {
            cell.editable = true;
          }
          self.dates[column.dateText()][cell.fromText()] = cell;
        }
        console.log(column);
        self.loadDateAgenda(column.dateText());
      }
    },
    loadDateAgenda: function (date) {

      var self = this;
      var params = {
        promoter: $scope._agenda.agent.ID,
        scheduled: date
      };

      if (self.agent == null) {
        return;
      }

      console.log(urlGetAgenda, params);
      RequestServices.getRequest(urlGetAgenda, params)
          .success(function (data) {
            if (data.result == "OK") {
              angular.forEach(data.appointments, function (appointment, key) {

                var sch = DateServices.parseDate(appointment.SCHEDULED);
                var tmp = appointment.TIME_FROM.split(":");
                var fromTime = new Date(sch.getTime());
                fromTime.setHours(parseInt(tmp[0]));
                fromTime.setMinutes(parseInt(tmp[1]));

                var suffix = "am";
                if (parseInt(tmp[0]) > 12) {
                  tmp[0] = parseInt(tmp[0]) - 12;
                  suffix = "pm";
                }
                appointment.from = tmp[0] + ":" + tmp[1] + " " + suffix;

                tmp = appointment.TIME_TO.split(":");
                var endTime = new Date(sch.getTime());
                endTime.setHours(parseInt(tmp[0]));
                endTime.setMinutes(parseInt(tmp[1]));

                suffix = "am";

                if (parseInt(tmp[0]) > 12) {
                  tmp[0] = parseInt(tmp[0]) - 12;
                  suffix = "pm";
                }

                appointment.to = tmp[0] + ":" + tmp[1] + " " + suffix;

                var appStatus = null;

                switch (appointment.STATUS) {
                  case "SCHEDULED":
                    appStatus = 1;
                    break;
                  case "CANCELLED":
                    appStatus = 4;
                    break;
                  case "CONFIRMED":
                    appStatus = 2;
                    break;
                  default:
                    appStatus = 0;
                }

                while (fromTime.getHours() < endTime.getHours() || ((fromTime.getHours() == endTime.getHours()) && fromTime.getMinutes() < endTime.getMinutes())) {

                  var from = DateServices.zeroPad(fromTime.getHours(), 2) + ":" + DateServices.zeroPad(fromTime.getMinutes(), 2);
                  var cell = self.dates[appointment.SCHEDULED][from];

                  if (!cell) {
                    console.log("CELL NOT FOUND:" + appointment.SCHEDULED + "@" + from);
                  }

                  cell.status = appStatus;
                  cell.appointment = appointment;
                  fromTime = DateServices.addMinutes(fromTime, 15);
                }
              });

              // console.log(date + "==" + self.date + "=?" + date == DateServices.dateAsText(DateServices.parseDate(self.date)));
              if (date == DateServices.dateAsText(DateServices.parseDate(self.date))) {
                // console.log("replace");
                self.dayAppointments = data.appointments;
              }

            } else {
              alertify.alert("Problemas al cargar citas del agente: " + data.msg);
            }

          });
    },
    loadAgents: function () {
      var self = this;
      if ($routeParams.fAgent) {
        self.agent = self.agents[self.agentsMap[$routeParams.fAgent]];
      } else {
        self.agent = self.agents[0];
      }
      self.rebuildGrid();
    },
    agentChanged: function () {
      this.dateChanged();
    },
    nextDay: function () {
      var day = DateServices.parseDate(this.date);
      day.setDate(day.getDate() + 1);

      var Day = day.getDate();
      var Month = day.getMonth();
      var Year = day.getFullYear();
      this.searchByDate(Day, Month, Year)
    },
    prevDay: function () {
      var day = DateServices.parseDate(this.date);
      day.setDate(day.getDate() - 1);

      var Day = day.getDate();
      var Month = day.getMonth();
      var Year = day.getFullYear();
      this.searchByDate(Day, Month, Year);
    },
    nextWeek: function () {
      var day = DateServices.parseDate(this.date);
      day.setDate(day.getDate() + 7);

      var Day = day.getDate();
      var Month = day.getMonth();
      var Year = day.getFullYear();
      this.searchByDate(Day, Month, Year);
    },
    prevWeek: function () {
      var day = DateServices.parseDate(this.date);
      day.setDate(day.getDate() - 7);

      var Day = day.getDate();
      var Month = day.getMonth();
      var Year = day.getFullYear();
      this.searchByDate(Day, Month, Year);
    },
    nextMonth: function () {
      var day = DateServices.parseDate(this.date);
      day.setMonth(day.getMonth() + 1);

      var Day = day.getDate();
      var Month = day.getMonth();
      var Year = day.getFullYear();
      this.searchByDate(Day, Month, Year);
    },
    prevMonth: function () {
      var day = DateServices.parseDate(this.date);
      day.setMonth(day.getMonth() - 1);

      var Day = day.getDate();
      var Month = day.getMonth();
      var Year = day.getFullYear();
      this.searchByDate(Day, Month, Year);
    },
    nextYear: function () {
      var day = DateServices.parseDate(this.date);
      day.setFullYear(day.getFullYear() + 1);

      var Day = day.getDate();
      var Month = day.getMonth();
      var Year = day.getFullYear();
      this.searchByDate(Day, Month, Year);
    },
    prevYear: function () {
      var day = DateServices.parseDate(this.date);
      day.setFullYear(day.getFullYear() - 1);

      var Day = day.getDate();
      var Month = day.getMonth();
      var Year = day.getFullYear();
      this.searchByDate(Day, Month, Year);
    },
    searchByDate: function (day, month, year) {
      var urlPath = $location.path() + "?fDay=" + day + "&fMonth=" + (month + 1) + "&fYear=" + year + "&fAgent=" + this.agent.ID;
      // console.log( $location.path() );
      $location.url(urlPath);
    },
    dateChanged: function () {
      var day = DateServices.parseDate(this.date);
      var Day = day.getDate();
      var Month = day.getMonth();
      var Year = day.getFullYear();
      this.searchByDate(Day, Month, Year);
    }
  };

  $scope.hourText = function (date) {
    return DateServices.hourText(date);
  }

  $scope.hourBasicFromText = function (milHour) {
    return DateServices.hourBasicFromText(milHour);
  }
  $scope.getDayName = function (date) {
    return DateServices.getDayName(date)
  }

  $scope.getMonthName = function (date) {
    return DateServices.getMonthName(date)
  }

  function init() {
    $(document).foundation();
    AdminServices.validateSession();
    $(".date").fdatepicker({ language: "es" });
    $scope._session = AdminServices.getSession();
    $scope._agenda.agents = AdminServices.getAgents();
    $scope._agenda.agentsMap = AdminServices.getAgentsMap();
    $scope._agenda.init();
    AdminServices.setUrlBack($location.url());
    AdminServices.setPropertiesViews([]);
    $scope.$watch('_agenda.date', function () {
      if ($scope._agenda.agent) {
        $scope._agenda.rebuildGrid();
      }
    });
  }

  init();
});

// Propiedades

Controllers.controller('propertiesCtrl', function ($scope, $location, $routeParams, AdminServices, RequestServices, DateServices) {
  var mainPath = AdminServices.getPath(),
      urlPropList = mainPath + "property/list",
      urlNeighborhood = mainPath + "address/neighborhood/options";


  var statusMap = {
    "todos": 0,
    1: 1,
    99: 2,
    0: 3,
    4: 4,
    2: 5,
    3: 6,
    5: 7
  };

  var propMap = {},
      citiesMap = {};

  $scope.urlPath = "#/Properties?";
  $scope.urlSearchPath = "/Properties?";
  $scope.contSymbol = "";

  $scope._session = null;
  $scope._cities = [];
  $scope._propertyTypes = [];
  $scope._propertyTypesMap = [];

  $scope._statusList = AdminServices.getStatusList();
  $scope._statusMap = {}
  $scope.boolOptions = [
    { value: null, label: "Todos" },
    { value: "true", label: "Con" },
    { value: "false", label: "Sin" }
  ];
  $scope._propSection = {

    prop: null,
    bkp: null,
    details: null,
    photos: [],
    cityFilter: null,
    neighborhoodFilter: null,
    statusFilter: $scope._statusList[2],
    typeFilter: null,
    addressFilter: null,
    codeFilter: null,
    internalFilter: null,
    editAddress: false,
    priceFrom: null,
    priceTo: null,
    areaFrom: null,
    areaTo: null,
    roomsFilter: null,
    closetsFilter: null,
    wcFilter: null,

    condoFilter: null,
    items: [],
    errors: [],
    msg: "",
    page: 1,
    totalPages: 1,
    size: 10,
    readOnly: true,
    cityNeighborhoods: [],
    neighborhoods: [],
    neighborhoodsMap: {},
    cityModel: {},
    neighModel: {},
    employeeModel: {},
    retireComment: null,


    parkingFilter: $scope.boolOptions[0],
    poolFilter: $scope.boolOptions[0],
    elevatorFilter: $scope.boolOptions[0],
    hotFilter: $scope.boolOptions[0],
    unitFilter: $scope.boolOptions[0],

    clear: function () {
      this.statusFilter = $scope._statusList[2];

      if ($scope._cities.length > 0) {
        this.cityFilter = $scope._cities[0];
        this.loadCityNeighborhoods();
      };

      if ($scope._propertyTypes.length > 0) {
        this.typeFilter = $scope._propertyTypes[0];
      };

      this.codeFilter = null;
      this.addressFilter = null;
      this.internalFilter = null;
      this.priceFrom = null;
      this.priceTo = null;
      this.areaFrom = null;
      this.areaTo = null;
      this.roomsFilter = null;
      this.closetsFilter = null;
      this.wcFilter = null;
      this.condoFilter = null;
      this.neighborhoodFilter = null;
      this.parkingFilter = $scope.boolOptions[0];
      this.poolFilter = $scope.boolOptions[0];
      this.elevatorFilter = $scope.boolOptions[0];
      this.hotFilter = $scope.boolOptions[0];
      this.unitFilter = $scope.boolOptions[0];
      return;
    },
    init: function () {
      if ($routeParams.page) {
        this.page = parseInt($routeParams.page);
      }
      //listos
      if ($routeParams.fInternal) {
        this.internalFilter = $routeParams.fInternal;
        $scope.urlPath += "fInternal=" + this.internalFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fCode) {
        this.codeFilter = $routeParams.fCode;
        $scope.urlPath += "fCode=" + this.codeFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fStatus) {
        this.statusFilter = $scope._statusList[statusMap[$routeParams.fStatus]];
        $scope.urlPath += "fStatus=" + this.statusFilter.value;
        $scope.urlPath += "&";
      }
      if ($routeParams.fType) {
        this.typeFilter = $scope._propertyTypes[propMap[$routeParams.fType]];
        $scope.urlPath += "fType=" + this.typeFilter.value;
        $scope.urlPath += "&";
      } else {
        if ($scope._propertyTypes.length > 0) {
          this.typeFilter = $scope._propertyTypes[0];
        }
      }
      if ($routeParams.fCity) {
        this.cityFilter = $scope._cities[citiesMap[$routeParams.fCity]];
        $scope.urlPath += "fCity=" + this.cityFilter.value;
        $scope.urlPath += "&";
        this.loadCityNeighborhoods();
      } else {
        if ($scope._cities.length > 0) {
          this.cityFilter = $scope._cities[0];
          this.loadCityNeighborhoods();
        }
      }
      if ($routeParams.fNeighborhood) {
        this.neighborhoodFilter = $routeParams.fNeighborhood;
        $scope.urlPath += "fNeighborhood=" + this.neighborhoodFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fAddress) {
        this.addressFilter = $routeParams.fAddress;
        $scope.urlPath += "fAddress=" + this.addressFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fCondo) {
        this.condoFilter = $routeParams.fCondo;
        $scope.urlPath += "fCondo=" + this.condoFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fPriceFrom) {
        this.priceFrom = $routeParams.fPriceFrom;
        $scope.urlPath += "fPriceFrom=" + this.priceFrom;
        $scope.urlPath += "&";
      }
      if ($routeParams.fPriceTo) {
        this.priceTo = $routeParams.fPriceTo;
        $scope.urlPath += "fPriceTo=" + this.priceTo;
        $scope.urlPath += "&";
      }
      if ($routeParams.fAreaFrom) {
        this.areaFrom = $routeParams.fAreaFrom;
        $scope.urlPath += "fAreaFrom=" + this.areaFrom;
        $scope.urlPath += "&";
      }
      if ($routeParams.fAreaTo) {
        this.areaTo = $routeParams.fAreaTo;
        $scope.urlPath += "fAreaTo=" + this.areaTo;
        $scope.urlPath += "&";
      }
      if ($routeParams.fRooms) {
        this.roomsFilter = $routeParams.fRooms;
        $scope.urlPath += "fRooms=" + this.roomsFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fWc) {
        this.wcFilter = $routeParams.fWc;
        $scope.urlPath += "fWc=" + this.wcFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fClosets) {
        this.closetsFilter = $routeParams.fClosets;
        $scope.urlPath += "fClosets=" + this.closetsFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fParking) {
        var i = ($routeParams.fParking == "true") ? 1 : 2;
        this.parkingFilter = $scope.boolOptions[i];
        $scope.urlPath += "fParking=" + this.parkingFilter.value;
        $scope.urlPath += "&";
      }
      if ($routeParams.fPool) {
        var i = ($routeParams.fPool == "true") ? 1 : 2;
        this.poolFilter = $scope.boolOptions[i];
        $scope.urlPath += "fPool=" + this.poolFilter.value;
        $scope.urlPath += "&";
      }
      if ($routeParams.fElevator) {
        var i = ($routeParams.fElevator == "true") ? 1 : 2;
        this.elevatorFilter = $scope.boolOptions[i];
        $scope.urlPath += "fElevator=" + this.elevatorFilter.value;
        $scope.urlPath += "&";
      }
      if ($routeParams.fHot) {
        var i = ($routeParams.fHot == "true") ? 1 : 2;
        this.hotFilter = $scope.boolOptions[i];
        $scope.urlPath += "fHot=" + this.hotFilter.value;
        $scope.urlPath += "&";
      }
      if ($routeParams.fUnit) {
        var i = ($routeParams.fUnit == "true") ? 1 : 2;
        this.unitFilter = $scope.boolOptions[i];
        $scope.urlPath += "fUnit=" + this.unitFilter.value;
        $scope.contSymbol = "&";
      }
      this.loadPage();
    },
    loadCityNeighborhoods: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            city: self.cityFilter.value
          }

      RequestServices.getRequest(urlNeighborhood, params)
          .success(function (data) {
                if (data.result == "OK") {
                  self.cityNeighborhoods = data.options;
                  self.cityNeighborhoods.unshift({ value: null, label: "Todos" });
                  //self.neighborhoodFilter = self.cityNeighborhoods[0];
                } else {
                  alertify.alert(data.msg);
                }

              }
          );
    },
    search: function () {
      $scope.urlSearchPath = $location.path() + "?";
      if (this.internalFilter && this.internalFilter != "") {
        $scope.urlSearchPath += "fInternal=" + this.internalFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.codeFilter && this.codeFilter != "") {
        $scope.urlSearchPath += "fCode=" + this.codeFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.statusFilter && this.statusFilter != "") {
        if (this.statusFilter.value === "") {
          this.statusFilter.value = "todos";
        }
        $scope.urlSearchPath += "fStatus=" + this.statusFilter.value;
        $scope.urlSearchPath += "&";
      }
      if (this.typeFilter && this.typeFilter.value != "") {
        $scope.urlSearchPath += "fType=" + this.typeFilter.value;
        $scope.urlSearchPath += "&";
      }
      if (this.cityFilter && this.cityFilter.value) {
        $scope.urlSearchPath += "fCity=" + this.cityFilter.value;
        $scope.urlSearchPath += "&";
      }
      if (this.neighborhoodFilter && this.neighborhoodFilter != "") {
        $scope.urlSearchPath += "fNeighborhood=" + this.neighborhoodFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.addressFilter && this.addressFilter != "") {
        $scope.urlSearchPath += "fAddress=" + this.addressFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.condoFilter && this.condoFilter != "") {
        $scope.urlSearchPath += "fCondo=" + this.condoFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.priceFrom && this.priceFrom != "") {
        $scope.urlSearchPath += "fPriceFrom=" + this.priceFrom;
        $scope.urlSearchPath += "&";
      }
      if (this.priceTo && this.priceTo != "") {
        $scope.urlSearchPath += "fPriceTo=" + this.priceTo;
        $scope.urlSearchPath += "&";
      }
      if (this.areaFrom && this.areaFrom != "") {
        $scope.urlSearchPath += "fAreaFrom=" + this.areaFrom;
        $scope.urlSearchPath += "&";
      }
      if (this.areaTo && this.areaTo != "") {
        $scope.urlSearchPath += "fAreaTo=" + this.areaTo;
        $scope.urlSearchPath += "&";
      }
      if (this.roomsFilter && this.roomsFilter != "") {
        $scope.urlSearchPath += "fRooms=" + this.roomsFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.wcFilter && this.wcFilter != "") {
        $scope.urlSearchPath += "fWc=" + this.wcFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.closetsFilter && this.closetsFilter != "") {
        $scope.urlSearchPath += "fClosets=" + this.closetsFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.parkingFilter.value !== null) {
        $scope.urlSearchPath += "fParking=" + this.parkingFilter.value
        $scope.urlSearchPath += "&";
      }
      if (this.poolFilter.value !== null) {

        $scope.urlSearchPath += "fPool=" + this.poolFilter.value
        $scope.urlSearchPath += "&";
      }
      if (this.elevatorFilter.value !== null) {

        $scope.urlSearchPath += "fElevator=" + this.elevatorFilter.value
        $scope.urlSearchPath += "&";
      }
      if (this.hotFilter.value !== null) {

        $scope.urlSearchPath += "fHot=" + this.hotFilter.value
        $scope.urlSearchPath += "&";
      }
      if (this.unitFilter.value !== null) {

        $scope.urlSearchPath += "fUnit=" + this.unitFilter.value
      }
      $location.url($scope.urlSearchPath);
    },
    loadPage: function () {
      var self = this;
      // console.log(JSON.stringify(self.statusFilter) + "-->" + self.statusFilter.value);
      var tmpTarget = (($scope._session.role == 5) ? 1 : 0);
      tmpTarget = (($scope._session.role == 1) ? null : tmpTarget);

      params = {
        status: self.statusFilter ? self.statusFilter.value : null,
        type: (self.typeFilter && self.typeFilter.value != "") ? self.typeFilter.value : null,
        city: self.cityFilter ? self.cityFilter.value : null,
        code: self.codeFilter,
        neighborhood: self.neighborhoodFilter,
        address: self.addressFilter,
        internal: self.internalFilter,
        page: self.page,
        priceFrom: self.priceFrom,
        priceTo: self.priceTo,
        areaFrom: self.areaFrom,
        areaTo: self.areaTo,
        rooms: self.roomsFilter,
        wc: self.wcFilter,
        closets: self.closetsFilter,
        parking: self.parkingFilter.value,
        pool: self.poolFilter.value,
        elevator: self.elevatorFilter.value,
        hot: self.hotFilter.value,
        unit: self.unitFilter.value,
        condo: self.condoFilter,
        size: 10,
        target: tmpTarget
      };
      RequestServices.getRequest(urlPropList, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.items = data.properties;
              AdminServices.setUrlBack($location.url());
              AdminServices.setPropertiesViews(data.properties)
              self.totalPages = data.total;
            }
          });
    },
    backToList: function () {
      this.photos = [];
      this.readOnly = true;
      this.errors = [];
      this.prop = null;
      this.details = null;
      this.cityModel = null;
      this.neighModel = null;
      this.employeeModel = null;
      this.editAddress = false;
      $scope._addressBuilder.clear();
      this.msg = null;
      this.loadPage();
    }
  };

  function init() {
    $(document).foundation();
    AdminServices.validateSession();
    propMap = AdminServices.getPropMap();
    citiesMap = AdminServices.getCitiesMap();

    $scope._statusMap = AdminServices.getStatusMap();
    $scope._session = AdminServices.getSession();
    $scope._propertyTypes = AdminServices.getPropertyTypes();

    $scope._propertyTypesMap = AdminServices.getPropertyTypesMap();
    $scope._cities = AdminServices.getCities();
    $scope._propSection.init()
  }

  init();
});

Controllers.controller('propertiesDetailCtrl', function ($scope, $http, $q, $sce, $location, $routeParams, AdminServices, RequestServices, DateServices, EmployeeServices) {
  var mainPath = AdminServices.getPath(),
      urlLoadRequest = mainPath + "contact-request/updates",
      urlBuilderOptions = mainPath + "address/builder/options",
      urlFindAllProp = mainPath + "agenda/findAllByProperty",
      // url's sets
      urlSetRequest = mainPath + "property/status/requested",
      urlSetReleased = mainPath + "property/release",
      urlSetAvailable = mainPath + "property/status/available",
      // url´s neighborhoods
      urlLoadNeigborhoods = mainPath + "address/neighborhood/options",
      // url's Property
      urlgetProperty = mainPath + "property/get",
      urlAddProperty = mainPath + "property/add",
      urlUpdateProperty = mainPath + "property/update",
      urlRetireProperty = mainPath + "property/retire",
      // url's features
      urlLoadFeatures = mainPath + "property/features",
      urlAddFeature = mainPath + "property/features/add",
      urlSaveFeatures = mainPath + "property/update/features",
      urlRemoveFeature = mainPath + "property/features/remove",
      urlFeatOptions = mainPath + "property/features/options",
      // url photos
      urlPhotos = mainPath + "property/photos",
      urlDeletePhoto = mainPath + "property/photo/delete",
      urlUploadPhoto = mainPath + "property/photo/upload",
      // url's cover
      urlUpdateCover = mainPath + "property/cover/update";

  $scope.employees = {};

  var features = [];

  $scope.files = [];
  $scope.landscape = [];
  $scope.fileControl = null;

  $scope._session = null;

  $scope._cities = [];

  $scope._propertyTypes = [];
  $scope._propertyTypesMap = {};

  $scope._serviceTypesMap = {
    0: "Arrendamiento",
    1: "Venta"
  };

  $scope.propertiesViews = {};

  $scope._statusList = [];

  $scope._statusMap = {};

  $scope._addressBuilder = {
    viaOptions: [],
    quadrantOptions: [],
    propTypeOptions: [],
    address: {
      via1: {
        via: null,
        number: "",
        letter: "",
        quadrant: null
      },
      via2: {
        number: "",
        letter: "",
        quadrant: null
      },
      number: "",
      prop1: {
        propType: null,
        propNumber: ""
      },
      prop2: {
        propType: null,
        propNumber: ""
      }
    },
    clear: function () {
      var self = this;
      self.address = {
        via1: {
          via: self.viaOptions[0],
          number: "",
          letter: "",
          quadrant: self.quadrantOptions[0]
        },
        via2: {
          number: "",
          letter: "",
          quadrant: self.quadrantOptions[0]
        },
        number: "",
        prop1: {
          propType: self.propTypeOptions[0],
          propNumber: ""
        },
        prop2: {
          propType: self.propTypeOptions[0],
          propNumber: ""
        }
      }
    },
    build: function () {
      var tmp = this.address.via1.via.ABBREVIATION + ' ';
      tmp += this.address.via1.number ? this.address.via1.number + ' ' : '';
      tmp += this.address.via1.letter ? this.address.via1.letter + ' ' : '';
      tmp += this.address.via1.quadrant.NAME != "No Aplica" ? this.address.via1.quadrant.NAME + ' ' : '';
      tmp += this.address.via2.number ? this.address.via2.number + ' ' : '';
      tmp += this.address.via2.letter ? this.address.via2.letter + ' ' : '';
      tmp += this.address.via2.quadrant.NAME != "No Aplica" ? this.address.via2.quadrant.NAME + ' ' : '';
      tmp += this.address.number ? this.address.number + ' ' : '';
      tmp += this.address.prop1.propType.ABBREVIATION ? this.address.prop1.propType.ABBREVIATION + ' ' : '';
      tmp += this.address.prop1.number ? this.address.prop1.number + ' ' : '';
      tmp += this.address.prop2.propType.ABBREVIATION ? this.address.prop2.propType.ABBREVIATION + ' ' : '';
      tmp += this.address.prop2.number ? this.address.prop2.number : '';

      return tmp;
    },
    load: function () {
      var self = this,
          paramsDirVia = {
            token: $scope._session.token,
            table: "dir_via"
          },
          paramsDirQuadrant = {
            token: $scope._session.token,
            table: "dir_quadrant"
          },
          paramsDirPropType = {
            token: $scope._session.token,
            table: "dir_property_type"
          };

      RequestServices.getRequest(urlBuilderOptions, paramsDirVia)
          .success(function (data) {
            if (data.result == "OK") {
              self.viaOptions = data.options;
              self.address.via1.via = self.viaOptions[0];
            } else {
              alertify.alert(data.msg);
            }

          });
      RequestServices.getRequest(urlBuilderOptions, paramsDirQuadrant)
          .success(function (data) {
            if (data.result == "OK") {
              self.quadrantOptions = data.options;
              self.quadrantOptions.unshift({ ID: 0, NAME: "No Aplica", ABBREVIATION: "" });
              self.address.via1.quadrant = self.quadrantOptions[0];
              self.address.via2.quadrant = self.quadrantOptions[0];
            } else {
              alertify.alert(data.msg);
            }
          });
      RequestServices.getRequest(urlBuilderOptions, paramsDirPropType)
          .success(function (data) {
            if (data.result == "OK") {
              self.propTypeOptions = data.options;
              self.propTypeOptions.unshift({ ID: 0, NAME: "No Aplica", ABBREVIATION: "" });
              self.address.prop1.propType = self.propTypeOptions[0];
              self.address.prop2.propType = self.propTypeOptions[0];
            } else {
              alertify.alert(data.msg);
            }
          });
    }
  };

  $scope._reportManager = {
    propStatusFrom: DateServices.dateAsText(DateServices.addDays(new Date(), -30)),
    propStatusTo: DateServices.dateAsText(new Date()),
    propStatus: $scope._statusList[0]
  };

  $scope.featureValues = {
    options: {},
    load: function () {
      var self = this;
      RequestServices.getRequest(urlFeatOptions)
          .success(function (data) {
            if (data.result == "OK") {
              self.options = data.values;
            }
          });
    }
  };

  $scope._propSection = {

    prop: null,
    photoToDelete: null,
    coverPhoto: null,
    bkp: null,
    waitFor: null,
    details: null,
    photos: [],
    photos360: [],

    items: [],
    errors: [],
    msg: "",
    readOnly: true,
    cityNeighborhoods: [],
    neighborhoods: [],
    neighborhoodsMap: {},
    cityModel: {},
    neighModel: {},
    employeeModel: {},
    retireComment: null,
    nextView: null,
    prevView: null,

    features: [],
    swNew: false,
    newFeature: null,
    codeTypes: [],
    codesMap: {
      0: [
        {
          value: "AA", label: "Arr. Apartamentos"
        },
        {
          value: "AC", label: "Arr. Casas"
        },
        {
          value: "AL", label: "Arr. Locales"
        }
      ],
      1: [
        {
          value: "VA", label: "Ventas Apartamentos"
        },
        {
          value: "VC", label: "Ventas Casas"
        },
        {
          value: "VL", label: "Ventas Locales"
        },
        {
          value: "VF", label: "Ventas Fincas"
        },
        {
          value: "VT", label: "Ventas Terrenos"
        }
      ]
    },
    retireProperty: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            ID: self.prop.ID,
            CODE: self.prop.CODE,
            SERVICE_TYPE: self.prop.SERVICE_TYPE,
            RETIRE_MOTIVE: self.retireComment
          }

      RequestServices.getRequest(urlRetireProperty, params)
          .success(function (data) {
            if (data.result == "OK") {
              $location.url($scope.lastPropSearch);
            } else {
              alertify.alert(data.msg);
            }
          });
    },
    setAvailable: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            ID: self.prop.ID
          };

      RequestServices.getRequest(urlSetAvailable, params)
          .success(function (data) {

                if (data.result == "OK") {
                  $location.url($scope.lastPropSearch)
                } else {
                  alertify.alert(data.msg);
                }

              }
          );
    },
    setReleased: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            ID: self.prop.ID
          };

      RequestServices.getRequest(urlSetReleased, params)
          .success(function (data) {
                if (data.result == "OK") {
                  $location.url($scope.lastPropSearch)
                } else {
                  alertify.alert(data.msg);
                }

              }
          );
    },
    setRequested: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            ID: self.prop.ID
          };

      RequestServices.getRequest(urlSetRequest, params)
          .success(function (data) {
            if (data.result == "OK") {
              $location.url($scope.lastPropSearch);
            } else {
              alertify.alert(data.msg);
            }
          });
    },
    loadCodeTypes: function () {
      this.codeTypes = this.codesMap[this.prop.SERVICE_TYPE];
      this.prop.CODE = this.codeTypes[0];
    },
    init: function () {
      var self = this;
      console.log()
      if ($routeParams.propId) {
        this.getProperty()
      } else {
        this.readOnly = false;
        if ($routeParams.found) {
          this.restoreProp();
          this.ownerFound();
        } else if ($routeParams.restoreProp) {
          this.restoreProp();
          AdminServices.flash.clear();
        } else {
          this.showAddressEditor();
          this.prop = {};
          this.prop.SERVICE_TYPE = 0;
          this.loadCodeTypes();
          this.prop.TYPE_MODEL = $scope._propertyTypes[1];
          this.prop.STATUS_MODEL = $scope._statusList[3];
          this.employeeModel = $scope.employees.list[2];
          this.employeeUpdated();
          angular.forEach($scope._cities, function (city, key) {
            if (city.value == 1) {
              self.cityModel = $scope._cities[key];
            };
          });
          this.loadNeighborhoods();
        }
      }
    },
    restoreProp: function () {
      var self = this;
      this.prop = AdminServices.getPropSaved();
      this.loadMain();
      this.codeTypes = this.codesMap[this.prop.SERVICE_TYPE];
      var code = this.prop.CODE;
      if (!this.prop.ID) {
        angular.forEach(this.codeTypes, function (obj, key) {
          if (obj.value === code.value) {
            self.prop.CODE = self.codeTypes[key];
          }
        });
      };
      this.prop.TYPE_MODEL = $scope._propertyTypes[this.prop.TYPE_MODEL.value];
      this.employeeModel = $scope.employees.map[this.prop.ID_EMPLOYEE];
    },
    ownerFound: function () {
      var owner = AdminServices.getOwnerSaved();
      if (AdminServices.waitFor != "owner") {
        return;
      }
      this.prop.ID_OWNER = owner.ID;
      this.prop.OWNER_NAME = owner.NAME;
      AdminServices.flash.clear();
    },
    findOwner: function () {
      AdminServices.saveProp(this.prop);
      AdminServices.waitFor = "owner";
      AdminServices.flash.copyMode = true;
      // init the owners screen in search mode
      $location.path("/Owner/find")
    },
    showAddressEditor: function () {
      this.editAddress = true;
    },
    hideAddressEditor: function () {
      this.editAddress = false;
    },
    edit: function () {
      this.editAddress = false;
      this.readOnly = false;
      this.msg = null;
      this.prop.TYPE_MODEL = $scope._propertyTypes[this.prop.ID_TYPE];
      this.bkp = angular.copy(this.prop);
    },
    setupCityAndNeighborhoods: function () {
      var self = this;
      // setup the city combo
      angular.forEach($scope._cities, function (value, key) {
        if (self.prop.CITY_ID == value.value) {
          self.cityModel = value;
        }

      });
      self.loadNeighborhoods();
    },
    cityUpdated: function () {
      this.prop.CITY = this.cityModel.label;
      this.prop.CITY_ID = this.cityModel.value;
    },
    neighborhoodUpdated: function () {
      this.prop.NEIGHBORHOOD = this.neighModel.label;
      this.prop.ID_NEIGHBORHOOD = this.neighModel.value;
      this.cityUpdated();
    },
    loadNeighborhoods: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            city: self.cityModel.value
          };
      self.neighborhoods = [];
      RequestServices.getRequest(urlLoadNeigborhoods, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.neighborhoods = data.options;
              var found = false;
              angular.forEach(self.neighborhoods, function (value, key) {
                if (self.prop.ID_NEIGHBORHOOD == value.value) {
                  found = true;
                  self.neighModel = value;
                }
              });
              if (!found) {
                self.neighModel = self.neighborhoods[0];
              }

              self.neighborhoodUpdated();

            } else {
              alertify.alert(data.msg);
            }
          });
    },
    save: function () {
      var self = this;
      // console.log(self.prop);
      self.prop.ID_TYPE = self.prop.TYPE_MODEL.value;

      if (self.prop.ID) {
        $http.post(urlUpdateProperty,
            JSON.stringify({
              property: self.prop
            })
        ).success(function (data) {
              if (data.result == "OK") {
                self.saveFeatures();
              } else {
                alertify.alert(data.msg);
              }
            }
        );
      } else {
        // console.log("PROP:" + JSON.stringify(self.prop));
        $http.post(urlAddProperty,
            JSON.stringify({
              property: self.prop
            })
        ).success(function (data) {
              // console.log(data);
              if (data.result == "OK") {
                self.prop.ID = data.propertyId
                self.prop.CODE = data.code;
                self.prop.STATUS = data.status;
                self.prop.LAST_AVAILABLE = data.lastAvailable;
                self.prop.REG_DATE = data.regDate;
                self.swNew = true;
              } else {
                alertify.alert(data.msg);
              }

            }
        );
      }
    },
    cancel: function (restore) {
      this.readOnly = true;
      this.msg = null;
      AdminServices.flash.clear();
      if (restore) {
        angular.copy(this.bkp, this.prop);
      }
      if (this.prop.ID) {
        this.readOnly = true;
        this.errors = [];
        this.msg = null;
      } else {
        $location.url($scope.lastPropSearch);
      }
    },
    backToList: function () {
      $location.path("/Properties");
    },
    employeeUpdated: function () {
      this.prop.ID_EMPLOYEE = this.employeeModel.ID;
    },
    loadMain: function () {
      var self = this;
      self.loadPropertyFeatures();
      self.loadPhotos();
      self.setupCityAndNeighborhoods();
      self.findAllProperty();
      $scope.featureValues.load();
      self.loadPropertyViews();
    },
    getProperty: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            id: $routeParams.propId
          }

      RequestServices.getRequest(urlgetProperty, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.prop = data.property;
              self.employeeModel = $scope.employees.map[self.prop.ID_EMPLOYEE];
              // console.log(JSON.stringify(self.employeeModel));
              self.loadMain();
            }
          })
    },
    findAllProperty: function () {
      var self = this,
          params = {
            contact: self.prop.ID
          }
      RequestServices.getRequest(urlFindAllProp, params)
          .success(function (data) {

            if (data.result == "OK") {
              self.prop.appointments = [];

              angular.forEach(data.appointments, function (app, key) {
                app.SCHEDULED_LABEL = app.SCHEDULED.split("-").join("/");
                app.CREATED = new Date(app.CREATED);
                app.TIME_FROM = app.TIME_FROM.substring(0, 5);

                app.TIME_TO = app.TIME_TO.substring(0, 5);
                self.prop.appointments.push(app);

              });

            } else {
              alertify.alert(data.msg);
            }
          });
    },
    loadPhotos: function () {
      var self = this,
          params = {
            property: self.prop.ID
          };
      RequestServices.getRequest(urlPhotos, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.photos = [];
              self.photos360 = [];
              var cont = 0;
              var cont2 = 0;
              angular.forEach(data.keys, function (photo, key) {
                if (photo.endsWith("_360")) {
                  var pKey = photo.substring(0, photo.lastIndexOf('_'));
                  self.photos360.push({
                    img: "https://www.arrendamientosayura.com/property/photo?key=" + pKey + "_360",
                    label: "Foto-" + cont2
                  });
                  cont2++;
                } else if (photo.endsWith("_l")) {

                  var pKey = photo.substring(0, photo.lastIndexOf('_'));
                  self.photos.push({
                    l: "/property/photo?key=" + pKey + "_l",
                    s: "/property/photo?type=th&key=" + pKey + "_s",
                    label: "Foto-" + cont
                  });
                  cont++;
                }
              });
              console.log(self.photos360);
              if (self.prop.COVER) {
                angular.forEach(self.photos, function (photo, key) {
                  if (photo.s == ("/property/photo?type=th&key=" + self.prop.COVER)) {
                    self.coverPhoto = photo;
                  }
                });
              }
            }
          });
    },
    deletePhoto: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            photo: self.photoToDelete
          }
      RequestServices.getRequest(urlDeletePhoto, params)
          .success(function (data) {
                self.photoToDelete.s = "/images/noDisponible.jpg";
                self.photoToDelete = null;
                if (data.result == "OK") {
                  console.log("DELETED? R/" + data.didDelete);
                  self.loadPhotos();
                } else {
                  alertify.alert(data.msg);
                }

              }
          );
    },
    deletePhoto360: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            photo: self.photoToDelete360
          }
      params.photo['_360'] = params.photo.img;

      RequestServices.getRequest(urlDeletePhoto, params)
          .success(function (data) {
                console.log(self.photoToDelete)
                self.photoToDelete360.img = "/images/noDisponible.jpg";
                self.photoToDelete360 = null;
                if (data.result == "OK") {
                  console.log("DELETED? R/" + data.didDelete);
                  self.loadPhotos();
                } else {
                  alertify.alert(data.msg);
                }

              }
          );
    },
    updateCover: function () {
      var self = this;
      params = {
        key: self.coverPhoto.s.split("key=")[1],
        id: self.prop.ID
      }
      RequestServices.getRequest(urlUpdateCover, params)
          .success(function (data) {
                self.coverPhoto = null;
                if (data.result == "OK") {
                  alertify.alert("Foto de portda actualizada.")
                } else {
                  alertify.alert(data.msg);
                }
              }
          );
    },
    loadPropertyFeatures: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            id: self.prop.ID
          }
      RequestServices.getRequest(urlLoadFeatures, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.details = data.details;
              self.features = [];
              angular.forEach(features, function (feat, key) {
                var add = true;
                angular.forEach(data.details, function (det, key) {
                  if (feat.ID == det.type_id) {
                    add = false;
                  }
                });
                // solo pongamos disponibles las que ya no están agregadas
                if (add) {
                  self.features.push(feat);
                }
              });
              if (self.features.length) {
                self.newFeature = self.features[0];
              }
            }
          });
    },
    addFeature: function () {
      var self = this;
      self.saveFeatures(function () {
        $http.post(urlAddFeature,
            JSON.stringify({
              feature: {
                property: $scope._propSection.prop.ID,
                feature: $scope._propSection.newFeature.ID,
                type: $scope._propSection.newFeature.TYPE
              }
            })
        ).success(function (data) {
          if (data.result == "OK") {
            $scope._propSection.loadPropertyFeatures();
          }
        });
      });
    },
    saveFeatures: function (callback) {
      var self = this;
      $http.post(urlSaveFeatures,
          JSON.stringify({
            features: self.details
          })
      ).success(function (data) {
        if (data.result == "OK") {
          if (callback) {
            callback();
            return;
          };
          if (self.swNew) {
            $location.url("/Properties/" + self.prop.ID);
          } else {
            alertify.alert("Property updated.");
            self.cancel(false);
          }
        } else {
          alertify.alert(data.msg);
        }
      });
    },
    removeFeature: function (feature) {
      var self = this;
      $http.post(urlRemoveFeature,
          JSON.stringify({
            feature: {
              feature: feature.id
            }
          })
      ).success(function (data) {
        if (data.result == "OK") {
          self.loadPropertyFeatures();
        }

      });
    },
    replaceAddress: function () {
      this.prop.ADDRESS = $scope._addressBuilder.build();
      this.hideAddressEditor();
    },
    loadPropertyViews: function () {
      var self = this;
      $scope.propertiesViews = AdminServices.getPropertiesViews();

      angular.forEach($scope.propertiesViews, function (propView, key) {
        if (propView.ID == $routeParams.propId) {
          $scope.propertiesViewsIndex = key;
          if (key != ($scope.propertiesViews.length - 1)) {
            self.nextView = "/Properties/" + $scope.propertiesViews[key + 1].ID;
          }
          if (key > 0) {
            self.prevView = "/Properties/" + $scope.propertiesViews[key - 1].ID;
          }

        }
      })
    }
  };

  $scope._requestComments = [];

  $scope.showWhen = function () {
    if ($scope._session.role == 1 || $scope._session.role == 8 || $scope._session.role == 5
        || $scope._session.role == 6 || $scope._session.role == 7 || $scope._session.id == 1037571770
        || $scope._session.id == 43630114 || $scope._session.id == 43733389 || $scope._session.id == 43746263 || $scope._session.id == 1037645365 || $scope._session.id == 1037636128
        || $scope._session.id == 1037628515 || $scope._session.id == 1010208974 || $scope._session.id == 1037645596 || $scope._session.id == 43911974) {

      return true;
    };
    return false;
  }

  $(document).on('close.fndtn.reveal', '[data-reveal]', function () {
    if (PSV) {
      PSV.destroy();
      $('#js-panorama *').remove()
    }
  })

  $scope.panorama = function (evt, url) {
    evt.preventDefault();

    $(document).on('opened.fndtn.reveal', '[data-reveal]', function () {
      var modal = $(this);
      $('#js-panorama *').remove()

      // Loader
      var loader = document.createElement('div');
      loader.className = 'fa fa-2x fa-spinner fa-spin ';
      console.log(url)

      // Panorama display
      var div = document.getElementById('js-panorama')
      div.className = 'text-center';
      div.style.height = '30px';



      PSV = new PhotoSphereViewer({
        // Path to the panorama
        panorama: url,

        // Container
        container: div,

        // Deactivate the animation
        time_anim: false,
        mousewheel: false,

        // Display the navigation bar
        navbar: [
          'fullscreen'
        ],
        default_fov: 179,

        // Resize the panorama
        size: {
          width: '900px',
          height: '600px'
        },
        loading_txt: 'Loading'
      });
      $scope.$apply();
    });
  }

  $scope.loadComments = function (contactId) {
    console.info(contactId)
    params = {
      token: $scope._session.token,
      request: contactId
    }
    console.log(params);
    RequestServices.getRequest(urlLoadRequest, params)
        .success(function (data) {
              console.log(data)
              if (data.result == "OK") {
                angular.forEach(data.updates, function (comment, key) {
                  if (comment.comment) {
                    var strInit = comment.comment;
                    if (strInit.indexOf("#") != -1) {
                      var startCode = strInit.indexOf("#");
                      var str = strInit.substring(startCode);
                      var strToE = str.substring(1);
                      var endCode = 0;
                      if (strToE.search(/(\s|\D)/) != -1) {
                        endCode = strToE.search(/(\s|\D)/) + 1;
                      } else {
                        endCode = str.length;
                      }
                      var strToReplace = str.substring(0, endCode);
                      var newMessage = strInit.replace(strToReplace, "<a href='#/Properties/" + str.substring(1, endCode) + "'>" + strToReplace + "</a>");
                      comment.comment = $sce.trustAsHtml(newMessage.split("\n").join("<br>"));
                    } else {
                      comment.comment = $sce.trustAsHtml(comment.comment.split("\n").join("<br>"));
                    }
                  }
                })
                $scope._requestComments = data.updates;
              } else {
                alertify.alert(data.msg);
              }

            }
        );
  };

  $scope.range = function (from, to) {
    return AdminServices.range(from, to);
  };

  $scope.hourBasicFromText = function (milHour) {
    return DateServices.hourBasicFromText(milHour);
  }

  $scope.save = function () {
    //now add all of the assigned files
    for (var ic = 0; ic < $scope.files.length; ic++) {
      $scope.send($scope.files[ic], $scope.landscape[ic]);
    }
    $scope.fileControl.form.reset();
    $scope.files = [];
    $scope.landscape = [];
  };

  $scope.send = function (file, landscape) {
    console.log(landscape);
    AdminServices.send(file, $scope._propSection.prop, urlUploadPhoto, null, landscape)
        .success(function () {
          $scope._propSection.loadPhotos();
        });
  };

  function init() {
    AdminServices.validateSession();
    $scope._session = AdminServices.getSession();
    $scope._addressBuilder.load();
    $scope.lastPropSearch = AdminServices.getUrlBack();
    $scope.employees = EmployeeServices;
    features = AdminServices.getFeatures();
    $scope._statusList = AdminServices.getStatusList();
    $scope._statusMap = AdminServices.getStatusMap();
    $scope._propertyTypes = AdminServices.getPropertyTypes();
    $scope._propertyTypesMap = AdminServices.getPropertyTypesMap();
    $scope._cities = AdminServices.getCities();

    $scope._propSection.init();
    $(document).foundation();
    $scope.$on("fileSelected", function (event, args) {
      $scope.$apply(function () {
        //add the file object to the scope's files collection
        $scope.landscape.push(false);
        console.log($scope.landscape);
        $scope.files.push(args.file);
        $scope.fileControl = args.control;
      });
    });
  };

  init();
});

// Solicitudes

Controllers.controller('requestCtrl', function ($scope, $http, $q, $location, $routeParams, AdminServices, RequestServices, DateServices, EmployeeServices) {
  var mainPath = AdminServices.getPath(),
      urlgetProperty = mainPath + "property/get",
      urlGetItems = mainPath + "content/search",
      urlUpload = mainPath + "content/upload",
      urlPropRequest = mainPath + "prop-request/list",
      urlAddComment = mainPath + "prop-request/comments/add",
      urlListComment = mainPath + "prop-request/list/comments",
      urlRequestAction = mainPath + "prop-request/action",
      urlGetStatus = mainPath + "prop-request/list/status",
      urlAddRequest = mainPath + "prop-request/add",
      urlGetRequest = mainPath + "prop-request/get",
      urlUpdatePropReq = mainPath + "prop-request/update/property";


  var features = [];
  var imageValidate = AdminServices.getImgValidate();
  var typeItems = {
        0: "folder",
        1: "file",
        2: "folder-o",
        3: "files-o",
        4: "folder-open",
        5: "folder-open-o",
        6: "file-text",
        7: "archive",
        8: "file-o",
        9: "file-text-o",
      },
      typeItemsMap = {
        "Folder": 0,
        "Document": 9,
      },
      folderId = 1;

  $scope.mainPage = 1;
  $scope.itemsPage = 1;

  $scope.tabActive = true
  $scope.files = [];
  $scope.landscape = [];
  $scope.filesSend = [];
  $scope.nav = false;
  $scope.states = [];

  $scope.requestIdSelected = null;

  $scope._session = null;
  $scope.docType = null;

  $scope._requestComments = {
    listComments: [],
    comment: null,
    getComments: function () {
      var self = this;
      var params = {
        request: $scope.requestIdSelected
      }
      RequestServices.getRequest(urlListComment, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.listComments = data.updates;
            };
          });
    },
    addComment: function () {
      var self = this;
      var params = {
        token: $scope._session.token, // (obligatorio porque de ahí se agrega el usuario que hizo el comment)
        comment: self.comment, //  (el comentario)
        request: $scope.requestIdSelected //  (la solicitud asociada al comentario(
      }
      RequestServices.getRequest(urlAddComment, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.comment = null;
              self.getComments();
            } else {
              alertify.alert(data.msg)
            }
          })
    }
  };
  $scope._serviceTypesMap = {
    0: "Arrendamiento",
    1: "Venta"
  };

  $scope.prop = {};

  function requestModel(id, createdDate, owner, status, statusId, actions, propId, metadata) {
    this.id = id || "",
        this.createdDate = DateServices.formatDate(new Date(createdDate)) || "",
        this.status = status || "",
        this.statusId = statusId || ""
    this.owner = owner || "",
        this.propId = propId || "",
        this.actions = actions || [],
        this.actionSelected = this.actions[0],
        this.metadata = metadata || {}
  };

  requestModel.prototype.selected = function () {
    if ($scope.requestIdSelected != this.id) {
      refreshRequest(this);
      $scope.requestIdSelected = this.id;
      $scope.requestSelected = this.metadata;
      $scope._requestComments.comment = null;
      $scope.docType = null;
      $scope.nav = false;
      $scope._requestComments.getComments();
      if (this.propId) {
        getPropDetail(this.propId);
      }
    }
  };

  requestModel.prototype.finish = function (evt) {
    evt.preventDefault();
    evt.stopPropagation();
    $scope.bkpRequest = angular.copy(this);
    refreshRequest(this, function (obj) {
      var params = {
        token: $scope._session.token,
        request: obj.id,
        action: obj.actionSelected.value
      };
      RequestServices.getRequest(urlRequestAction, params)
          .success(function (data) {
            if (data.result == "OK") {
              getRequestProp();
            } else {
              alertify.alert(data.msg);
            }
          });
    })
  };

  requestModel.prototype.addProp = function () {
    $scope.requestSprop = this.id;
    console.log($scope.requestSprop)
  }

  function itemModel(name, type, metadata) {
    this.name = name || "",
        this.type = type || "",
        this.metadataInfo = metadata || []
  };

  itemModel.prototype.action = function (evt) {
    if (this.metadataInfo.ICON_TYPE == "Folder") {
      if (this.metadataInfo.DOC_TYPE_ID) {
        $scope.docType = this.metadataInfo.DOC_TYPE_ID;
      }
      $scope.itemsPage = 1;
      if (!$scope.nav) {
        $scope.nav = true
      }
      getItems();
    }
  };



  $scope.prevent = function (evt) {
    evt.stopPropagation();
  };

  var getPropDetail = function (propId) {
    var self = this,
        params = {
          token: $scope._session.token,
          id: propId
        }
    RequestServices.getRequest(urlgetProperty, params)
        .success(function (data) {
          if (data.result == "OK") {
            $scope.prop = data.property;
          }
        })
  };
  $scope.refresh = function () {
    $scope.mainPage = 1;
    getRequestProp();
  }

  $scope._ContactFilters = {
    state: null,
    name: null,
    docId: null,
    requestId: null,
    contactId: null,
    filter: function (evt) {
      evt.preventDefault();
      evt.stopPropagation();
      $scope.mainPage = 1;
      // var urlFIlterContact = $location.path()+"?";
      // if(this.state && this.state.value != null){
      //   urlFIlterContact += "fstate="+this.state.value;
      //   urlFIlterContact +="&";
      // }
      // if(this.name && this.name != ""){
      //   urlFIlterContact += "fname="+this.name;
      //   urlFIlterContact +="&";
      // }
      // if(this.docId && this.docId != ""){
      //   urlFIlterContact += "fdocId="+this.docId;
      //   urlFIlterContact +="&";
      // }
      // if(this.requestId && this.requestId != ""){
      //   urlFIlterContact += "frequestId="+this.requestId;
      //   urlFIlterContact +="&";
      // }
      // if(this.contactId && this.contactId != ""){
      //   urlFIlterContact += "fcontactId="+this.contactId;
      // }
      $("#contactSearch").foundation('reveal', 'close');
      // $location.url( urlFIlterContact );
      getRequestProp();
    },
    clear: function (evt) {
      if (evt) {
        evt.preventDefault();
        evt.stopPropagation();
      };
      this.state = $scope.states[0];
      this.name = null;
      this.docId = null;
      this.requestId = null;
      this.contactId = null;
    },
    loadFilter: function () {
      if ($routeParams.frequestId) {
        this.requestId = $routeParams.frequestId;
      }
    }
  };

  $scope.prevMain = function () {
    var urlSearch = "";
    $scope.mainPage -= 1;
    // if( $routeParams.page ){
    //   $location.search('page',$scope.mainPage)
    // }else{
    //   urlSearch = $location.url()+"&page="+$scope.mainPage;
    //   $location.url(urlSearch);
    // }
    getRequestProp();
  }
  $scope.nextMain = function () {
    var urlSearch = "";
    $scope.mainPage += 1;
    // if( $routeParams.page ){
    //   console.log("here");
    //   $location.search('page',$scope.mainPage)
    // }else{
    //   if($location.url() != $location.path()){
    //     urlSearch = $location.url()+"&page="+$scope.mainPage;
    //   }else{
    //     urlSearch = $location.url()+"?page="+$scope.mainPage;
    //   }  
    //   $location.url(urlSearch);
    // }
    getRequestProp();
  };

  var refreshRequest = function (obj, callback) {
    var params = {
      token: $scope._session.token,
      request: obj.id
    };
    RequestServices.getRequest(urlGetRequest, params)
        .success(function (data) {
          if (data.result == "OK") {
            if (callback) {
              if (obj.statusId == data.request.id_status) {
                callback(obj);
              } else {
                alertify.alert("La solicitud que seleccionó ha cambiado de estado, por favor recargue las solicitudes nuevamente.");
                obj.status = data.request.status;
                obj.statusId = data.request.id_status;
                obj.actions = data.request.actions;
                obj.actionSelected = obj.actions[0];
                obj.metadata = data.request;
                $scope.requestSelected = obj.metadata;
                return;
              }
            } else {
              obj.status = data.request.status,
                  obj.statusId = data.request.id_status,
                  obj.actions = data.request.actions,
                  obj.actionSelected = obj.actions[0],
                  obj.metadata = data.request
            }
          };
        })
  }

  var getRequestProp = function () {
    var params = {
      token: $scope._session.token,
      page: $scope.mainPage,
      size: 5,
      stateFilter: $scope._ContactFilters.state ? $scope._ContactFilters.state.value : null,  // (ID del estado a filtrar)
      nameFilter: $scope._ContactFilters.name,   // (Filtro de nombre de la persona del contacto)
      requestFilter: $scope._ContactFilters.requestId,// (filtro por ID del contacto)
      contactFilter: $scope._ContactFilters.contactId,
      docFilter: $scope._ContactFilters.docId
    };
    RequestServices.getRequest(urlPropRequest, params)
        .success(function (data) {
          $scope.requests = [];
          $scope.requestIdSelected = null;
          if (data.result == "OK") {
            $scope.mainTotalPages = data.totalPages
            angular.forEach(data.requests, function (request, key) {
              var req = new requestModel(
                  request.id,
                  request.created,
                  request.contact_name,
                  request.status,
                  request.id_status,
                  request.actions,
                  request.property_id,
                  request
              );
              $scope.requests.push(req);
            })
            if ($scope.mainTotalPages == 1 && $scope.requests.length == 1) {
              $scope.requests[0].selected();
            };
          };
        })
  };

  var getStatus = function () {
    RequestServices.getRequest(urlGetStatus)
        .success(function (data) {
          if (data.result == "OK") {
            $scope.states = data.statusList;
            $scope.states.unshift({
              label: "TODOS",
              value: null
            })
            if ($routeParams.fstate) {
              angular.forEach($scope.states, function (state, key) {
                if ($routeParams.fstate == state.value) {
                  $scope._ContactFilters.state = $scope.states[key]
                }
              });
            } else {
              $scope._ContactFilters.state = $scope.states[0]
            }
          };
        })
  }

  $scope.clearRequest = function () {
    $scope.requestInfo = "";
    $scope.propToRequest = null;
    $scope.requestError = [];
  };

  $scope.addProp = function () {
    $scope.requestInfo = "";
    $scope.requestError = [];
    $scope.createMsg = false;
    if (!$scope.propToRequest) {
      return
    }
    var params = {
      token: $scope._session.token,
      id: $scope.propToRequest
    };
    var paramsAddRequest = {
      token: $scope._session.token,
      prop: $scope.propToRequest,
      request: $scope.requestSprop
    };

    RequestServices.getRequest(urlgetProperty, params)
        .success(function (data) {
          console.warn(data)
          if (data.result == "ERROR") {
            $scope.requestError.push("La Propiedad no es valida")
            $scope.createMsg = true;
          } else {
            RequestServices.getRequest(urlUpdatePropReq, paramsAddRequest)
                .success(function (data) {
                  if (data.result == "OK") {
                    $scope.createMsg = true;
                    $scope.requestInfo = "Se ha agrego la propiedad a la solicitud";
                    setTimeout(function () {
                      getRequestProp();
                    }, 1000);
                  };
                })
          }
        })

  }

  function init() {
    AdminServices.validateSession();
    $scope._session = AdminServices.getSession();
    $scope._propertyTypesMap = AdminServices.getPropertyTypesMap();
    $scope._statusMap = AdminServices.getStatusMap();
    $scope.employees = EmployeeServices;
    $scope._ContactFilters.loadFilter();
    getRequestProp();
    getStatus();
    $(document).foundation();
    AdminServices.setUrlBack($location.url());
    AdminServices.setPropertiesViews([]);
    $scope.$on("fileSelected", function (event, args) {
      $scope.$apply(function () {
        //add the file object to the scope's files collection
        $scope.files.push(args.file);
        $scope.landscape.push(false);
        console.log($scope.landscape)
        $scope.fileControl = args.control;
      });
    });
  };

  init();
});

// Propietarios Completed!

Controllers.controller('ownerCtrl', function ($scope, $location, $routeParams, AdminServices, RequestServices) {
  var mainPath = AdminServices.getPath(),
      urlOwnerList = mainPath + "owner/list";
  $scope.urlFlashMode = "#/Properties/add?found";
  $scope._session = null;
  $scope.flash = null;
  $scope.urlPath = $location.path() + "?";
  // console.log($scope.urlPath)
  $scope.urlSearchPath = $scope.urlPath;
  $scope.contSymbol = "";

  $scope._ownerSection = {
    // MODEL
    items: [],
    errors: [],
    msg: "",
    page: 1,
    totalPages: 1,
    size: 10,
    documentFilter: null,
    nameFilter: null,

    init: function () {
      if ($routeParams.page) {
        this.page = parseInt($routeParams.page);
      }
      if ($routeParams.fDocument) {
        this.documentFilter = $routeParams.fDocument;
        $scope.urlPath += "fDocument=" + this.documentFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fName) {
        this.nameFilter = $routeParams.fName;
        $scope.urlPath += "fName=" + this.nameFilter;
        $scope.contSymbol = "&";
      }
      this.loadPage();
    },

    search: function () {
      $scope.urlSearchPath = $location.path() + "?";
      console.log($scope.urlSearchPath)
      if (this.documentFilter && this.documentFilter != "") {
        $scope.urlSearchPath += "fDocument=" + this.documentFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.nameFilter && this.nameFilter != "") {
        $scope.urlSearchPath += "fName=" + this.nameFilter;
      }
      $location.url($scope.urlSearchPath);
    },

    loadPage: function () {
      var self = this,
          params = {
            page: this.page,
            size: this.size,
            token: $scope._session.token,
            documentFilter: this.documentFilter,
            nameFilter: this.nameFilter
          };

      RequestServices.getRequest(urlOwnerList, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.items = data.owners;
              self.totalPages = data.totalPages;
            } else {
              alertify.alert(data.msg);
            }
          });
    }
  };

  $scope.callbackFlashMode = function (owner) {
    AdminServices.saveOwner(owner);
  }

  function init() {
    AdminServices.validateSession();
    $scope._session = AdminServices.getSession();
    $scope.flash = AdminServices.flash;
    $scope._ownerSection.init();
    if (!$scope.flash.copyMode) {
      AdminServices.setUrlBack($location.url());
    };
    AdminServices.setPropertiesViews([]);
  }

  init();
});

Controllers.controller('ownerDetailCtrl', function ($scope, $sce, $cookies, $location, $routeParams, AdminServices, RequestServices) {
  var mainPath = AdminServices.getPath(),
      urlOwnerGet = mainPath + "owner/get",
      urlOwnerSave = mainPath + "owner/update",
      urlOwnerPassword = mainPath + "owner/password",
      urlOwnerProperties = mainPath + "property/list",
      urlgetProperty = mainPath + "property/get";


  $scope._session = null;
  $scope._statusMap = {}

  $scope.getPropDetail = function (propId) {
    var self = this,
        params = {
          token: $scope._session.token,
          id: propId
        }
    RequestServices.getRequest(urlgetProperty, params)
        .success(function (data) {
          if (data.result == "OK") {
            $scope.prop = data.property;
          }
        })
  };

  $scope._ownerSection = {
    // MODEL
    items: [],
    ownerProperties: [],
    errors: [],
    msg: "",
    page: 1,
    totalPages: 1,
    size: 10,
    documentFilter: null,
    nameFilter: null,
    owner: null,
    readOnly: true,
    loadOwnerProperties: function () {
      var self = this,
          params = {
            owner: self.owner.ID,
            page: 1,
            size: 100
          };
      RequestServices.getRequest(urlOwnerProperties, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.ownerProperties = data.properties;
            }

          });
    },
    getOwner: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            id: $routeParams.ownerId
          }
      RequestServices.getRequest(urlOwnerGet, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.owner = data.owner;
              self.loadOwnerProperties();
            }
          });
    },
    generatePassword: function () {
      var self = this,
          params = {
            token: $scope._session.token,
            owner_id: $routeParams.ownerId
          }
      RequestServices.getRequest(urlOwnerPassword, params)
          .success(function (data) {
            if (data.result == "OK") {
              $scope.logPass = data.owner.password;
              $('#passwrodLog').foundation('reveal', 'open');
            }
          });
    },
    init: function () {
      $scope._session = AdminServices.getSession();
      if ($routeParams.ownerId) {
        this.getOwner();
      } else {
        this.readOnly = false;
        this.owner = {};
      }
    },
    cancel: function () {
      if (this.owner.ID) {
        this.readOnly = true;
        this.errors = [];
        this.msg = null;
      } else {
        $location.path("/Owner")
      }
    },
    validate: function () {
      this.errors = [];

      if (!this.owner.NAME) {
        this.errors.push("El 'Nombre' es requerido");
      }

      if (!this.owner.DOCUMENT) {
        this.errors.push("El 'Documento' es requerido");
      }
    },
    edit: function () {
      this.readOnly = false;
      this.msg = null;
    },
    save: function () {
      var self = this;
      this.validate();
      // if there are errors, dont save
      if (this.errors.length) {
        return;
      }
      var params = {
        ID: this.owner.ID,
        NAME: this.owner.NAME,
        DOCUMENT: this.owner.DOCUMENT,
        ADDRESS: this.owner.ADDRESS ? this.owner.ADDRESS : "",
        PHONES: this.owner.PHONES,
        CELULAR: this.owner.CELULAR,
        MAIL: this.owner.MAIL,
        BIRTHDAY: this.owner.BIRTHDAY,
        COMMENT: this.owner.COMMENT,
        token: $scope._session.token
      }
      RequestServices.getRequest(urlOwnerSave, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.msg = "Propietario Guardado.";
              self.readOnly = true;
            } else {
              alertify.alert(data.msg);
            }

          });
    }
  };

  function init() {
    AdminServices.validateSession();
    $scope._session = AdminServices.getSession();
    $scope._statusMap = AdminServices.getStatusMap();
    $scope._ownerSection.init();
    $(document).foundation();
    $scope.flash = AdminServices.flash;
    if (!$scope.flash.copyMode) {
      AdminServices.setUrlBack($location.url());
    };
    AdminServices.setPropertiesViews([]);
  }

  init();
});

// Inquilinos Completed!

Controllers.controller('tenantCtrl', function ($scope, $routeParams, $location, AdminServices, RequestServices) {

  var mainPath = AdminServices.getPath(),
      urlTenant = mainPath + "tenant/list";

  $scope._session = null;

  $scope.items = [];
  $scope.urlPath = "#/Tenant?";
  $scope.urlSearchPath = $scope.urlPath;
  $scope.contSymbol = "";

  $scope._tenantSection = {

    contract: null,

    // MODEL
    items: [],
    errors: [],
    msg: "",
    page: 1,
    totalPages: 1,
    size: 10,
    documentFilter: null,
    nameFilter: null,
    owner: null,
    readOnly: true,
    invalidContract: true,

    init: function () {
      if ($routeParams.page) {
        this.page = parseInt($routeParams.page);
      }
      if ($routeParams.fDocument) {
        this.documentFilter = $routeParams.fDocument;
        $scope.urlPath += "fDocument=" + this.documentFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fContract) {
        this.contractFilter = $routeParams.fContract;
        $scope.urlPath += "fContract=" + this.contractFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fProperty) {
        this.propertyFilter = $routeParams.fProperty;
        $scope.urlPath += "fProperty=" + this.propertyFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fOwner) {
        this.ownerFilter = $routeParams.fOwner;
        $scope.urlPath += "fOwner=" + this.ownerFilter;
        $scope.urlPath += "&";
      }
      if ($routeParams.fName) {
        this.nameFilter = $routeParams.fName;
        $scope.urlPath += "fName=" + this.nameFilter;
        $scope.contSymbol = "&";
      }
      this.loadPage();
    },

    search: function () {
      $scope.urlSearchPath = "#" + $location.path() + "?";
      if (this.documentFilter && this.documentFilter != "") {
        $scope.urlSearchPath += "fDocument=" + this.documentFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.contractFilter && this.contractFilter != "") {
        $scope.urlSearchPath += "fContract=" + this.contractFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.propertyFilter && this.propertyFilter != "") {
        $scope.urlSearchPath += "fProperty=" + this.propertyFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.ownerFilter && this.ownerFilter != "") {
        $scope.urlSearchPath += "fOwner=" + this.ownerFilter;
        $scope.urlSearchPath += "&";
      }
      if (this.nameFilter && this.nameFilter != "") {
        $scope.urlSearchPath += "fName=" + this.nameFilter;
      }
      // console.log($scope.urlSearchPath)
    },

    loadPage: function () {
      var self = this;
      params = {
        page: this.page,
        size: this.size,
        token: $scope._session.token,
        documentFilter: this.documentFilter,
        contractFilter: this.contractFilter,
        propertyFilter: this.propertyFilter,
        ownerFilter: this.ownerFilter,
        nameFilter: this.nameFilter
      }
      RequestServices.getRequest(urlTenant, params)
          .success(function (data) {
                if (data.result == "OK") {
                  self.items = data.tenants;
                  self.totalPages = data.totalPages;
                } else {
                  alertify.alert(data.msg);
                }

              }
          );
    }
  };

  function init() {
    AdminServices.validateSession();
    $scope._session = AdminServices.getSession();
    $scope._tenantSection.init();
    AdminServices.setUrlBack($location.url());
    AdminServices.setPropertiesViews([]);
  }

  init();
});

Controllers.controller('tenantDetailCtrl', function ($scope, $sce, $location, $routeParams, AdminServices, RequestServices, EmployeeServices, DateServices) {
  var mainPath = AdminServices.getPath();
  var urlTenantContract = mainPath + "tenant/contracts";
  var urlContractCode = mainPath + "tenant/contracts/last";
  var urlLinkReqCont = mainPath + "tenant/contracts/link";
  var urlTenantSave = mainPath + "tenant/update";
  var urlGetTenant = mainPath + "tenant/get";
  var urlSetPasswordTenant = mainPath + "tenant/password";
  var urlRent = mainPath + "property/rent";
  var urlPropertyData = mainPath + "property/list";
  var urlPropRequest = mainPath + "prop-request/list";

  $scope._session = null;

  $scope.employees = {};

  // function tenantContract(contracts){
  //   this.contracts = contracts;
  // };

  // tenantContract.prototype.selected = function(){
  //   $scope.liked.contract = this.contracts.CODE;
  // }

  $scope.formLink = {
    contract: null,
    request: null,
    requestSearch: null,
    requestError: null,
    init: function (c) {
      this.clear();
      this.contract = c.ID;
    },
    clear: function () {
      this.request = null;
      this.contract = null;
      this.requestSearch = null;
      this.requestError = null;
    },
    search: function () {
      var self = this;
      this.requestError = null;
      self.request = null;
      if (!this.requestSearch) {
        this.requestError = "Escribir una solicitud";
        return;
      };

      var params = {
        token: $scope._session.token,
        page: 1,
        size: 1,
        requestFilter: this.requestSearch,// (filtro por ID del contacto)
      };
      RequestServices.getRequest(urlPropRequest, params)
          .success(function (data) {
            if (data.result == "OK") {
              if (data.requests[0]) {
                self.request = data.requests[0];
              } else {
                self.requestError = "No se encontro una solicitud con el radicado indicado."
              }
            };
          })
    },
    linked: function () {
      var self = this;

      var params = {
        ID_CONTRACT: this.contract,
        ID_REQUEST: this.request.id
      }
      RequestServices.getRequest(urlLinkReqCont, params)
          .success(function (data) {
            if (data.result == "OK") {
              $scope._tenantSection.loadTenantContracts();
              $('#addLinked').foundation('reveal', 'close');
            } else {

            }
          });
    }

  }

  $scope._tenantSection = {
    contract: null,
    // MODEL
    items: [],
    errors: [],
    contracts: [],
    msg: "",
    page: 1,
    totalPages: 1,
    size: 10,
    documentFilter: null,
    contractFilter: null,
    nameFilter: null,
    owner: null,
    readOnly: true,
    invalidContract: true,

    getTenant: function () {
      var self = this;
      self.tenant = null;
      var params = {
        token: $scope._session.token,
        id: $routeParams.tenantId
      };
      RequestServices.getRequest(urlGetTenant, params)
          .success(function (data) {
            if (data.result = "OK") {
              self.tenant = data.tenant;
              self.loadTenantContracts();
            };
          })
    },

    init: function () {
      if ($routeParams.tenantId) {
        this.getTenant();
      } else {
        this.readOnly = false;
        this.tenant = {};
      }
    },

    validateContract: function () {

      var self = this;

      self.contract.errors = [];

      if (!this.contract.CODE) {
        self.contract.errors.push("El Número de Contrato es requerido.");
      } else if (parseInt(this.contract.CODE) != (parseInt(this.lastContract) + 1)) {
        console.log(parseInt(this.contract.CODE) != (parseInt(this.lastContract) + 1));
        console.log(parseInt(this.contract.CODE) + "!=" + (parseInt(this.lastContract) + 1));
        self.contract.errors.push("El Número de Contrato es inválido, no sigue secuencia.");
      }

      if (!this.contract.PROPERTY_ADDRESS) {
        self.contract.errors.push("El código de la propiedad no es válido.");
      }

      this.invalidContract = (self.contract.errors && self.contract.errors.length);
    },

    saveTenantPassword: function () {
      console.log("Creando contraseña.");

      var self = this;
      var params = {
        ID: self.tenant.ID,
        PASSWORD: self.password,
        NAME: self.tenant.NAME.trim(),
        DOCUMENT: self.tenant.DOCUMENT,
        UPDATE: self.tenant.LOGIN ? true : false
      }

      RequestServices.postRequest(urlSetPasswordTenant, params)
          .success(function (data) {
            self.password = null;
            if (data.result == "OK") {
              console.log("Contraseña Creada.");
              alertify.success('Contraseña Asignada.');
              self.getTenant();
            }

          });
    },

    saveContract: function () {

      console.log("Creando contrato....");

      var self = this;
      params = {
        CODE: self.contract.CODE,
        START_DATE: DateServices.dateAsText(DateServices.parseDate(self.contract.START_DATE)),
        END_DATE: DateServices.dateAsText(DateServices.parseDate(self.contract.END_DATE)),
        ID_TENANT: self.tenant.ID,
        ID_PROPERTY: self.contract.ID_PROPERTY,
        CODE_PROPERTY: self.contract.PROPERTY,
        ID_EMPLOYEE: self.contract.EMPLOYEE.ID,
        ID_EMPLOYEE_CONTACT: self.contract.EMPLOYEE_CONTACT.ID,
        ID_REQUEST: self.contract.REQUEST_CODE
      }
      RequestServices.getRequest(urlRent, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.loadTenantContracts();
            }

          });
    },

    createContract: function () {

      var self = this;
      self.loadLastContractCode();

      var nextYear = new Date();
      nextYear.setMonth(nextYear.getMonth() + 6);

      self.contract = {
        CODE: null,
        tenant: self.tenant,
        PROPERTY: null,
        START_DATE: DateServices.formatDate(new Date()),
        END_DATE: DateServices.formatDate(nextYear),
        EMPLOYEE_CONTACT: $scope.employees.list[0],
        EMPLOYEE: $scope.employees.list[0]
      }
    },

    loadPropertyData: function () {

      var self = this;

      console.log(self.contract.PROPERTY);

      if (!self.contract.PROPERTY) {
        return;
      }

      params = {
        internal: self.contract.PROPERTY,
        page: 1,
        size: 1
      }

      RequestServices.getRequest(urlPropertyData, params)
          .success(function (data) {
            if (data.result == "OK") {
              if (data.properties.length) {
                self.contract.PROPERTY_ADDRESS = data.properties[0].ADDRESS;
                self.contract.PROPERTY_OWNER = data.properties[0].OWNER_NAME;
                self.contract.ID_PROPERTY = data.properties[0].ID;
              } else {
                self.contract.PROPERTY_ADDRESS = null;
                self.contract.PROPERTY_OWNER = null;
                self.contract.ID_PROPERTY = null;
              }
            }
            self.validateContract();
          });
    },

    loadLastContractCode: function () {
      var self = this, params = { token: $scope._session.token }
      RequestServices.getRequest(urlContractCode, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.lastContract = data.contractCode;
            } else {
              alertify.alert(data.msg);
            }
          });
    },

    loadTenantContracts: function () {
      var self = this,
          params = {
            tenant: this.tenant.ID,
            token: $scope._session.token
          }
      RequestServices.getRequest(urlTenantContract, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.contracts = data.contracts;
            } else {
              alertify.alert(data.msg);
            }
          });
    },

    cancel: function () {
      if (this.tenant.ID) {
        this.readOnly = true;
        this.errors = [];
        this.msg = null;
      } else {
        $location.path("/Tenant");
      }
    },

    validate: function () {

      this.errors = [];

      if (!this.tenant.NAME) {
        this.errors.push("El 'Nombre' es requerido");
      }

      if (!this.tenant.DOCUMENT) {
        this.errors.push("El 'Documento' es requerido");
      }
    },

    edit: function () {
      this.readOnly = false;
      this.msg = null;
    },

    save: function () {

      var self = this;

      this.validate();

      // if there are errors, dont save
      if (this.errors.length) {
        return;
      }
      var params = {
        ID: this.tenant.ID,
        NAME: this.tenant.NAME,
        DOCUMENT: this.tenant.DOCUMENT,
        MAIL: this.tenant.MAIL,
        token: $scope._session.token
      }

      RequestServices.getRequest(urlTenantSave, params)
          .success(function (data) {
            if (data.result == "OK") {
              self.msg = "Inquilino Guardado.";
              self.readOnly = true;
              self.tenant.ID = data.ID;
            } else {
              alertify.alert(data.msg);
            }
          });
    }
  };

  function init() {
    AdminServices.validateSession();
    $(".date").fdatepicker();
    $scope._session = AdminServices.getSession();
    $scope.employees = EmployeeServices;
    $(document).foundation();
    $scope._tenantSection.init();
    AdminServices.setUrlBack($location.url());
    AdminServices.setPropertiesViews([]);
  };

  init();
});

