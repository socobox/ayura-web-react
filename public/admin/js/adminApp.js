var adminApp = angular.module('adminayura', [
  'ngRoute',
  'ngCookies',
  'controllers',
  'angucomplete',
  'ngSanitize',
  'ngCsv'
]);

adminApp.config(function ($routeProvider) {
  $routeProvider
    .when('/', {
      templateUrl: 'partials/login.html',
      controller: 'loginCtrl'
    })
    .when('/dashboard', {
      templateUrl: 'partials/dashboard.html',
      controller: 'dashboardCtrl'
    })
    .when('/Contact', {
      templateUrl: 'partials/contactUs.html',
      controller: 'ContactCtrl'
    })
    .when('/Contact/add', {
      templateUrl: 'partials/contactUsDetail.html',
      controller: 'ContactDetailCtrl'
    })
    .when('/Contact/:contactId', {
      templateUrl: 'partials/contactUsDetail.html',
      controller: 'ContactDetailCtrl'
    })
    .when('/Agenda', {
      templateUrl: 'partials/agenda.html',
      controller: 'agendaCtrl'
    })
    .when('/AgendarCita', {
      templateUrl: 'partials/agendaMaster.html',
      controller: 'agendaMasterCtrl'
    })
    .when('/AgendarCita/:contactId', {
      templateUrl: 'partials/agendaMaster.html',
      controller: 'agendaMasterCtrl'
    })
    .when('/Properties', {
      templateUrl: 'partials/properties.html',
      controller: 'propertiesCtrl'
    })
    .when('/Properties/add', {
      templateUrl: 'partials/propertiesDetail.html',
      controller: 'propertiesDetailCtrl'
    })
    .when('/Properties/:propId', {
      templateUrl: 'partials/propertiesDetail.html',
      controller: 'propertiesDetailCtrl'
    })
    .when('/Owner', {
      templateUrl: 'partials/owner.html',
      controller: 'ownerCtrl'
    })
    .when('/Owner/find', {
      templateUrl: 'partials/owner.html',
      controller: 'ownerCtrl'
    })
    .when('/Owner/add', {
      templateUrl: 'partials/ownerDetail.html',
      controller: 'ownerDetailCtrl'
    })
    .when('/Owner/:ownerId', {
      templateUrl: 'partials/ownerDetail.html',
      controller: 'ownerDetailCtrl'
    })
    .when('/Tenant', {
      templateUrl: 'partials/tenant.html',
      controller: 'tenantCtrl'
    })
    .when('/Tenant/add', {
      templateUrl: 'partials/tenantDetail.html',
      controller: 'tenantDetailCtrl'
    })
    .when('/Tenant/:tenantId', {
      templateUrl: 'partials/tenantDetail.html',
      controller: 'tenantDetailCtrl'
    })
    .when('/Solicitudes', {
      templateUrl: 'partials/request.html',
      controller: 'requestCtrl'
    })
    .when('/Account', {
      templateUrl: 'partials/account.html',
      controller: 'accountCtrl'
    })
    .when('/DigitalArchive', {
      templateUrl: 'partials/digitalArchive.html',
      controller: 'digitalArchiveCtrl'
    })
    .when('/DigitalArchive/:folderId/:entity', {
      templateUrl: 'partials/digitalArchive.html',
      controller: 'digitalArchiveCtrl'
    })
    .when('/Contract/:contractId', {
      templateUrl: 'partials/contracts/detail.html',
      controller: 'contractDetailCtrl'
    })
    .when('/Contract', {
      templateUrl: 'partials/contracts/list.html',
      controller: 'contractCtrl'
    })
    .when('/Contract/add', {
      templateUrl: 'partials/contracts/detail.html',
      controller: 'contractDetailCtrl'
    })
    .when('/Consultants', {
      templateUrl: 'partials/reports/consultants.html',
      controller: 'consultantsCtrl'
    })
    .when('/contact-request/status', {
      templateUrl: 'partials/reports/requests.html',
      controller: 'contactRequestCtrl'
    });
});

adminApp.directive('loading', [
  '$http',
  function ($http) {
    return {
      restrict: 'A',
      link: function (scope, elm, attrs) {
        scope.isLoading = function () {
          return $http.pendingRequests.length > 0;
        };

        scope.$watch(scope.isLoading, function (v) {
          if (v) {
            elm.show();
          } else {
            elm.hide();
          }
        });
      }
    };
  }
]);
adminApp.directive('capitalize', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, modelCtrl) {
      var capitalize = function (inputValue) {
        if (inputValue) {
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }

          return capitalized;
        }

        return inputValue;
      };
      modelCtrl.$parsers.push(capitalize);
      capitalize(scope[attrs.ngModel]); // capitalize initial value
    }
  };
});
adminApp.directive('ngEnter', function () {
  return function (scope, element, attrs) {
    element.bind('keydown keypress', function (event) {
      if (event.which === 13) {
        scope.$apply(function () {
          scope.$eval(attrs.ngEnter, {event: event});
        });

        event.preventDefault();
      }
    });
  };
});
adminApp.directive('fileUpload', function () {
  return {
    scope: true, //create a new scope
    link: function (scope, el, attrs) {
      el.bind('change', function (event) {
        var files = event.target.files;
        //iterate files since 'multiple' may be specified on the element
        for (var i = 0; i < files.length; i++) {
          //emit event upward
          scope.$emit('fileSelected', {
            file: files[i],
            control: event.target
          });
        }
      });
    }
  };
});
adminApp.directive('dropzone', function () {
  return {
    restrict: 'A',
    link: function (scope, elem, attrs) {
      elem.bind('dragover', function (evt) {
        evt.stopPropagation();
        evt.preventDefault();
        elem.addClass('hover');
      });
      elem.bind('dragenter', function (evt) {
        evt.stopPropagation();
        evt.preventDefault();
      });
      elem.bind('dragleave', function (evt) {
        elem.removeClass('hover');
      });
      elem.bind('drop', function (evt) {
        evt.stopPropagation();
        evt.preventDefault();
        elem.removeClass('hover');
        evt.dataTransfer = evt.dataTransfer
          ? evt.dataTransfer
          : evt.originalEvent.dataTransfer;
        var files = evt.dataTransfer.files;
        for (var i = 0, f; (f = files[i]); i++) {
          var reader = new FileReader();
          reader.readAsArrayBuffer(f);

          reader.onload = (function (theFile) {
            return function (e) {
              // var newFile = {
              //   name : theFile.name,
              //   type : theFile.type,
              //   size : theFile.size,
              //   lastModifiedDate : theFile.lastModifiedDate
              // }
              scope.$emit('fileSelected', {
                file: theFile,
                control: evt.target
              });
            };
          })(f);
        }
      });
    }
  };
});
adminApp.directive('d3asesor', function ($parse) {
  return {
    restrict: 'E',
    replace: true,
    scope: false,
    template: '<div id="d3asesor"></div>',
    link: function (scope, element, attrs) {
      scope.$watch(
        'asesorasData',
        function (newValue, oldValue) {
          var data = newValue,
            chart = d3
              .select('#d3asesor')
              .html('')
              .append('div')
              .attr('class', 'chart')
              .selectAll('div')
              .data(data)
              .enter()
              .append('a')
              .attr('href', function (d) {
                return '#/Contact?fAdviserName=' + d.NAME;
              })
              .html(function (d) {
                if (d.COUNT > 200) {
                  return (
                    d.NAME + "(<span class='alert'>" + d.COUNT + '</span>)'
                  );
                } else {
                  return d.NAME + ' (' + d.COUNT + ')';
                }
              })
              .transition()
              .ease('elastic')
              .style('width', function (d) {
                if (d.COUNT > 500) {
                  return 100 + '%';
                } else {
                  return 250 + d.COUNT + 'px';
                }
              });
        },
        true
      );
    }
  };
});
adminApp.directive('d3tipo', function ($parse) {
  return {
    restrict: 'E',
    replace: true,
    scope: false,
    template: '<div id="d3tipo"></div>',
    link: function (scope, element, attrs) {
      scope.$watch(
        'tipoData',
        function (newValue, oldValue) {
          var data = newValue,
            chart = d3
              .select('#d3tipo')
              .html('')
              .append('div')
              .attr('class', 'chart')
              .selectAll('div')
              .data(data)
              .enter()
              .append('a')
              .attr('href', function (d) {
                return '#/Contact?fType=' + d.NAME;
              })
              .transition()
              .ease('elastic')
              .style('width', function (d) {
                if (d.COUNT > 500) {
                  return 100 + '%';
                } else {
                  return 250 + d.COUNT + 'px';
                }
              })
              .text(function (d) {
                return d.NAME + ' (' + d.COUNT + ')';
              });
        },
        true
      );
    }
  };
});
adminApp.directive('repeatFinish', [
  '$timeout',
  function ($timeout) {
    return {
      scope: true,
      link: function (scope, element, attrs) {
        $timeout(function () {
          if (scope.$last) {
            $('.flexslider').flexslider({
              animation: 'slide',
              controlNav: false,
              animationLoop: false,
              slideshow: false,
              itemWidth: 130,
              keyboard: false,
              minItems: 3,
              maxItems: 7
            });
          }
        }, 0); //Calling a scoped method
      }
    };
  }
]);

adminApp.directive('render360', [
  '$timeout',
  function ($timeout) {
    return {
      scope: true,
      link: function (scope, element, attrs) {
        $timeout(function () {
          if (scope.$last) {
            $('.test-popup').magnificPopup({
              type: 'image',
              mainClass: 'myPano',
              callbacks: {
                open: function () {
                  console.log($('.mfp-img'));
                  var img = $('.mfp-img').attr('src');

                  $('.js-myPano').pano({
                    img: img
                  });
                  $('.mfp-img').css('display', 'none');

                  // Will fire when this exact popup is opened
                  // this - is Magnific Popup object
                }
              },
              image: {
                markup:
                  '<div class="mfp-figure">' +
                  '<div class="mfp-close"></div>' +
                  '<div class="js-myPano pano" ></div>' +
                  '<div style="display:none;" class="mfp-img"></div>' +
                  '<div class="mfp-bottom-bar">' +
                  '<div class="mfp-title"></div>' +
                  '<div class="mfp-counter"></div>' +
                  '</div>' +
                  '</div>', // Popup HTML markup. `.mfp-img` div will be replaced with img tag, `.mfp-close` by close button
                titleSrc: 'title', // Attribute of the target element that contains caption for the slide.
                // Or the function that should return the title. For example:
                // titleSrc: function(item) {
                //   return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
                // }

                verticalFit: true, // Fits image in area vertically

                tError: '<a href="%url%">The image</a> could not be loaded.' // Error message
              }
            });
          }
        }, 0); //Calling a scoped method
      }
    };
  }
]);

adminApp.directive('refreshDropdown', [
  '$timeout',
  function ($timeout) {
    return {
      scope: true,
      link: function (scope, element, attrs) {
        $timeout(function () {
          if (scope.$last) {
            $(document).foundation();
          }
        }, 0); //Calling a scoped method
      }
    };
  }
]);

adminApp.directive('numbersOnly', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, modelCtrl) {
      modelCtrl.$parsers.push(function (inputValue) {
        // this next if is necessary for when using ng-required on your input.
        // In such cases, when a letter is typed first, this parser will be called
        // again, and the 2nd time, the value will be undefined
        if (inputValue == undefined) return '';
        var transformedInput = inputValue.replace(/[^0-9]/g, '');
        if (transformedInput != inputValue) {
          modelCtrl.$setViewValue(transformedInput);
          modelCtrl.$render();
        }

        return transformedInput;
      });
    }
  };
});
