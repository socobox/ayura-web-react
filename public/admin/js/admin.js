var appModule = angular.module('adminayura', ['ngCookies', "angucomplete"])
.directive('loading', ['$http' , function ($http) {
  return {
    restrict: 'A',
    link: function (scope, elm, attrs) {
      scope.isLoading = function () {
        return $http.pendingRequests.length > 0;
      };

      scope.$watch(scope.isLoading, function (v) {
        if (v) {
          elm.show();
        } else {
          elm.hide();
        }
      });
    }
  };

}]).directive('capitalize', function () {
  return {
    require: 'ngModel',
    link: function (scope, element, attrs, modelCtrl) {
      var capitalize = function (inputValue) {

        if (inputValue) {
          var capitalized = inputValue.toUpperCase();
          if (capitalized !== inputValue) {
            modelCtrl.$setViewValue(capitalized);
            modelCtrl.$render();
          }

          return capitalized;
        }

        return inputValue;
      };
      modelCtrl.$parsers.push(capitalize);
      capitalize(scope[attrs.ngModel]);  // capitalize initial value
    }
  };
}).directive('ngEnter', function () {
  return function (scope, element, attrs) {
    element.bind("keydown keypress", function (event) {
      if (event.which === 13) {
        scope.$apply(function () {
          scope.$eval(attrs.ngEnter, {'event': event});
        });

        event.preventDefault();
      }
    });
  };
}).directive('fileUpload', function () {
  return {
    scope: true,        //create a new scope
    link: function (scope, el, attrs) {
      el.bind('change', function (event) {
        var files = event.target.files;
        //iterate files since 'multiple' may be specified on the element
        for (var i = 0; i < files.length; i++) {
          //emit event upward
          scope.$emit("fileSelected", { file: files[i], control: event.target});

        }
      });
    }
  };
}).directive('d3asesor', function ($parse) {
  return {
    restrict: 'E',
    replace: true,
    scope: false,
    template: '<div id="d3asesor"></div>',
    link: function (scope, element, attrs) {


      scope.$watch('asesorasData', function (newValue, oldValue) {
        var data = newValue,
          chart = d3.select('#d3asesor').html('')
            .append("div").attr("class", "chart")
            .selectAll('div')
            .data(data).enter()
            .append("div")
            .transition().ease("elastic")
            .style("width", function (d) {
              return (250 + d.COUNT) + "px";
            })
            .text(function (d) {
              return d.NAME + " (" + d.COUNT + ")";
            });
      }, true);


    }
  };
}).directive('d3tipo', function ($parse) {
  return {
    restrict: 'E',
    replace: true,
    scope: false,
    template: '<div id="d3tipo"></div>',
    link: function (scope, element, attrs) {


      scope.$watch('tipoData', function (newValue, oldValue) {
        var data = newValue,
          chart = d3.select('#d3tipo').html('')
            .append("div").attr("class", "chart")
            .selectAll('div')
            .data(data).enter()
            .append("div")
            .transition().ease("elastic")
            .style("width", function (d) {
              return (250 + d.COUNT) + "px";
            })
            .text(function (d) {
              return d.NAME + " (" + d.COUNT + ")";
            });
      }, true);


    }
  };
}).directive('numbersOnly', function(){
   return {
     require: 'ngModel',
     link: function(scope, element, attrs, modelCtrl) {
       modelCtrl.$parsers.push(function (inputValue) {
           // this next if is necessary for when using ng-required on your input. 
           // In such cases, when a letter is typed first, this parser will be called
           // again, and the 2nd time, the value will be undefined
           if (inputValue == undefined) return '' 
           var transformedInput = inputValue.replace(/[^0-9]/g, ''); 
           if (transformedInput!=inputValue) {
              modelCtrl.$setViewValue(transformedInput);
              modelCtrl.$render();
           }         

           return transformedInput;         
       });
     }
   };
});


appModule.controller('AdminCtrl', function ($scope, $http, $sce, $cookies, $document) {

  $scope.asesorasData = [];
  $scope.tipoData = [];

  //an array of files selected
  $scope.files = [];

  $scope.fileControl = null;

  //listen for the file selected event
  $scope.$on("fileSelected", function (event, args) {
    $scope.$apply(function () {
      //add the file object to the scope's files collection
      $scope.files.push(args.file);
      $scope.fileControl = args.control;
    });
  });

  //the save method
  $scope.save = function () {

    //now add all of the assigned files
    for (var ic = 0; ic < $scope.files.length; ic++) {
      $scope.send($scope.files[ic]);
    }


    $scope.fileControl.form.reset();
    $scope.files = [];
  };


  $scope.send = function (file) {

    $http({
      method: 'POST',
      cache: false,
      url: "http://www.arrendamientosayura.com/property/photo/upload",
      //IMPORTANT!!! You might think this should be set to 'multipart/form-data'
      // but this is not true because when we are sending up files the request
      // needs to include a 'boundary' parameter which identifies the boundary
      // name between parts in this multi-part request and setting the Content-type
      // manually will not set this boundary parameter. For whatever reason,
      // setting the Content-type to 'false' will force the request to automatically
      // populate the headers properly including the boundary parameter.
      headers: { 'Content-Type': undefined },
      //This method will allow us to change how the data is sent up to the server
      // for which we'll need to encapsulate the model data in 'FormData'
      transformRequest: function (data) {
        var formData = new FormData();

        //now add all of the assigned files
        formData.append("file", data.file, $scope.createToken());
        //need to convert our json object to a string version of json otherwise
        // the browser will do a 'toString()' on the object which will result
        // in the value '[Object object]' on the server.
        formData.append("model", angular.toJson(data.model));

        return formData;
      },
      //Create an object that contains the model and files which will be transformed
      // in the above transformRequest method
      data: { model: $scope._propSection.prop, file: file }
    }).
      success(function (data, status, headers, config) {
        $scope._propSection.loadPhotos();
      }).
      error(function (data, status, headers, config) {
        alert("failed!");
      });
  };


  $scope._session = {
    token: null,
    screen: null,
    id: 0
  };

  $scope.createToken = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  };

  $scope.flash = {
    "employee": null,
    "property": null,
    "owner": null,
    copyModel: false,
    callback: null,
    clear: function () {
      this.copyMode = false;
      this.callback = null;
    }
  };

  $scope._statusList = [
    {value: "", label: "Todos"},
    {value: 1, label: "Arrendado"},
    {value: 99, label: "Disponible/Solicitud"},
    {value: 0, label: "Disponible"},
    {value: 4, label: "Liberado"},
    {value: 2, label: "Retirado"},
    {value: 3, label: "Solicitud"},
    {value: 5, label: "Vendido"}
  ];

  $scope.boolOptions = [
    {value: null, label: "Todos"},
    {value: "true", label: "Con"},
    {value: "false", label: "Sin"}
  ];

  $scope._statusMap = {
    "": "Todos",
    1: "Arrendado",
    0: "Disponible",
    99: "Disponible/Solicitud",
    4: "Liberado",
    2: "Retirado",
    3: "Solicitud",
    5: "Vendido"
  };

  $scope.features = [];


  $scope._cities = [];

  $scope._propertyTypes = [];
  $scope._propertyTypesMap = {};

  $scope._propertyTypesMap = {};

  $scope._requestList = [];
  $scope._userList = [];
  $scope._userMap = [];

  $scope._serviceTypesMap = {
    0: "Arrendamiento",
    1: "Venta"
  };

  $scope._requestManager = {

    page: 1,
    totalPages: 1,

    firstPage: function () {

      if (this.page == 1) {
        return;
      }

      this.page = 1;
      this.loadPage();

    },
    prevPage: function () {

      if (this.page == 1) {
        return;
      }

      this.page--;
      this.loadPage();

    },
    nextPage: function () {
      if (this.page == this.totalPages) {
        return;
      }

      this.page++;
      this.loadPage();

    },
    lastPage: function () {
      if (this.page == this.totalPages) {
        return;
      }

      this.page = this.totalPages;
      this.loadPage();
    },
    loadPage: function () {
      $scope.loadContactRequests();
    },

    search: function () {
      this.page = 1;
      this.loadPage();
    }
  }


  $scope.range = function (from, to) {

    var tmp = [];

    for (var i = from; i <= to; i++) {
      tmp.push(i);
    }

    return tmp;
  };


  $scope._dashboard = {

    loadCharts: function () {

      $scope._session.screen = "dashboard";


      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/contact-request/stats/sales",
        params: {
        }
      }).success(function (data) {
        $scope.asesorasData = data.sales;
      });


      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/contact-request/stats/type",
        params: {
        }
      }).success(function (data) {
        $scope.tipoData = data.sales;
      });

    }
  };

  $scope.featureValues = {

    options: {},

    load: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/features/options",
        params: {}
      }).success(function (data) {

        if (data.result == "OK") {
          self.options = data.values;
        }

      });
    }

  };

  $scope.employees = {
    options: [],
    list: [],
    map: {},
    load: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/employee/list",
        params: {}
      }).success(function (data) {

        if (data.result == "OK") {
          self.options = data.employees.slice(0);
          self.list = data.employees;
          self.options.unshift({ID: "", NAME: "Todos"});

          angular.forEach(data.employees, function (value, key) {
            self.map[value.ID] = value;
          });
        }

      });
    },
    find: function (value) {

      var employee = null;

      angular.forEach(data.options, function (value, key) {

        if (value.value == value) {
          employee = value;
        }


      });

      return employee;

    }
  };

  $scope.loadFeatures = function () {
    $http({
      method: "GET",
      url: "features/list",
      params: {}
    }).success(function (data) {

      if (data.result == "OK") {
        $scope.features = data.features;
      }

    });
  };


  $scope.loadCities = function () {
    $http({
      method: "GET",
      url: "address/city/options",
      params: {}
    }).success(function (data) {

      if (data.result == "OK") {
        $scope._cities = data.options;
        $scope._cities.unshift({value: null, label: "Todos"});
      }

    });
  };


  $scope.loadEmployees =

    $scope.loadPropertyTypes = function () {
      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/type/options",
        params: {}
      }).success(function (data) {

        if (data.result == "OK") {
          $scope._propertyTypes = data.options;
          $scope._propertyTypes.unshift({value: "", label: "Todos"});

          angular.forEach(data.options, function (value, key) {
            $scope._propertyTypesMap[value.value] = value.label;
          });
        }

      });

    };


  $scope._contactSection = {

    errors: [],

    userList: [],

    typeList: [
      { value: null, label: "Todos"},
      { value: "WEB", label: "Página Web"},
      { value: "EMAIL", label: "Correo Electrónico"},
      { value: "TEL", label: "Llamada"},
      { value: "PERSONAL", label: "Visita"}
    ],

    newList: [
      { value: "EMAIL", label: "Correo Electrónico"},
      { value: "TEL", label: "Llamada"},
      { value: "PERSONAL", label: "Visita"}
    ],

    typeMap: {
      WEB: "Página Web",
      EMAIL: "Correo Electrónico",
      TEL: "Llamada",
      PERSONAL: "Visita"
    },


    statusList: [
      { value: "TODOS", label: "Todos"},
      {   value: "PENDIENTE", label: "Pendiente"},
      {   value: "FINALIZADO", label: "Finalizado"},
      {   value: "PROCESANDO", label: "Procesando"}
    ],

    statusMap: {
      PENDIENTE: 1,
      FINALIZADO: 2,
      PROCESANDO: 3
    },

    request: null,
    readOnly: true,
    assignTo: null,

    scheduleDate: function () {
      console.log(JSON.stringify(this.request));
      $scope._agenda.currentContact = this.request;
      $scope._agenda.init();
    },


    isType: function (type) {

      if (!this.request) {
        return false;
      }

      var tmp = this.request.type;

      if (tmp.value) {
        tmp = tmp.value;
      }

      return (tmp == type);
    },

    transferRequest: function () {

      var self = this;

      //owner, id, token

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/contact-request/assign",
        params: {
          token: $scope._session.token,
          id: self.request.id,
          owner: self.assignTo.ID
        }
      }).success(function (data) {

          if (data.result == "OK") {
            alert("Solicitud Asignada.");
            $scope.leaveRequest();
          } else {
            alert(data.msg);
          }

        }
      );

    },

    clear: function () {

      $scope._requestFilters.type = this.typeList[0];
      $scope._requestFilters.status = $scope._contactSection.statusList[1];
      $scope._requestFilters.user = this.assignTo = $scope._userMap[$scope._session.id];
      $scope._requestFilters.contactName = null;
      $scope._requestFilters.userName = null;
      $scope._requestManager.search();
    },

    requestDetail: function (request) {
      var self = this;
      this.readOnly = true;
      this.request = request;
      this.assignTo = $scope._userMap[$scope._session.id];
      this.request.htmlMessage = this.request.message ? ($sce.trustAsHtml("<p>" + this.request.message.split("\n").join("<br>") + "</p>")) : "";
      $scope.loadRequestUpdates();
      $scope._session.screen = 'contactenos-detalle';


      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/agenda/findAllByRequest",
        params: {
          contact: self.request.id
        }
      }).success(function (data) {

        if (data.result == "OK") {


          request.appointments = [];


          angular.forEach(data.appointments, function (app, key) {
            app.SCHEDULED_LABEL = app.SCHEDULED.split("-").join("/")
            app.CREATED = new Date(app.CREATED);
            app.TIME_FROM = app.TIME_FROM.substring(0, 5);


            app.TIME_TO = app.TIME_TO.substring(0, 5);
            request.appointments.push(app);

          });


        } else {
          alert(data.msg);
        }

      });
    },

    init: function () {
      $scope._requestFilters.type = this.typeList[0]
    },

    save: function () {

      var self = this;

      self.errors = [];

      if (!self.request.name) {
        self.errors.push("El nombre es requerido")
      }

      if (!self.request.phone) {
        self.errors.push("El Teléfono es requerido")
      }

      if (self.request.type.value == "EMAIL" && !self.request.email) {
        self.errors.push("El email es requerido")
      }

      if (self.errors.length) {
        return;
      }


      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/contact-request/add",
        params: {
          name: self.request.name,
          email: self.request.email,
          phone: self.request.phone,
          msg: self.request.message,
          type: self.request.type.value,
          user: $scope._session.id
        }
      }).success(function (data) {

        if (data.result == "OK") {
          self.request.type = self.request.type.label;
          self.request.updated = self.request.created;
          self.request.id = data.id;
          self.requestDetail(self.request);
        } else {
          alert(data.msg);
        }

      });
    },

    add: function () {
      this.readOnly = false;
      this.request = {
        type: this.newList[2],
        created: new Date(),
        status: this.statusList[1]
      };

      console.log("" + this.request.status.label);
      $scope._session.screen = 'contactenos-detalle';
    }
  };


  $scope._tenantSection = {


    contract: null,

    // MODEL
    items: [],
    errors: [],
    contracts: [],
    msg: "",
    page: 1,
    totalPages: 1,
    size: 10,
    documentFilter: null,
    nameFilter: null,
    owner: null,
    readOnly: true,
    invalidContract: true,

    init: function () {
      $scope._session.screen = 'inquilinos';
      this.loadPage();
    },

    // BEHAVIOR
    reset: function () {
      this.page = 1;
      this.items = [];
      this.documentFilter = null;
      this.nameFilter = null;
      this.readOnly = true;
      this.msg = null;
    },

    nextPage: function () {

      if (this.page == this.totalPages) {
        return;
      }

      this.page++;
      this.loadPage();
    },

    prevPage: function () {

      if (this.page == 1) {
        return;
      }

      this.page--;
      this.loadPage();
    },


    backToList: function () {
      this.readOnly = true;
      this.errors = [];
      this.contracts = [];
      this.contract = null;
      this.invalidContract = true;
      this.tenant = null;
      this.msg = null;
      $scope._session.screen = 'inquilinos';
      this.loadPage();
    },

    tenantDetails: function (tmp) {
      this.tenant = tmp;
      $scope._session.screen = 'inquilinos-detalle';
      this.loadTenantContracts();
    },


    validateContract: function () {


      var self = this;


      self.contract.errors = [];

      if (!this.contract.CODE) {
        self.contract.errors.push("El Número de Contrato es requerido.");
      } else if (parseInt(this.contract.CODE) != (parseInt(this.lastContract) + 1)) {
        console.log(parseInt(this.contract.CODE) != (parseInt(this.lastContract) + 1));
        console.log(parseInt(this.contract.CODE) + "!=" + (parseInt(this.lastContract) + 1));
        self.contract.errors.push("El Número de Contrato es inválido, no sigue secuencia.");
      }

      if (!this.contract.PROPERTY_ADDRESS) {
        self.contract.errors.push("El código de la propiedad no es válido.");
      }


      this.invalidContract = (self.contract.errors && self.contract.errors.length);
    },


    saveContract: function () {

      console.log("Creando contrato....");

      var self = this;

      console.log({
        CODE: self.contract.CODE,
        START_DATE: ($scope.parseDate(self.contract.START_DATE)),
        END_DATE: ($scope.parseDate(self.contract.END_DATE)),
        ID_TENANT: self.tenant.ID,
        ID_PROPERTY: self.contract.ID_PROPERTY,
        CODE_PROPERTY: self.contract.PROPERTY,
        ID_EMPLOYEE: self.contract.EMPLOYEE.ID,
        ID_EMPLOYEE_CONTACT: self.contract.EMPLOYEE_CONTACT.ID
      });

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/rent",
        params: {
          CODE: self.contract.CODE,
          START_DATE: $scope.dateAsText($scope.parseDate(self.contract.START_DATE)),
          END_DATE: $scope.dateAsText($scope.parseDate(self.contract.END_DATE)),
          ID_TENANT: self.tenant.ID,
          ID_PROPERTY: self.contract.ID_PROPERTY,
          CODE_PROPERTY: self.contract.PROPERTY,
          ID_EMPLOYEE: self.contract.EMPLOYEE.ID,
          ID_EMPLOYEE_CONTACT: self.contract.EMPLOYEE_CONTACT.ID
        }
      }).success(function (data) {

        if (data.result == "OK") {
          self.loadTenantContracts();
        }

      });

    },

    createContract: function () {

      var self = this;
      self.loadLastContractCode();

      var nextYear = new Date();
      nextYear.setMonth(nextYear.getMonth() + 6);


      self.contract = {
        CODE: null,
        tenant: self.tenant,
        PROPERTY: null,
        START_DATE: $scope.formatDate(new Date()),
        END_DATE: $scope.formatDate(nextYear),
        EMPLOYEE_CONTACT: $scope.employees.list[0],
        EMPLOYEE: $scope.employees.list[0]
      }

    },

    loadPropertyData: function () {

      var self = this;

      console.log(self.contract.PROPERTY);

      if (!self.contract.PROPERTY) {
        return;
      }

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/list",
        params: {
          internal: self.contract.PROPERTY,
          page: 1,
          size: 1
        }
      }).success(function (data) {

        if (data.result == "OK") {
          if (data.properties.length) {
            self.contract.PROPERTY_ADDRESS = data.properties[0].ADDRESS;
            self.contract.PROPERTY_OWNER = data.properties[0].OWNER_NAME;
            self.contract.ID_PROPERTY = data.properties[0].ID;

          } else {
            self.contract.PROPERTY_ADDRESS = null;
            self.contract.PROPERTY_OWNER = null;
            self.contract.ID_PROPERTY = null;
          }

        }

        self.validateContract();

      });

    },

    loadLastContractCode: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/tenant/contracts/last",
        params: {
          token: $scope._session.token
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.lastContract = data.contractCode;
          } else {
            alert(data.msg);
          }

        }
      );

    },


    loadTenantContracts: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/tenant/contracts",
        params: {
          tenant: this.tenant.ID,
          token: $scope._session.token
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.contracts = data.contracts;
            console.log(self.contracts);
          } else {
            alert(data.msg);
          }

        }
      );

    },

    search: function () {
      this.page = 1;
      this.items = [];

      this.loadPage();
    },

    cancel: function () {
      if (this.tenant.ID) {
        this.readOnly = true;
        this.errors = [];
        this.msg = null;
      } else {
        this.backToList();
      }

    },

    validate: function () {

      this.errors = [];

      if (!this.tenant.NAME) {
        this.errors.push("El 'Nombre' es requerido");
      }

      if (!this.tenant.DOCUMENT) {
        this.errors.push("El 'Documento' es requerido");
      }

    },

    edit: function () {
      this.readOnly = false;
      this.msg = null;
    },

    add: function () {
      this.readOnly = false;
      this.tenant = {};
      $scope._session.screen = 'inquilinos-detalle';
    },

    save: function () {

      var self = this;

      this.validate();

      // if there are errors, dont save
      if (this.errors.length) {
        return;
      }

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/tenant/update",
        params: {
          ID: this.tenant.ID,
          NAME: this.tenant.NAME,
          DOCUMENT: this.tenant.DOCUMENT,
          MAIL: this.tenant.MAIL,
          token: $scope._session.token

        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.msg = "Inquilino Guardado.";
            self.readOnly = true;
            self.tenant.ID = data.ID;
          } else {
            alert(data.msg);
          }

        }
      );

    },

    loadPage: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/tenant/list",
        params: {
          page: this.page,
          size: this.size,
          token: $scope._session.token,
          documentFilter: this.documentFilter,
          nameFilter: this.nameFilter
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.items = data.tenants;
            self.totalPages = data.totalPages;
          } else {
            alert(data.msg);
          }

        }
      );

    }

  };


  $scope._addressBuilder = {

    viaOptions: [],
    quadrantOptions: [],
    propTypeOptions: [],

    address: {
      via1: {
        via: null,
        number: "",
        letter: "",
        quadrant: null
      },
      via2: {
        number: "",
        letter: "",
        quadrant: null
      },
      number: "",
      prop1: {
        propType: null,
        propNumber: ""
      },
      prop2: {
        propType: null,
        propNumber: ""
      }
    },


    clear: function () {

      var self = this;

      self.address = {
        via1: {
          via: self.viaOptions[0],
          number: "",
          letter: "",
          quadrant: self.quadrantOptions[0]
        },
        via2: {
          number: "",
          letter: "",
          quadrant: self.quadrantOptions[0]
        },
        number: "",
        prop1: {
          propType: self.propTypeOptions[0],
          propNumber: ""
        },
        prop2: {
          propType: self.propTypeOptions[0],
          propNumber: ""
        }
      }

    },

    build: function () {
      var tmp = this.address.via1.via.ABBREVIATION + ' ';
      tmp += this.address.via1.number ? this.address.via1.number + ' ' : '';
      tmp += this.address.via1.letter ? this.address.via1.letter + ' ' : '';
      tmp += this.address.via1.quadrant.NAME != "No Aplica" ? this.address.via1.quadrant.NAME + ' ' : '';
      tmp += this.address.via2.number ? this.address.via2.number + ' ' : '';
      tmp += this.address.via2.letter ? this.address.via2.letter + ' ' : '';
      tmp += this.address.via2.quadrant.NAME != "No Aplica" ? this.address.via2.quadrant.NAME + ' ' : '';
      tmp += this.address.number ? this.address.number + ' ' : '';
      tmp += this.address.prop1.propType.ABBREVIATION ? this.address.prop1.propType.ABBREVIATION + ' ' : '';
      tmp += this.address.prop1.number ? this.address.prop1.number + ' ' : '';
      tmp += this.address.prop2.propType.ABBREVIATION ? this.address.prop2.propType.ABBREVIATION + ' ' : '';
      tmp += this.address.prop2.number ? this.address.prop2.number : '';

      return tmp;
    },

    load: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/address/builder/options",
        params: {
          token: $scope._session.token,
          table: "dir_via"
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.viaOptions = data.options;
            self.address.via1.via = self.viaOptions[0];
          } else {
            alert(data.msg);
          }

        }
      );

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/address/builder/options",
        params: {
          token: $scope._session.token,
          table: "dir_quadrant"
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.quadrantOptions = data.options;
            self.quadrantOptions.unshift({ID: 0, NAME: "No Aplica", ABBREVIATION: ""});
            self.address.via1.quadrant = self.quadrantOptions[0];
            self.address.via2.quadrant = self.quadrantOptions[0];
          } else {
            alert(data.msg);
          }

        }
      );

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/address/builder/options",
        params: {
          token: $scope._session.token,
          table: "dir_property_type"
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.propTypeOptions = data.options;
            self.propTypeOptions.unshift({ID: 0, NAME: "No Aplica", ABBREVIATION: ""});
            self.address.prop1.propType = self.propTypeOptions[0];
            self.address.prop2.propType = self.propTypeOptions[0];
          } else {
            alert(data.msg);
          }

        }
      );

    }
  };


//    $scope._contractViewer = {
//
//        contract:null,
//
//        loadContractByProperty:function(ID)
//
//
//    }


  $scope._settingsSection = {

    newPassword: null,
    confirmNewPassword: null,
    currentPassword: null,

    logout: function () {

    },

    changePassword: function () {

      var self = this;

      if (!self.currentPassword || !self.newPassword || !self.confirmNewPassword) {
        alert("Todos los campos son requeridos");
        return;
      }

      if (self.newPassword != self.confirmNewPassword) {
        alert("Las claves no coinciden");
        return;
      }


      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/user/password/change",
        params: {
          token: $scope._session.token,
          ID: $scope._session.id,
          PASSWORD: self.newPassword,
          CURRENT: self.currentPassword
        }
      }).success(function (data) {

          if (data.result == "OK") {
            $('#changePassword').foundation('reveal', 'close');
          } else {
            alert(data.msg);
          }


          self.newPassword = null;
          self.confirmNewPassword = null;
          self.currentPassword = null;

        }
      );


    }
  };


  $scope._propSection = {
    prop: null,
    photoToDelete: null,
    coverPhoto: null,
    bkp: null,
    waitFor: null,
    details: null,
    photos: [],
    cityFilter: null,
    neighborhoodFilter: null,
    statusFilter: $scope._statusList[2],
    typeFilter: null,
    addressFilter: null,
    codeFilter: null,
    internalFilter: null,
    editAddress: false,
    priceFrom: null,
    priceTo: null,
    areaFrom: null,
    areaTo: null,
    roomsFilter: null,
    closetsFilter: null,
    wcFilter: null,

    condoFilter: null,
    items: [],
    errors: [],
    msg: "",
    page: 1,
    totalPages: 1,
    size: 10,
    readOnly: true,
    cityNeighborhoods: [],
    neighborhoods: [],
    neighborhoodsMap: {},
    cityModel: {},
    neighModel: {},
    employeeModel: {},
    retireComment: null,


    parkingFilter: $scope.boolOptions[0],
    poolFilter: $scope.boolOptions[0],
    elevatorFilter: $scope.boolOptions[0],
    hotFilter: $scope.boolOptions[0],
    unitFilter: $scope.boolOptions[0],


    features: [],

    newFeature: null,

    codeTypes: [],

    codesMap: {
      0: [
        {
          value: "AA", label: "Arr. Apartamentos"
        },
        {
          value: "AC", label: "Arr. Casas"
        },
        {
          value: "AL", label: "Arr. Locales"
        }
      ],
      1: [
        {
          value: "VA", label: "Ventas Apartamentos"
        },
        {
          value: "VC", label: "Ventas Casas"
        },
        {
          value: "VL", label: "Ventas Locales"
        },
        {
          value: "VF", label: "Ventas Fincas"
        },
        {
          value: "VT", label: "Ventas Terrenos"
        }
      ]
    },


    updateCover: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/cover/update",
        params: {
          key: self.coverPhoto.s.split("key=")[1],
          id: self.prop.ID
        }
      }).success(function (data) {
          self.coverPhoto = null;

          if (data.result == "OK") {
            alert("Foto de portda actualizada.")
          } else {
            alert(data.msg);
          }

        }
      );

    },

    deletePhoto: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/photo/delete",
        params: {
          token: $scope._session.token,
          photo: self.photoToDelete
        }
      }).success(function (data) {
          self.photoToDelete.s = "images/noDisponible.jpg";
          self.photoToDelete = null;

          if (data.result == "OK") {
            console.log("DELETED? R/" + data.didDelete);
            $scope._propSection.loadPhotos();
          } else {
            alert(data.msg);
          }

        }
      );

    },

    retireProperty: function () {
      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/retire",
        params: {
          token: $scope._session.token,
          ID: self.prop.ID,
          CODE: self.prop.CODE,
          SERVICE_TYPE: self.prop.SERVICE_TYPE,
          RETIRE_MOTIVE: self.retireComment
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.backToList();
          } else {
            alert(data.msg);
          }

        }
      );

    },

    setAvailable: function () {
      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/status/available",
        params: {
          token: $scope._session.token,
          ID: self.prop.ID
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.backToList();
          } else {
            alert(data.msg);
          }

        }
      );


    },

    setReleased: function () {
      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/release",
        params: {
          token: $scope._session.token,
          ID: self.prop.ID
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.backToList();
          } else {
            alert(data.msg);
          }

        }
      );


    },


    setRequested: function () {
      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/status/requested",
        params: {
          token: $scope._session.token,
          ID: self.prop.ID
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.backToList();
          } else {
            alert(data.msg);
          }

        }
      );


    },

    loadCodeTypes: function () {
      this.codeTypes = this.codesMap[this.prop.SERVICE_TYPE];
      this.prop.CODE = this.codeTypes[0];

      console.log(JSON.stringify(this.prop.CODE));
    },


    clear: function () {
      this.statusFilter = $scope._statusList[2];

      if ($scope._cities.length > 0) {
        this.cityFilter = $scope._cities[0];
        this.loadCityNeighborhoods();
      }

      if ($scope._propertyTypes.length > 0) {
        this.typeFilter = $scope._propertyTypes[0];
      }
      this.codeFilter = null;
      this.addressFilter = null;
      this.internalFilter = null;
      this.priceFrom = null;
      this.priceTo = null;
      this.areaFrom = null;
      this.areaTo = null;
      this.roomsFilter = null;
      this.closetsFilter = null;
      this.wcFilter = null;
      this.condoFilter = null;
      this.neighborhoodFilter = null;
      this.parkingFilter = $scope.boolOptions[0];
      this.poolFilter = $scope.boolOptions[0];
      this.elevatorFilter = $scope.boolOptions[0];
      this.hotFilter = $scope.boolOptions[0];
      this.unitFilter = $scope.boolOptions[0];

    },

    init: function () {

      // it's a return;
      if (this.prop != null) {
        $scope._session.screen = "propiedades-detalle";
        return;
      }

      if ($scope._cities.length > 0) {
        this.cityFilter = $scope._cities[0];
        this.loadCityNeighborhoods();
      }

      if ($scope._propertyTypes.length > 0) {
        this.typeFilter = $scope._propertyTypes[0];
      }

      this.loadPage();

      $scope._session.screen = "propiedades";

    },


    ownerFound: function (owner) {

      //THIS IS A CALBACK, CANT USE 'THIS'!!

      console.log(JSON.stringify(owner));
      console.log("WAITING:" + $scope._propSection.waitFor);
      //check if the callback was ok
      if ($scope._propSection.waitFor != "owner") {
        return;
      }

      $scope._propSection.prop.ID_OWNER = owner.ID;
      $scope._propSection.prop.OWNER_NAME = owner.NAME;
      $scope._session.screen = "propiedades-detalle";

      $scope.flash.clear();

    },

    findOwner: function () {
      this.waitFor = "owner";
      $scope.flash.copyMode = true;
      $scope.flash.callback = this.ownerFound;
      // init the owners screen in search mode
      $scope.loadOwnerList();
    },

    showAddressEditor: function () {
      this.editAddress = true;
    },

    hideAddressEditor: function () {
      this.editAddress = false;
    },

    edit: function () {

      this.editAddress = false;
      this.readOnly = false;
      this.msg = null;
      this.prop.TYPE_MODEL = $scope._propertyTypes[this.prop.ID_TYPE];

      this.bkp = angular.copy(this.prop);
    },

    cityUpdated: function () {
      this.prop.CITY = this.cityModel.label;
      this.prop.CITY_ID = this.cityModel.value;

      this.loadNeighborhoods();
    },

    neighborhoodUpdated: function () {
      this.prop.NEIGHBORHOOD = this.neighModel.label;
      this.prop.ID_NEIGHBORHOOD = this.neighModel.value;
    },


    loadCityNeighborhoods: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/address/neighborhood/options",
        params: {
          token: $scope._session.token,
          city: self.cityFilter.value
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.cityNeighborhoods = data.options;
            self.cityNeighborhoods.unshift({value: null, label: "Todos"});
            //self.neighborhoodFilter = self.cityNeighborhoods[0];
          } else {
            alert(data.msg);
          }

        }
      );

    },


    loadNeighborhoods: function () {

      var self = this;
      self.neighborhoods = [];

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/address/neighborhood/options",
        params: {
          token: $scope._session.token,
          city: self.cityModel.value
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.neighborhoods = data.options;


            var found = false;
            angular.forEach(self.neighborhoods, function (value, key) {

              if (self.prop.ID_NEIGHBORHOOD == value.value) {
                found = true;
                self.neighModel = value;
              }

            });

            if (!found) {
              self.neighModel = self.neighborhoods[0];
            }

            self.neighborhoodUpdated();

          } else {
            alert(data.msg);
          }

        }
      );

    },

    save: function () {

      var self = this;

      console.log(self.prop);

      self.prop.ID_TYPE = self.prop.TYPE_MODEL.value;

      if (self.prop.ID) {
        $http.post("/property/update",
          JSON.stringify({
            property: self.prop
          })
        ).success(function (data) {

            if (data.result == "OK") {
              self.saveFeatures();
            } else {
              alert(data.msg);
            }

          }
        );
      } else {

        console.log("PROP:" + JSON.stringify(self.prop));

        $http.post("/property/add",
          JSON.stringify({
            property: self.prop
          })
        ).success(function (data) {

            if (data.result == "OK") {
              self.prop.ID = data.propertyId;
              self.prop.CODE = data.code;
            } else {
              alert(data.msg);
            }

          }
        );
      }


    },

    saveFeatures: function () {
      var self = this;

      $http.post("/property/update/features",
        JSON.stringify({
          features: self.details
        })
      ).success(function (data) {

          if (data.result == "OK") {
            alert("Property updated.");
            self.cancel(false);
          } else {
            alert(data.msg);
          }

        }
      );
    },

    cancel: function (restore) {
      this.readOnly = true;
      this.msg = null;

      if (restore) {
        angular.copy(this.bkp, this.prop);
      }

      console.log(this.prop);


      if (this.prop.ID) {
        this.readOnly = true;
        this.errors = [];
        this.msg = null;
      } else {
        this.backToList();
      }
    },

    nextPage: function () {

      if (this.page == this.totalPages) {
        return;
      }

      this.page++;
      this.loadPage();
    },
    firstPage: function () {

      if (this.page == 1) {
        return;
      }

      this.page = 1;
      this.loadPage();
    },

    lastPage: function () {

      if (this.page == this.totalPages) {
        return;
      }

      this.page = this.totalPages;
      this.loadPage();
    },

    prevPage: function () {

      if (this.page == 1) {
        return;
      }

      this.page--;
      this.loadPage();
    },

    search: function () {
      this.page = 1;
      this.loadPage();
    },

    add: function () {
      this.readOnly = false;

      this.prop = {};
      this.prop.SERVICE_TYPE = 0;
      this.loadCodeTypes();

      this.prop.TYPE_MODEL = $scope._propertyTypes[1];
      this.prop.STATUS_MODEL = $scope._statusList[3];
      this.employeeModel = $scope.employees.list[2];
      this.employeeUpdated();

      $scope._session.screen = "propiedades-detalle";
      this.showAddressEditor();
      this.cityModel = $scope._cities[0];
      this.loadNeighborhoods();
    },


    loadPage: function () {


      var self = this;

      console.log(JSON.stringify(self.statusFilter) + "-->" + self.statusFilter.value);

      var tmpTarget = (($scope._session.role == 5) ? 1 : 0);

      tmpTarget = (($scope._session.role == 1) ? null : tmpTarget);


      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/list",
        params: {
          status: self.statusFilter ? self.statusFilter.value : null,
          type: (self.typeFilter && self.typeFilter.value != "") ? self.typeFilter.value : null,
          city: self.cityFilter ? self.cityFilter.value : null,
          code: self.codeFilter,
          neighborhood: self.neighborhoodFilter,
          address: self.addressFilter,
          internal: self.internalFilter,
          page: self.page,
          priceFrom: self.priceFrom,
          priceTo: self.priceTo,
          areaFrom: self.areaFrom,
          areaTo: self.areaTo,
          rooms: self.roomsFilter,
          wc: self.wcFilter,
          closets: self.closetsFilter,
          parking: self.parkingFilter.value,
          pool: self.poolFilter.value,
          elevator: self.elevatorFilter.value,
          hot: self.hotFilter.value,
          unit: self.unitFilter.value,
          condo: self.condoFilter,
          size: 10,
          target: tmpTarget
        }
      }).success(function (data) {

        if (data.result == "OK") {
          self.items = data.properties;
          self.totalPages = data.total;
        }

      });
    },

    backToList: function () {
      this.photos = [];
      this.readOnly = true;
      this.errors = [];
      this.prop = null;
      this.details = null;
      this.cityModel = null;
      this.neighModel = null;
      this.employeeModel = null;
      this.editAddress = false;
      $scope._addressBuilder.clear();
      this.msg = null;
      $scope._session.screen = 'propiedades';
      this.loadPage();
    },

    employeeUpdated: function () {
      this.prop.ID_EMPLOYEE = this.employeeModel.ID;
    },

    loadPropertyFeatures: function () {
      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/features",
        params: {
          token: $scope._session.token,
          id: self.prop.ID
        }
      }).success(function (data) {

        if (data.result == "OK") {
          self.details = data.details;
          self.features = [];

          angular.forEach($scope.features, function (feat, key) {

            var add = true;

            angular.forEach(data.details, function (det, key) {

              if (feat.ID == det.type_id) {
                add = false;
              }


            });

            // solo pongamos disponibles las que ya no están agregadas
            if (add) {
              self.features.push(feat);
            }

          });

          if (self.features.length) {
            self.newFeature = self.features[0];
          }

        }

      });
    },

    propDetails: function (prop) {

      var self = this;

      self.prop = prop;
      self.employeeModel = $scope.employees.map[self.prop.ID_EMPLOYEE];

      console.log(JSON.stringify(self.employeeModel));


      $scope._session.screen = "propiedades-detalle";

      self.loadPropertyFeatures();

      self.loadPhotos();


      self.setupCityAndNeighborhoods();

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/agenda/findAllByProperty",
        params: {
          contact: self.prop.ID
        }
      }).success(function (data) {

        if (data.result == "OK") {


          self.prop.appointments = [];


          angular.forEach(data.appointments, function (app, key) {
            app.SCHEDULED_LABEL = app.SCHEDULED.split("-").join("/");
            app.CREATED = new Date(app.CREATED);
            app.TIME_FROM = app.TIME_FROM.substring(0, 5);


            app.TIME_TO = app.TIME_TO.substring(0, 5);
            self.prop.appointments.push(app);

          });


        } else {
          alert(data.msg);
        }

      });


      $scope.featureValues.load();
    },

    removeFeature: function (feature) {
      var self = this;

      $http.post("/property/features/remove",
        JSON.stringify({
          feature: {
            feature: feature.id
          }
        })
      ).success(function (data) {

          if (data.result == "OK") {
            self.loadPropertyFeatures();
          }

        });
    },


    loadPhotos: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/photos",
        params: {
          property: self.prop.ID
        }
      }).success(function (data) {

        if (data.result == "OK") {

          self.photos = [];

          var cont = 0;

          angular.forEach(data.keys, function (photo, key) {

            if (photo.endsWith("_l")) {

              var pKey = photo.substring(0, photo.lastIndexOf('_'));
              self.photos.push({
                l: "/property/photo?key=" + pKey + "_l",
                s: "/property/photo?type=th&key=" + pKey + "_s",
                label: "Foto-" + cont
              });

              cont++;

            }

          });


          if (self.prop.COVER) {
            angular.forEach(self.photos, function (photo, key) {

              if (photo.s == ("/property/photo?type=th&key=" + self.prop.COVER)) {
                self.coverPhoto = photo;
              }

            });

          }


        } // if

      });
    },

    addFeature: function () {

      var self = this;

      $http.post("/property/features/add",
        JSON.stringify({
          feature: {
            property: self.prop.ID,
            feature: self.newFeature.ID,
            type: self.newFeature.TYPE
          }
        })
      ).success(function (data) {

          if (data.result == "OK") {
            self.loadPropertyFeatures();
          }

        });
    },

    replaceAddress: function () {
      this.prop.ADDRESS = $scope._addressBuilder.build();
      this.hideAddressEditor();
    },

    setupCityAndNeighborhoods: function () {
      var self = this;

      // setup the city combo
      angular.forEach($scope._cities, function (value, key) {

        if (self.prop.CITY_ID == value.value) {
          self.cityModel = value;
        }

      });

      self.loadNeighborhoods();
    }

  };


  $scope._ownerSection = {

    // MODEL
    items: [],
    ownerProperties: [],
    errors: [],
    msg: "",
    page: 1,
    totalPages: 1,
    size: 10,
    documentFilter: null,
    nameFilter: null,
    owner: null,
    readOnly: true,

    loadOwnerProperties: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/property/list",
        params: {
          owner: self.owner.ID,
          page: 1,
          size: 100
        }
      }).success(function (data) {

        if (data.result == "OK") {
          self.ownerProperties = data.properties;
        }

      });

    },

    // BEHAVIOR
    reset: function () {
      this.page = 1;
      this.items = [];
      this.documentFilter = null;
      this.nameFilter = null;
      this.readOnly = true;
      this.msg = null;
    },

    nextPage: function () {

      if (this.page == this.totalPages) {
        return;
      }

      this.page++;
      this.loadPage();
    },

    prevPage: function () {

      if (this.page == 1) {
        return;
      }

      this.page--;
      this.loadPage();
    },

    backToList: function () {
      this.readOnly = true;
      this.errors = [];
      this.owner = null;
      this.msg = null;
      $scope._session.screen = 'propietarios';
      this.loadPage();
    },

    ownerDetails: function (tmp) {
      this.owner = tmp;
      this.loadOwnerProperties();
      $scope._session.screen = 'propietarios-detalle';
    },

    search: function () {
      this.page = 1;
      this.items = [];

      this.loadPage();
    },

    cancel: function () {
      if (this.owner.ID) {
        this.readOnly = true;
        this.errors = [];
        this.msg = null;
      } else {
        this.backToList();
      }

    },


    validate: function () {

      this.errors = [];

      if (!this.owner.NAME) {
        this.errors.push("El 'Nombre' es requerido");
      }

      if (!this.owner.DOCUMENT) {
        this.errors.push("El 'Documento' es requerido");
      }

    },

    edit: function () {
      this.readOnly = false;
      this.msg = null;
    },

    add: function () {
      this.readOnly = false;
      this.owner = {};
      $scope._session.screen = 'propietarios-detalle';
    },

    save: function () {

      var self = this;

      this.validate();

      // if there are errors, dont save
      if (this.errors.length) {
        return;
      }

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/owner/update",
        params: {
          ID: this.owner.ID,
          NAME: this.owner.NAME,
          DOCUMENT: this.owner.DOCUMENT,
          ADDRESS: this.owner.ADDRESS ? this.owner.ADDRESS : "",
          PHONES: this.owner.PHONES,
          CELULAR: this.owner.CELULAR,
          MAIL: this.owner.MAIL,
          BIRTHDAY: this.owner.BIRTHDAY,
          COMMENT: this.owner.COMMENT,
          token: $scope._session.token

        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.msg = "Propietario Guardado.";
            self.readOnly = true;
          } else {
            alert(data.msg);
          }

        }
      );

    },

    loadPage: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/owner/list",
        params: {
          page: this.page,
          size: this.size,
          token: $scope._session.token,
          documentFilter: this.documentFilter,
          nameFilter: this.nameFilter
        }
      }).success(function (data) {

          if (data.result == "OK") {
            self.items = data.owners;
            self.totalPages = data.totalPages;
          } else {
            alert(data.msg);
          }

        }
      );

    }

  };


  $scope._requestComments = [];

  $scope._requestFilters = {
    type: "TODOS",
    status: $scope._contactSection.statusList[1],
    user: 0,
    contactName: null,
    userName: null
  };


  $scope._requestUpdateForm = {
    comment: null
  };


  $scope._loginForm = {
    // MODEL
    login: null,
    password: null,
    errors: [],

    // METHODS
    validate: function (cb) {

      this.errors = [];

      if (!this.login) {
        this.errors.push("Correo electrónico inválido/requerido.");
      }

      if (!this.password) {
        this.errors.push("Clave inválida/requerida");
      }

      cb(this.errors.length == 0);
      return this;
    },
    clear: function (cb) {
      this.errors = [];
      if (cb)cb();
      return this;
    }
  };

  $scope.showRequest = function (request) {

  };

  $scope.loadRequestUpdates = function () {


    $http({
      method: "GET",
      url: "http://www.arrendamientosayura.com/contact-request/updates",
      params: {
        request: $scope._contactSection.request.id
      }
    }).success(function (data) {

        if (data.result == "OK") {
          $scope._requestComments = data.updates;
        } else {
          alert(data.msg);
        }

      }
    );
  };


  $scope.updateRequest = function (state) {

    $http({
      method: "GET",
      url: "http://www.arrendamientosayura.com/contact-request/update",
      params: {
        request: $scope._contactSection.request.id,
        status: state,
        token: $scope._session.token,
        comment: $scope._requestUpdateForm.comment ? $scope._requestUpdateForm.comment : ""
      }
    }).success(function (data) {

        if (data.result == "OK") {
          $scope.loadRequestUpdates();
          $scope._contactSection.request.status = state;
          $scope._requestUpdateForm.comment = null;
        } else {
          alert(data.msg);
        }

      }
    );
  };


  $scope.leaveRequest = function (request) {
    $scope._contactSection.request = null;
    $scope._requestUpdateForm.comment = null;
    $scope._requestComments = [];
    $scope.loadRequestList();
  };


  $scope.loadRequestList = function () {
    $scope._session.screen = 'contactenos';
    $scope.loadContactRequests();
  };


//owner: $scope._requestFilters.user.ID,

  $scope.loadContactRequests = function () {
    $http({
      method: "GET",
      url: "http://www.arrendamientosayura.com/contact-request/list",
      params: {
        token: $scope._session.token,
        status: $scope._requestFilters.status.value,
        type: $scope._requestFilters.type.value,
        contact: $scope._requestFilters.contactName,
        ownerName: $scope._requestFilters.userName,
        size: 10,
        page: $scope._requestManager.page
      }


    }).success(function (data) {

        if (data.result == "OK") {

          angular.forEach(data.requestList, function (value, key) {
            value.status = $scope._contactSection.statusList[$scope._contactSection.statusMap[value.status]];
            value.created = new Date(value.created);
            value.updated = new Date(value.updated);
          });

          $scope._requestList = data.requestList;
          $scope._requestManager.totalPages = data.total;
        } else {
          alert(data.msg);
        }

      }
    );
  };

  $scope.formatDate = function (day) {
    return ($scope.zeroPad(day.getMonth() + 1, 2) + "/" + $scope.zeroPad(day.getDate(), 2) + "/" + $scope.zeroPad(day.getFullYear()));
  };

  $scope.dateAsText = function (d) {
    return d.getFullYear() + "-" + $scope.zeroPad(d.getMonth() + 1, 2) + "-" + $scope.zeroPad(d.getDate(), 2);
  };

  $scope.getTime = function (date) {
    return $scope.zeroPad(date.getHours(), 2) + ":" + $scope.zeroPad(date.getMinutes(), 2) + ":00"
  };

  $scope.parseDate = function (input) {
    var parts = [];
    parts = input.split('/');
    return new Date(parts[2], parts[0] - 1, parts[1]); // months are 0-based
  };

  $scope.zeroPad = function (num, places) {
    var zero = places - num.toString().length + 1;
    return new Array(+(zero > 0 && zero)).join("0") + num;
  };

  $scope.loadOwnerList = function () {
    $scope._session.screen = 'propietarios';
    $scope._ownerSection.loadPage();
  };


  $scope.doLogin = function () {


    $scope._loginForm.validate(function (valid) {

      if (valid) {

        $http({
          method: "GET",
          url: "http://www.arrendamientosayura.com/user/login",
          params: {
            login: $scope._loginForm.login,
            password: $scope._loginForm.password
          }
        }).success(function (data) {

            if (data.result == "OK") {
              $scope._session.token = data.token;
              $scope._session.role = data.role;
              $scope._session.id = data.id;
              $scope.init();
            } else {
              $scope._loginForm.errors.push(data.msg);
            }

          }
        );

      }

    });


  };


  $scope._agenda = {

    date: null,
    overviewDate: null,
    grid: [],
    agents: [],
    dates: {},
    agent: null,
    currentCell: null,
    currentContact: null,
    dayAppointments: [],
    overviewAppointments: [],


    appointment: {
      toOptions: [],
      customers: [],
      properties: [],
      fromTime: null,
      keysOptions: ["AGENCIA", "OTRO"],
      endTime: null,
      endTimeVal:null,
      comments: null,
      customerId: null,
      customerError: null,
      propertyCode: null,
      property: null,
      propertyError: null,
      place: "AGENCIA",

      places: [
        "AGENCIA"
      ],

      codeTypes: [
        "CITA CONTACTO",
        "BÚSQUEDA PROPIEDADES",
        "NO DISPONIBLE",
        "DILIGENCIAS ADMINISTRATIVAS"
      ],


      endTimeText: function () {

        if (this.endTime == null) {
          return "";
        }

        var suffix = " am";
        var end = this.endTime.getHours();
        if (end > 12) {
          end = end - 12;
          suffix = " pm";
        }

        return  $scope.zeroPad(end, 2)
          + ":" + $scope.zeroPad(this.endTime.getMinutes(), 2) + suffix
      },

      addCustomer: function () {
        var self = this;
        self.customers.push(self.customer);
        self.customer = null;
        self.customerId = null;
        self.customError = null;

        if ($scope._agenda.currentContact && $scope._agenda.currentContact == self.customerId) {
          $scope._agenda.currentContact = null;
        }

      },

      searchCustomer: function () {

        var self = this;

        self.customerError = "";
        self.customer = null;

        if (!self.customerId) {
          this.customerError = "Por favor ingrese el código de la solicitud.";
          return;
        }


        for (var i = 0; i < self.customers.length; i++) {

          if (self.customers[i].ID == self.customerId) {
            self.customerError = "El contacto de la solicitud " + self.customerId + " ya fue citado";
            return;
          }
        }


        $http({
          method: "GET",
          url: "http://www.arrendamientosayura.com/contact-request/get",
          params: {
            id: self.customerId
          }
        }).success(function (data) {

          if (data.result == "OK") {

            if (data.contact) {
              self.customer = data.contact;
            } else {
              self.customer = null;
              self.customerError = "Código de solicitud inválido, intente de nuevo.";
            }

          }

        });
      },

      removeCustomer: function (idx) {
        this.customers.splice(idx, 1);
      },

      clearSearchCustomer: function () {
        var self = this;
        self.customer = null;
        self.customerId = null;
        self.customError = null;

        if ($scope._agenda.currentContact) {
          self.customerId = $scope._agenda.currentContact.id;
          self.searchCustomer();
        }
      },

      addProperty: function () {
        var self = this;
        self.places.push(self.property.CODE);
        self.properties.push(self.property);
        self.property = null;
        self.propertyCode = null;
        self.keys = "AGENCIA";
        self.propertyError = null;

      },


      removeProperty: function (idx) {
        var self = this;
        self.properties.splice(idx, 1);
        self.places.splice(idx + 1, 1);
        self.place = "AGENCIA";
      },


      clearSearchProperty: function () {
        var self = this;
        self.propertyError = null;
        self.property = null;
        self.keys = "AGENCIA";
        self.propertyCode = null;
      },

      searchProperty: function () {
        var self = this;

        self.propertyError = null;
        self.property = null;

        if (!self.propertyCode) {
          self.propertyError = "Por favor ingrese el código a buscar.";
          return;
        }


        self.propertyCode = self.propertyCode.toUpperCase();


        for (var i = 0; i < self.properties.length; i++) {

          console.log(self.properties[i].CODE + "==" + self.propertyCode);
          if (self.properties[i].CODE == self.propertyCode) {
            self.propertyError = "El código " + self.propertyCode + " ya fue asignado";
            return;
          }
        }


        $http({
          method: "GET",
          url: "http://www.arrendamientosayura.com/property/list",
          params: {
            internal: isNaN(self.propertyCode) ? self.propertyCode : null,
            code: isNaN(self.propertyCode) ? null : self.propertyCode,
            page: 1,
            size: 1
          }
        }).success(function (data) {

          if (data.result == "OK") {

            if (data.properties.length) {
              self.property = data.properties[0];
              self.property.keys = "AGENCIA";
            } else {
              self.property = null;
              self.propertyError = "Código inválido, intente de nuevo.";
            }

          }

        });
      },

      cancelAppointment: function () {
        var self = this;

        $http({
          method: "GET",
          url: "http://www.arrendamientosayura.com/agenda/cancel",
          params: {
            appointment: $scope._agenda.currentCell.appointment.ID
          }
        }).success(function (data) {

          if (data.result == "OK") {
            $scope._agenda.rebuildGrid();
          } else {
            alert("ERROR:" + data.msg);
          }

        });


      },

      confirmAppointment: function () {
        var self = this;

        $http({
          method: "GET",
          url: "http://www.arrendamientosayura.com/agenda/confirm",
          params: {
            appointment: $scope._agenda.currentCell.appointment.ID
          }
        }).success(function (data) {

          if (data.result == "OK") {
            $scope._agenda.rebuildGrid();
          } else {
            alert("ERROR:" + data.msg);
          }

        });


      },

      saveAppointment: function () {

        var self = this;

        var contacts = [];

        // join contacts if any
        for (var i = 0; i < self.customers.length; i++) {
          contacts.push(self.customers[i].ID);
        }

        var properties = [];

        // join properties if any
        for (var i = 0; i < self.properties.length; i++) {
          properties.push(self.properties[i].ID + "-" + self.properties[i].keys);
        }


        $http({
          method: "GET",
          url: "http://www.arrendamientosayura.com/agenda/add",
          params: {
            id: $scope._agenda.currentCell.appointment ? $scope._agenda.currentCell.appointment.ID : null,
            promoter: $scope._agenda.agent.ID,
            consultant: $scope._session.id,
            scheduled: $scope.dateAsText(self.fromTime),
            time_from: $scope.getTime(self.fromTime),
            time_to: $scope.getTime(self.endTime),
            comments: self.comments,
            place: self.place,
            contacts: contacts.join(","),
            properties: properties.join(",")
          }
        }).success(function (data) {

          if (data.result == "OK") {

            self.places =
              $scope._agenda.rebuildGrid();

          } else {
            alert("ERROR:" + data.msg);
          }

        });


      }

    },


    overview: function () {
      var self = this;

      if (self.overviewDate == null) {
        self.overviewDate = $scope.formatDate(new Date());
      }


      $scope._session.screen = "agenda-overview";

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/agenda/overview",
        params: {
          scheduled: $scope.dateAsText($scope.parseDate(self.overviewDate))
        }
      }).success(function (data) {

        if (data.result == "OK") {


          angular.forEach(data.appointments, function (appointment, idx) {
            var sch = $scope.parseDate(appointment.SCHEDULED);

            var tmp = appointment.TIME_FROM.split(":");
            var fromTime = new Date(sch.getTime());
            fromTime.setHours(parseInt(tmp[0]));
            fromTime.setMinutes(parseInt(tmp[1]));

            var suffix = "am";
            if (parseInt(tmp[0]) > 12) {
              tmp[0] = parseInt(tmp[0]) - 12;
              suffix = "pm";
            }
            appointment.from = $scope.zeroPad(tmp[0], 2) + ":" + $scope.zeroPad(tmp[1], 2) + " " + suffix;

            tmp = appointment.TIME_TO.split(":");
            var endTime = new Date(sch.getTime());
            endTime.setHours(parseInt(tmp[0]));
            endTime.setMinutes(parseInt(tmp[1]));

            suffix = "am";

            if (parseInt(tmp[0]) > 12) {
              tmp[0] = parseInt(tmp[0]) - 12;
              suffix = "pm";
            }

            appointment.to = $scope.zeroPad(tmp[0], 2) + ":" + $scope.zeroPad(tmp[1], 2) + " " + suffix;
          });

          self.overviewAppointments = data.appointments;
        }
      });
    },

    initClear: function () {
      this.currentContact = null;
      this.init();
    },

    init: function () {
      var self = this;
      $scope._session.screen = "agenda-master";
      var day = new Date();
      self.date = ($scope.zeroPad(day.getMonth() + 1, 2) + "/" + $scope.zeroPad(day.getDate(), 2) + "/" + $scope.zeroPad(day.getFullYear()));
      self.loadAgents();
    },

    cellClicked: function (cell) {
      var self = this;

      //alert($scope.getDayName(cell.from)+"["+cell.from.toLocaleString()+"]" + "->" + cell.from.getHours() + ":" + cell.from.getMinutes() + "-" + cell.to.getHours() + ":" + cell.to.getMinutes());
      self.currentCell = cell;


      // build timeTo options from starting time and default to now + 1 delta
      var endDate = new Date(cell.to.getTime());
      var fromDate = new Date(cell.to.getTime());
      var currentEnd = new Date(cell.to.getTime());

      if(cell.appointment){
        currentEnd.setHours(cell.appointment.TIME_TO.split(":")[0])
        currentEnd.setMinutes(cell.appointment.TIME_TO.split(":")[1])

        self.appointment.fromTime = new Date(cell.from);
        self.appointment.fromTime.setHours(cell.appointment.TIME_FROM.split(":")[0]);
        self.appointment.fromTime.setMinutes(cell.appointment.TIME_FROM.split(":")[1]);
      }else{
        self.appointment.fromTime = new Date(cell.from);
      }


      self.appointment.toOptions = [];
      self.appointment.customers = [];
      self.appointment.properties = [];
      self.appointment.comments = self.currentCell.appointment ? self.currentCell.appointment.COMMENTS : null;
      self.appointment.place = self.currentCell.appointment ? self.currentCell.appointment.place : "AGENCIA";


      endDate.setHours(18);
      endDate.setMinutes(0);

      self.appointment.places = ["AGENCIA"];

      self.appointment.endTime=null;


      while (fromDate.getHours() < endDate.getHours() || ((fromDate.getHours() == endDate.getHours()) && fromDate.getMinutes() <= endDate.getMinutes())) {

        self.appointment.toOptions.push(fromDate);

        if(fromDate.getHours()==currentEnd.getHours() && fromDate.getMinutes()==currentEnd.getMinutes()){
          console.log(fromDate +" is equal"+currentEnd);
          self.appointment.endTime = fromDate;
        }



        // fi there is an app starting on the endtime then break the cycle;
        if ($scope._agenda.dates[$scope.dateAsText(fromDate)] && $scope._agenda.dates[$scope.dateAsText(fromDate)][$scope.timeText(fromDate)]) {
          var tmpCell = $scope._agenda.dates[$scope.dateAsText(fromDate)][$scope.timeText(fromDate)];

          console.log(tmpCell ? (tmpCell.appointment ? (self.currentCell.appointment ? (tmpCell.appointment.ID != self.currentCell.appointment.ID ? "DIFFERENT" : "SAME") : "NO APP CURRENT") : "NO APP") : "NO CELL");

          if (tmpCell && tmpCell.appointment && (!self.currentCell.appointment || (self.currentCell.appointment && tmpCell.appointment.ID != self.currentCell.appointment.ID))) {
            break;
          }

        }



        fromDate = $scope.addMinutes(fromDate, 30);




      }


      if(self.appointment.endTime==null){
        self.appointment.endTime = self.appointment.toOptions[0];
      }


      if (self.currentCell.appointment) {
        self.appointment.customers = self.currentCell.appointment.contacts;
        self.appointment.properties = self.currentCell.appointment.properties;
        self.appointment
        self.appointment.places = ["AGENCIA"];
        angular.forEach(self.appointment.properties, function (prop, idx) {
          self.appointment.places.push(prop.CODE);
        });
        self.appointment.place = self.currentCell.appointment.PLACE

      } // if


    },


    rebuildGrid: function () {

      var self = this;

      var columns = 1;
      console.log($document.width());

      self.setupGrid(columns, 30);
    },


    setupGrid: function (extraDays, delta) {

      var self = this;

      if (self.date == null) {
        return;
      }

      self.grid.splice(0, self.grid.length);

      var fromDate = $scope.parseDate(self.date);

      var maxDelta = 10 * (60 / delta);

      var day = new Date(+fromDate);


      for (var k = 0; k < extraDays; k++) {

        day.setDate(fromDate.getDate() + k);
        day.setHours(8, 0, 0, 0);

        console.log(k + ":Day:" + $scope.getDayName(day));


        var column = {
          cells: [],
          dayName: $scope.getDayName(day),
          date: new Date(+day),
          dateText: function () {
            return $scope.dateAsText(this.date)
          }
        };

        self.grid.push(column);

        self.dates[column.dateText()] = {};

        for (var i = 0; i < maxDelta; i++) {

          var from = new Date(day.getTime() + (i * 30) * 60000);
          var to = new Date(day.getTime() + ((i + 1) * 30) * 60000);

          var cell = {
            from: from,
            to: to,
            status: 0,
            patient: null,
            fromText: function () {
              return $scope.zeroPad(this.from.getHours(), 2)
                + ":" + $scope.zeroPad(this.from.getMinutes(), 2);
            },
            toText: function () {
              return  $scope.zeroPad(this.to.getHours(), 2)
                + ":" + $scope.zeroPad(this.to.getMinutes(), 2)
            },
            dateText: function () {
              return  $scope.zeroPad(this.from.getFullYear(), 4)
                + "-" + $scope.zeroPad(this.from.getMonth() + 1, 2)
                + "-" + $scope.zeroPad(this.from.getDate(), 2)
            }
          };

          self.grid[k].cells.push(cell);

          if (cell.to.getTime() < new Date().getTime()) {
            cell.editable = false;
          } else {
            cell.editable = true;
          }

          self.dates[column.dateText()][cell.fromText()] = cell;

        }

        self.loadDateAgenda(column.dateText());


      }


    },


    loadDateAgenda: function (date) {


      var self = this;


      if (self.agent == null) {
        return;
      }


      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/agenda/list",
        params: {
          promoter: $scope._agenda.agent.ID,
          scheduled: date
        }
      }).success(function (data) {

        if (data.result == "OK") {


          angular.forEach(data.appointments, function (appointment, key) {

            var sch = $scope.parseDate(appointment.SCHEDULED);

            var tmp = appointment.TIME_FROM.split(":");
            var fromTime = new Date(sch.getTime());
            fromTime.setHours(parseInt(tmp[0]));
            fromTime.setMinutes(parseInt(tmp[1]));

            var suffix = "am";
            if (parseInt(tmp[0]) > 12) {
              tmp[0] = parseInt(tmp[0]) - 12;
              suffix = "pm";
            }
            appointment.from = tmp[0] + ":" + tmp[1] + " " + suffix;

            tmp = appointment.TIME_TO.split(":");
            var endTime = new Date(sch.getTime());
            endTime.setHours(parseInt(tmp[0]));
            endTime.setMinutes(parseInt(tmp[1]));

            suffix = "am";

            if (parseInt(tmp[0]) > 12) {
              tmp[0] = parseInt(tmp[0]) - 12;
              suffix = "pm";
            }

            appointment.to = tmp[0] + ":" + tmp[1] + " " + suffix;


            var appStatus = null;

            switch (appointment.STATUS) {
              case "SCHEDULED":
                appStatus = 1;
                break;
              case "CANCELLED":
                appStatus = 4;
                break;
              case "CONFIRMED":
                appStatus = 2;
                break;
              default:
                appStatus = 0;
            }


            while (fromTime.getHours() < endTime.getHours() || ((fromTime.getHours() == endTime.getHours()) && fromTime.getMinutes() < endTime.getMinutes())) {

              var from = $scope.zeroPad(fromTime.getHours(), 2) + ":" + $scope.zeroPad(fromTime.getMinutes(), 2);
              var cell = self.dates[appointment.SCHEDULED][from];

              if (!cell) {
                console.log("CELL NOT FOUND:" + appointment.SCHEDULED + "@" + from);
              }

              cell.status = appStatus;
              cell.appointment = appointment;
              fromTime = $scope.addMinutes(fromTime, 30);

            }

          });


          console.log(date + "==" + self.date + "=?" + date == $scope.dateAsText($scope.parseDate(self.date)));
          if (date == $scope.dateAsText($scope.parseDate(self.date))) {
            console.log("replace");
            self.dayAppointments = data.appointments;
          }


        } else {
          alert("Problemas al cargar citas del agente: " + data.msg);
        }

      });


    },

    loadAgents: function () {

      var self = this;

      $http({
        method: "GET",
        url: "http://www.arrendamientosayura.com/agent/list",
        params: {
        }
      }).success(function (data) {

        if (data.result == "OK") {
          self.agents = data.agents;

          if (self.agents.length > 0) {
            self.agent = self.agents[0];
            self.agentChanged();
          }

        } else {
          $scope._loginForm.errors.push(data.msg);
        }

      });

    },


    agentChanged: function () {
      console.log(this.agent.NAME);
      this.rebuildGrid();
    },


    nextDay: function () {
      var day = $scope.parseDate(this.date);
      day.setDate(day.getDate() + 1);
      this.date = ($scope.zeroPad(day.getMonth() + 1, 2) + "/" + $scope.zeroPad(day.getDate(), 2) + "/" + $scope.zeroPad(day.getFullYear()));
    },

    prevDay: function () {
      var day = $scope.parseDate(this.date);
      day.setDate(day.getDate() - 1);
      this.date = ($scope.zeroPad(day.getMonth() + 1, 2) + "/" + $scope.zeroPad(day.getDate(), 2) + "/" + $scope.zeroPad(day.getFullYear()));

    },

    nextWeek: function () {
      var day = $scope.parseDate(this.date);
      day.setDate(day.getDate() + 7);
      this.date = ($scope.zeroPad(day.getMonth() + 1, 2) + "/" + $scope.zeroPad(day.getDate(), 2) + "/" + $scope.zeroPad(day.getFullYear()));
    },

    prevWeek: function () {
      var day = $scope.parseDate(this.date);
      day.setDate(day.getDate() - 7);
      this.date = ($scope.zeroPad(day.getMonth() + 1, 2) + "/" + $scope.zeroPad(day.getDate(), 2) + "/" + $scope.zeroPad(day.getFullYear()));

    },

    nextMonth: function () {
      var day = $scope.parseDate(this.date);
      day.setMonth(day.getMonth() + 1);
      this.date = ($scope.zeroPad(day.getMonth() + 1, 2) + "/" + $scope.zeroPad(day.getDate(), 2) + "/" + $scope.zeroPad(day.getFullYear()));
    },

    prevMonth: function () {
      var day = $scope.parseDate(this.date);
      day.setMonth(day.getMonth() - 1);
      this.date = ($scope.zeroPad(day.getMonth() + 1, 2) + "/" + $scope.zeroPad(day.getDate(), 2) + "/" + $scope.zeroPad(day.getFullYear()));

    },

    nextYear: function () {
      var day = $scope.parseDate(this.date);
      day.setFullYear(day.getFullYear() + 1);
      this.date = ($scope.zeroPad(day.getMonth() + 1, 2) + "/" + $scope.zeroPad(day.getDate(), 2) + "/" + $scope.zeroPad(day.getFullYear()));
    },

    prevYear: function () {
      var day = $scope.parseDate(this.date);
      day.setFullYear(day.getFullYear() - 1);
      this.date = ($scope.zeroPad(day.getMonth() + 1, 2) + "/" + $scope.zeroPad(day.getDate(), 2) + "/" + $scope.zeroPad(day.getFullYear()));

    }
  };



  $scope.timeText = function (date) {
    return $scope.zeroPad(date.getHours(), 2)
      + ":" + $scope.zeroPad(date.getMinutes(), 2);
  }


  $scope.addMinutes = function (date, minutes) {
    return new Date(date.getTime() + minutes * 60000);
  };

  $scope.zeroPad = function (num, places) {
    var zero = places - num.toString().length + 1;
    return new Array(+(zero > 0 && zero)).join("0") + num;
  };

  $scope.parseDate = function (input) {
    var parts = [];
    parts = input.split('/');
    return new Date(parts[2], parts[0] - 1, parts[1]); // months are 0-based
  };

  $scope.dayNames = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'];

  $scope.monthNames = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio",
    "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ];

  $scope.getDayName = function (date) {
    return $scope.dayNames[ date.getDay()];
  };

  $scope.getMonthName = function (date) {
    return $scope.monthNames[ date.getMonth()];
  };

  $scope.parseDate = function (input) {

    var parts = [];

    if (input.indexOf('-') > -1) {
      parts = input.split('-');
      return new Date(parts[0], parts[1] - 1, parts[2]); // months are 0-based
    } else {
      parts = input.split('/');
      return new Date(parts[2], parts[0] - 1, parts[1]); // months are 0-based
    }

  };

  $scope.addDays =function(date, days){
    var d = new Date();
    d.setDate(date.getDate() + days);
    return d;
  };

  $scope.hourBasicFromText = function (milHour) {

    var tmp = milHour.split(":");
    var end=parseInt(tmp[0]);
    var suffix = " am";

    if(end>=12){
      suffix = " pm";
    }

    if (end > 12) {
      end = end - 12;

    }



    return  $scope.zeroPad(end, 2)
      + ":" + $scope.zeroPad(parseInt(tmp[1]), 2) + suffix
  };


  $scope.hourText = function (date) {

    if (date == null) {
      return "";
    }

    var suffix = " am";

    var end = date.getHours();

    if (end > 12) {
      end = end - 12;
      suffix = " pm";
    }

    return  $scope.zeroPad(end, 2)
      + ":" + $scope.zeroPad(date.getMinutes(), 2) + suffix
  };

  $scope._reportManager = {
    propStatusFrom: $scope.dateAsText($scope.addDays(new Date(), -30)),
    propStatusTo: $scope.dateAsText(new Date()),
    propStatus: $scope._statusList[0]

  };

  $scope.init = function () {


    $http({
      method: "GET",
      url: "http://www.arrendamientosayura.com/user/list",
      params: {
      }
    }).success(function (data) {

        if (data.result == "OK") {

          $scope._userList = data.users;

          angular.forEach($scope._userList, function (value, key) {
            $scope._userMap[value.ID] = value;
          });

          $scope._requestFilters.user = $scope._userMap[$scope._session.id];


          $scope._contactSection.init();
          $scope.loadCities();
          $scope.employees.load();
          $scope.loadPropertyTypes();
          $scope._addressBuilder.load();
          //$scope.loadRequestList();

          $scope.loadFeatures();
          $scope._dashboard.loadCharts();


        } else {
          alert(data.msg);
        }

      }
    );

  };


  $scope.$watch('_agenda.date', function () {
    $scope._agenda.rebuildGrid();
  }); // initialize the watch


});