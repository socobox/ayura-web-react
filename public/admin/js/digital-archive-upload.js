var DigitalArchiveUpload = angular.module('digitalArchiveUpload', []);

DigitalArchiveUpload.factory('dgArchiveServices',function(RequestServices){
  var dgArchiveServices = {};
  var folderId = "";
  var urlGetDocuments = "content/document/types";

  dgArchiveServices.getFolderId = function(){
    return folderId;
  }

  dgArchiveServices.setFolderId = function( id ){
    folderId = id;
  }

  dgArchiveServices.getUrlDocuments = function(){
    return urlGetDocuments;
  }

  return dgArchiveServices;
})

DigitalArchiveUpload.controller('digitalArchiveUploadCtrl', function($scope, $routeParams, $location, AdminServices, RequestServices, dgArchiveServices){
  var mainPath    = AdminServices.getPath(),
  urlGetFolders   = mainPath + "content/folder/types";
  $scope._session = null;
  $scope.contentCategories = {};

  var loadFolders = function(){
    var params = {
      token: $scope._session.token
    }
    RequestServices.getRequest( urlGetFolders, params)
      .success(function (data){
        if (data.result == "OK") {
          console.log(data)
          $scope.contentCategories = data.options;
          if ($routeParams.fContent) {
            for (var i = 0; i < $scope.contentCategories.length; i++) {
              if ($scope.contentCategories[i].value == $routeParams.fContent) {
                $scope.directive = $scope.contentCategories[i];
              };
            };
          }else{
            $scope.directive = $scope.contentCategories[0];
          };
          $scope.setFolderId();
        } else {
          alert(data.msg);
        }
      })
  };

  $scope.setFolderId = function(){
    // if($routeParams.fContent  && $routeParams.fContent != $scope.directive.value){
    //   $location.url($location.path()+"?fContent="+$scope.directive.value);
    // }
    dgArchiveServices.setFolderId($scope.directive.value);
  }
  function init(){
    $scope._session = AdminServices.getSession();
    AdminServices.validateSession();
    loadFolders();
  }
  init()
});

DigitalArchiveUpload.directive('dgContracts', function() {
  return {
    restrict: 'AE',
    transclude: true,
    scope: {},
    controller: contractsCtrl,
    templateUrl: 'partials/digitalArchive/contracts.html'
  };
})
DigitalArchiveUpload.directive('dgCorrespondences', function() {
  return {
    restrict: 'AE',
    transclude: true,
    scope: {},
    controller: correspondencesCtrl,
    templateUrl: 'partials/digitalArchive/correspondences.html'
  };
})
DigitalArchiveUpload.directive('dgRequest', function() {
  return {
    restrict: 'AE',
    transclude: true,
    scope: {},
    controller: requestCtrl,
    templateUrl: 'partials/digitalArchive/request.html'
  };
})

var contractsCtrl = function ($scope, $timeout, AdminServices, RequestServices, dgArchiveServices, $filter, $routeParams) {
  var mainPath      = AdminServices.getPath(),
  urlGetDocuments   = mainPath + dgArchiveServices.getUrlDocuments(),
  urlUpload         = mainPath + "content/upload",
  urlContractSearh  = mainPath + "tenant/contracts/search";
  var _format = 'dd/MM/yy';

  $scope.searchBlock = false;
  $scope.submit = false;
  $scope.page = 1;
  $scope.totalPages = 1;

  $scope._session = null;
  // Filters
  $scope.tenantFilter     = null;
  $scope.tenantNroFilter  = null;
  $scope.contractFilter   = null;
  $scope.propFilter       = null;
  $scope.ownerFilter      = null;
  $scope.ownerNroFilter   = null;

  $scope.documentType = []
  $scope.contracts = [];
  var folderId = null;
  $scope.errors = [];
  $scope.fileControl = null;
  $scope.files = [];
  $scope.filesSend = [];

  function itemModel( id, contract, propiedad, tenant, tenantId, owner, ownerId, fromC, toC) {
    this.id        = id        || "",
    this.contract  = contract  || "",
    this.propiedad = propiedad || "",
    this.tenant    = tenant    || "",
    this.tenantId  = tenantId  || "",
    this.owner     = owner     || "",
    this.ownerId   = ownerId   || "",
    this.from      = fromC     || "",
    this.to        = toC       || ""
  };

  itemModel.prototype.selected = function(){
    $scope.contractSelected = {
      label: "#"+this.contract + " - " + $filter('date')(this.from, _format) + " - " + $filter('date')(this.to, _format),
      value: this.id
    }
    $('#contractSearch').foundation('reveal', 'close');
  }

  var validate = function () {
    $scope.errors = [];
    if (!$scope.contractSelected) {
        $scope.errors.push("El 'Contrato' es requerido");
    }
    if (!$scope.files || $scope.files.length==0) {
        $scope.errors.push("'Documento' es requerido");
    }
  }
  
  $scope.next = function(){
    $scope.page += 1;
    $scope.search();
  };
  $scope.prev = function(){
    $scope.page -= 1;
    $scope.search();
  };
  $scope.searchFilter = function(){
    $scope.page = 1;
    $scope.search();
  };
  $scope.search = function(){
    params = {
      page: $scope.page,
      size: 10,
      token: $scope._session.token,
      nameFilter: $scope.tenantFilter,
      documentFilter: $scope.tenantNroFilter,
      contractFilter: $scope.contractFilter,
      propertyFilter: $scope.propFilter,
      ownerNameFilter: $scope.ownerFilter,
      ownerDocFilter: $scope.ownerNroFilter
    }
    RequestServices.getRequest(urlContractSearh, params)
      .success(function (data) {
        console.log(data);
        if (data.result == "OK") {
          $scope.totalPages = data.totalPages;
          $scope.contracts = [];
          angular.forEach(data.contracts, function (value, key) {
            var item = new itemModel( value.CONTRACT_ID, value.CONTRACT_CODE, value.PROPERTY_ID, value.TENANT_NAME, value.TENANT_DOCUMENT, value.OWNER_NAME, value.OWNER_DOCUMENT, value.CONTRACT_START_DATE, value.CONTRACT_END_DATE );
            $scope.contracts.push(item);
          });
          if ($routeParams.fContract && $scope.contracts.length ==1) {
            $scope.contracts[0].selected();
          };
        } else {
          alert(data.msg);
        }

      });
  };

  var preLoadContract = function(){
    $scope.contractFilter = $routeParams.fContract;
    $scope.searchFilter();
  }

  var loadDocuments = function(){
    var params = {
      folder: folderId,
      token: $scope._session.token
    }
    RequestServices.getRequest( urlGetDocuments, params)
        .success(function (data) {
          if (data.result == "OK") {
            $scope.documentType = data.options;
            $scope.docType  = $scope.documentType[0];
          } else {
            alert(data.msg);
          }
        });
  };

  function init(){
    $(document).foundation();
    $scope._session = AdminServices.getSession();
    if($routeParams.fContract){
      preLoadContract();
    }
    folderId = dgArchiveServices.getFolderId();
    loadDocuments();
    $scope.$on("fileSelected", function (event, args) {
      $scope.$apply(function () {
        //add the file object to the scope's files collection
        $scope.files.push(args.file);
        $scope.fileControl = args.control;
      });
    }); 
  }
  init(); 
}

var correspondencesCtrl = function ($scope, AdminServices, RequestServices, dgArchiveServices) {
  var mainPath      = AdminServices.getPath(),
  urlGetDocuments   = mainPath + dgArchiveServices.getUrlDocuments(),
  urlUpload         = mainPath + "",
  urlContractSearh  = mainPath + "tenant/contracts/search";

  $scope.searchBlock = false;
  $scope.page = 1;

  $scope._session = null;
  // Filters
  $scope.tenantFilter     = null;
  $scope.tenantNroFilter  = null;
  $scope.contractFilter    = null;
  $scope.propFilter       = null;
  $scope.ownerFilter      = null;
  $scope.ownerNroFilter   = null;

  $scope.documentType = []
  $scope.contracts = [];

  function itemModel( id, contract, propiedad, tenant, tenantId, owner, ownerId) {
    this.id        = id        || "",
    this.contract  = contract  || "",
    this.propiedad = propiedad || "",
    this.tenant    = tenant    || "",
    this.tenantId  = tenantId  || "",
    this.owner     = owner     || "",
    this.ownerId   = ownerId   || ""
  };

  $scope.save = function () {
    for (var ic = 0; ic < $scope.files.length; ic++) {
      $scope.send($scope.files[ic]);
    }
    $scope.fileControl.form.reset();
    $scope.files = [];
  };

  $scope.send = function(file){
    AdminServices.send(file, {}, urlUpload)
      .success(function(){
        alert("tenant success");
      });
  };

  var loadDocuments = function(){
    var params = {
      folder: dgArchiveServices.getFolderId(),
      token: $scope._session.token
    }
    RequestServices.getRequest( urlGetDocuments, params)
        .success(function (data) {
          if (data.result == "OK") {
            $scope.documentType = data.options;
            $scope.docType  = $scope.documentType[0];
          } else {
            alert(data.msg);
          }
        });
  };

  function init(){
    $scope._session = AdminServices.getSession();
    loadDocuments();
    $scope.$on("fileSelected", function (event, args) {
      $scope.$apply(function () {
        //add the file object to the scope's files collection
        $scope.files.push(args.file);
        $scope.fileControl = args.control;
      });
  }); 
  }
  init(); 
}

var requestCtrl = function ($scope, AdminServices, RequestServices, dgArchiveServices) {
  var mainPath     = AdminServices.getPath(),
  urlGetDocuments  = mainPath + dgArchiveServices.getUrlDocuments(),
  urlUpload        = mainPath + "content/upload",
  urlPropRequest   = mainPath + "prop-request/list";
  var _format = 'dd/MM/yy';

  $scope.searchBlock = false;
  $scope.submit = false;
  $scope.page = 1;
  $scope.totalPages = 1;

  $scope._session = null;
  // Filters
  $scope.tenantFilter     = null;
  $scope.tenantNroFilter  = null;
  $scope.contractFilter   = null;
  $scope.propFilter       = null;
  $scope.ownerFilter      = null;
  $scope.ownerNroFilter   = null;

  $scope.documentType = []
  $scope.contacts = [];
  var folderId = null;
  $scope.errors = [];
  $scope.fileControl = null;
  $scope.files = [];
  $scope.filesSend = [];

  $scope._ContactFilters = {
    name: null,
    docId: null,
    requestId: null,
    contactId: null
  }

  function contactModel( id, name) {
    this.id    = id    || "",
    this.name  = name  || ""
  };

  contactModel.prototype.selected = function(){
    $scope.requestSelected = {
      label: "#"+this.id,
      value: this.id
    }
    $('#contactSearch').foundation('reveal', 'close');
  }

  var validate = function () {
    $scope.errors = [];
    if (!$scope.requestSelected) {
        $scope.errors.push("La 'Solicitud' es requerida");
    }
    if (!$scope.files || $scope.files.length==0) {
        $scope.errors.push("'Documento' es requerido");
    }
  }
  
  $scope.next = function(){
    $scope.page += 1;
    $scope.searchContact();
  };
  $scope.prev = function(){
    $scope.page -= 1;
    $scope.searchContact();
  };
  $scope.seacrhConctactFilter = function(){
    $scope.page = 1;
    $scope.searchContact();
  };
  $scope.searchContact = function(){
    var params = {
      token: $scope._session.token,
      page: $scope.page,
      size: 5,
      nameFilter: $scope._ContactFilters.name,   // (Filtro de nombre de la persona del contacto)
      requestFilter: $scope._ContactFilters.requestId,// (filtro por ID del contacto)
      contactFilter: $scope._ContactFilters.contactId,
      docFilter: $scope._ContactFilters.docId
    };
    RequestServices.getRequest( urlPropRequest, params)
      .success(function (data){
        $scope.contacts = [];
        console.log(data)
        $scope.requestIdSelected = null;
        if (data.result == "OK") {
          $scope.totalPages = data.totalPages;
          angular.forEach( data.requests, function ( request, key){
            var req = new contactModel(request.id, request.contact_name);
            $scope.contacts.push(req);
          })
          if ($scope.totalPages == 1 && $scope.contacts.length == 1) {
            $scope.contacts[0].selected();
          };
        };
      })
  };

  $scope.showErrors = function(){
    alert("El Error se pudo presentar por los sigts erroes:\n\t-El tamaño del Archivo es mayor a 10Mb\n\t-El Archivo se encuentra dañado\n\t-Problemas con el servidor\nIntenta cargar de nuevo el Archivo o ingrese uno nuevo.");
  }

  $scope.save = function () {
    validate();
    if ($scope.errors.length) {
      return;
    }
    $($scope.fileControl).val('');
    $scope.submit = true;
    var length = $scope.files.length;
    for (var ic = 0; ic < length; ic++) {
      var index = $scope.filesSend.length;
      if($scope.files[0].size < 10000000){
        $scope.send($scope.files[0], index);
      }else{
        $scope.files[0].status = true;
        $scope.files[0].error = true;
      }
      $scope.filesSend.push($scope.files[0]);
      $scope.files.splice(0,1);
    }
  };

  $scope.send = function(file, index){
    var model = {
      DOC_TYPE_ID: $scope.docType.value,
      ENTITY_ID: $scope.requestSelected.value,
      FOLDER_ID: folderId,
      token: $scope._session.token
    }
    console.log(model);
    AdminServices.send(file, model, urlUpload, true)
      .success(function (data){
        $scope.filesSend[index].status=true;
        if(data.result = "OK"){
          $scope.filesSend[index].upload = true;
        }else{
          $scope.filesSend[index].error = true;
        }
      })
      .error(function (data){
        $scope.filesSend[index].status=true;
        $scope.filesSend[index].error = true;
      })
  };

  var preLoadContract = function(){
    $scope.contractFilter = $routeParams.fContract;
    $scope.searchFilter();
  }

  var loadDocuments = function(){
    var params = {
      folder: folderId,
      token: $scope._session.token
    }
    RequestServices.getRequest( urlGetDocuments, params)
        .success(function (data) {
          if (data.result == "OK") {
            $scope.documentType = data.options;
            $scope.docType  = $scope.documentType[0];
          } else {
            alert(data.msg);
          }
        });
  };

  function init(){
    $(document).foundation();
    $scope._session = AdminServices.getSession();
    folderId = dgArchiveServices.getFolderId();
    loadDocuments();
    $scope.$on("fileSelected", function (event, args) {
      $scope.$apply(function () {
        //add the file object to the scope's files collection
        $scope.files.push(args.file);

        $scope.fileControl = args.control;
      });
  }); 
  }
  init(); 
}