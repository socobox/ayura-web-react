var consultApp = angular.module('adminAyuraTenant', ["ngCookies", "services"]);


consultApp.controller("tenantCtrl", function ($scope, $cookies, RequestServices, AdminServices, DateServices) {
    var mainPath = AdminServices.getPath();
    var urlLogin = mainPath + "user/login";
    var urlGetItems = mainPath + "content/search";
    var urlUpload = mainPath + "content/upload";
    var urlPropRequest = mainPath + "prop-request/list";
    var urlContractSearh = mainPath + "tenant/contracts/search";
    var urlAddComment = mainPath + "prop-request/comments/add";
    var urlListComment = mainPath + "prop-request/list/comments";

    var imageValidate = AdminServices.getImgValidate();
    var typeItems = {
        0: "folder",
        1: "file",
        2: "folder-o",
        3: "files-o",
        4: "folder-open",
        5: "folder-open-o",
        6: "file-text",
        7: "archive",
        8: "file-o",
        9: "file-text-o",
    };
    var typeItemsMap = {
        "Folder": 0,
        "Document": 9,
    };
    var folderId = 2;

    $scope.tabActive = true;
    $scope.files = [];
    $scope.filesSend = [];
    $scope.nav = false;
    $scope.requestIdSelected = null;
    $scope._session = null;
    $scope.docType = null;
    $scope.sw = false;
    $scope.view = 1;

    $scope._loginForm = {
        // MODEL
        login: null,
        password: null,
        errors: [],
        // METHODS
        validate: function (cb) {
            this.errors = [];

            if (!this.login) {
                this.errors.push("Correo electrónico inválido/requerido.");
            }

            if (!this.password) {
                this.errors.push("Clave inválida/requerida");
            }

            // cb(this.errors.length == 0);
            cb(true);
            return this;
        },
        clear: function (cb) {
            this.errors = [];
            if (cb) cb();
            return this;
        },
        doLogin: function () {
            var self = this;
            this.validate(function (valid) {
                if (valid) {
                    params = {
                        login: self.login,
                        password: self.password,
                        type: 'TENANT'
                    };
                    RequestServices.getRequest(urlLogin, params)
                        .success(function (data) {
                            if (data.result == "OK") {
                                AdminServices.setSessionToken(data.token);
                                AdminServices.setSessionRole(data.role);
                                AdminServices.setSessionId(data.id);
                                $scope._session = AdminServices.getSession();
                                AdminServices.validateSession(function () {
                                    $cookies.ownerDoc = self.login;
                                    $scope.searchContracts();
                                    $scope.view = 2;
                                });
                            } else {
                                $scope._loginForm.errors.push(data.msg);
                            }
                        });
                }
            });
        },
        logOut: function () {
            $scope.view = 1;
            $scope.collection = [];
            $scope.docType = null;
            $scope.contractIdSelected = null;
            $scope.nav = false;
            this.password = null;
        }
    };
    // MODELS
    function itemModel(name, type, metadata) {
        this.name = name || '';
        this.type = type || '';
        this.metadataInfo = metadata || [];
    }

    itemModel.prototype.action = function (evt) {
        if (this.metadataInfo.ICON_TYPE == "Folder") {
            if (this.metadataInfo.DOC_TYPE_ID) {
                $scope.docType_name = this.name;
                $scope.docType = this.metadataInfo.DOC_TYPE_ID;
            }
            $scope.mainPage = 1;
            if (!$scope.nav) {
                $scope.nav = true;
            }
            getItems();
        }
    };

    function contractModel(id, contract, propiedad, tenant, tenantId, owner, ownerId, fromC, toC) {
        this.id = id || '';
        this.contract = contract || '';
        this.propiedad = propiedad || '';
        this.tenant = tenant || '';
        this.tenantId = tenantId || '';
        this.owner = owner || '';
        this.ownerId = ownerId || '';
        this.from = fromC || '';
        this.to = toC || '';
    }

    contractModel.prototype.selected = function () {
        if ($scope.contractIdSelected != this.id) {
            $scope.contractIdSelected = this.id;
            $scope.levelUp();
        }
    };

    // Gets
    var getItems = function () {
        $scope.collection = [];
        $scope.sw = true;
        var params = {
            page: $scope.mainPage,
            size: 100,
            token: $scope._session.token,
            FOLDER_ID: folderId,
            DOC_TYPE_ID: $scope.docType,
            ENTITY_ID: $scope.contractIdSelected
        };
        RequestServices.getRequest(urlGetItems, params)
            .success(function (data) {
                if (data.result == "OK") {
                    $scope.sw = false;
                    $scope.mainTotalPages = data.totalPages;
                    $scope.collection = [];
                    angular.forEach(data.items, function (value, key) {
                        if (value.ICON_TYPE == "Document") {
                            if (imageValidate.indexOf(value.CONTENT_TYPE) != -1) {
                                value.isImage = true;
                            }
                        }
                        var item = new itemModel(value.FOLDER_NAME, typeItems[typeItemsMap[value.ICON_TYPE]], value);

                        if (validateFolder(item.metadataInfo.DOC_TYPE_ID) && value.ICON_TYPE == "Folder") {
                            $scope.collection.push(item);
                            $scope.mainTotalPages = 1;
                        }

                        if (value.ICON_TYPE == "Document") {
                            console.count(item);
                            $scope.collection.push(item);
                        }
                    });
                    $(document).foundation();
                } else {
                    $scope.collection = [];
                    $scope.mainTotalPages = 1;
                }
            });
    };

    function validateFolder(docId) {
        return (
            docId == 125 || // RECIBOS DE CAJA
            docId == 41 || // Cartera facturas
            docId == 19 || // Inventarios Iniciales
            docId == 42 || // CARTERA - INCREMENTOS
            docId == 20 || // Inventarios Finales
            docId == 30 || // Cartera referencia,
            docId == 23 // CUENTAS DE SERVICIOS
        );
    }

    $scope.levelUp = function () {
        $scope.nav = false;
        $scope.mainPage = 1;
        $scope.docType = null;
        $scope.docType_name = null;
        getItems();
    };

    $scope.showErrors = function () {
        alert("El Error se pudo presentar por los sigts erroes:\n\t-El tamaño del Archivo es mayor a 10Mb\n\t-El Archivo se encuentra dañado\n\t-Problemas con el servidor\nIntenta cargar de nuevo el Archivo o ingrese uno nuevo.");
    };

    var validate = function () {
        $scope.errors = [];
        if (!$scope.requestIdSelected) {
            $scope.errors.push("El 'Contrato' es requerido");
        }
        if (!$scope.files || $scope.files.length == 0) {
            $scope.errors.push("'Documento' es requerido");
        }
    };

    $scope.save = function () {
        validate();
        if ($scope.errors.length) {
            return;
        }
        $($scope.fileControl).val('');
        $scope.submit = true;
        var length = $scope.files.length;
        for (var ic = 0; ic < length; ic++) {
            var index = $scope.filesSend.length;
            if ($scope.files[0].size < 10000000) {
                $scope.send($scope.files[0], index);
            } else {
                $scope.files[0].status = true;
                $scope.files[0].error = true;
            }
            $scope.filesSend.push($scope.files[0]);
            $scope.files.splice(0, 1);
        }
    };

    $scope.send = function (file, index) {
        var model = {
            DOC_TYPE_ID: $scope.docType,
            ENTITY_ID: $scope.requestIdSelected,
            FOLDER_ID: folderId,
            token: $scope._session.token
        };
        AdminServices.send(file, model, urlUpload, true)
            .success(function (data) {
                $scope.filesSend[index].status = true;
                if (data.result == "OK") {
                    $scope.filesSend[index].upload = true;
                    getItems();
                } else {
                    $scope.filesSend[index].error = true;
                }
            })
            .error(function (data) {
                $scope.filesSend[index].status = true;
                $scope.filesSend[index].error = true;
            });
    };

    $scope.prevMain = function () {
        $scope.mainPage--;
        getItems();
    };

    $scope.nextMain = function () {
        $scope.mainPage++;
        getItems();
    };

    $scope._requestComments = {
        listComments: [],
        comment: null,
        getComments: function () {
            var self = this;
            var params = {
                request: $scope.requestIdSelected
            };
            RequestServices.getRequest(urlListComment, params)
                .success(function (data) {
                    if (data.result == "OK") {
                        self.listComments = data.updates;
                    }
                });
        },
        addComment: function () {
            var self = this;
            var params = {
                token: $scope._session.token, // (obligatorio porque de ahí se agrega el usuario que hizo el comment)
                comment: self.comment, //  (el comentario)
                request: $scope.requestIdSelected //  (la solicitud asociada al comentario(
            };
            RequestServices.getRequest(urlAddComment, params)
                .success(function (data) {
                    if (data.result == "OK") {
                        self.comment = null;
                        self.getComments();
                    } else {
                        alert(data.msg);
                    }
                });
        }
    };

    $scope.searchContracts = function () {
        params = {
            page: $scope.page,
            size: 10,
            token: $scope._session.token,
            documentFilter: $scope._loginForm.login
        };

        RequestServices.getRequest(urlContractSearh, params)
            .success(function (data) {
                if (data.result == "OK") {
                    $scope.totalPages = data.totalPages;
                    $scope.contracts = [];
                    angular.forEach(data.contracts, function (value, key) {
                        var item = new contractModel(value.CONTRACT_ID, value.CONTRACT_CODE, value.PROPERTY_ID, value.TENANT_NAME, value.TENANT_DOCUMENT, value.OWNER_NAME, value.OWNER_DOCUMENT, value.CONTRACT_START_DATE, value.CONTRACT_END_DATE);
                        $scope.contracts.push(item);
                    });

                    if ($scope.totalPages == 1 && $scope.contracts.length == 1) {
                        $scope.contracts[0].selected();
                    }
                } else {
                    alert(data.msg);
                }

            });
    };



    function init() {
        if ($cookies.ownerDoc) {
            $scope._loginForm.login = $cookies.ownerDoc;
        }
        $scope.$on("fileSelected", function (event, args) {
            $scope.$apply(function () {
                //add the file object to the scope's files collection
                $scope.files.push(args.file);
                $scope.fileControl = args.control;
            });
        });
    }

    init();
});