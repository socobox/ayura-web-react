Controllers.controller('contractCtrl', function( $scope, $sce, $location, $routeParams, AdminServices, RequestServices, EmployeeServices, DateServices){
	$scope._session  = null;
	var mainPath     = AdminServices.getPath(),
	urlUpload        = mainPath + "content/upload",
	urlGetItems      = mainPath + "content/search",
	urlGetFolders    = mainPath + "content/folder/types",
	urlGetDocuments  = mainPath + "content/document/types",
	urlContractSearh = mainPath + "tenant/contracts/search",
	urlPropRequest   = mainPath + "prop-request/list";

	$scope._session = null;

	$scope.employees = {};

	$scope.searchFilter = function(){
		$scope.page = 1;
		search();
	};

	$scope.search = search;

	$scope.prev = function(){
		$scope.page--;
		search();
	}

	$scope.next = function(){
		$scope.page++;
		search();
	}

	function search(){
		params = {
			page: $scope.page,
			size: 10,
			token: $scope._session.token,
			nameFilter: $scope.tenantFilter,
			documentFilter: $scope.tenantNroFilter,
			contractFilter: $scope.contractFilter,
			propertyFilter: $scope.propFilter,
			ownerNameFilter: $scope.ownerFilter,
			ownerDocFilter: $scope.ownerNroFilter
		}
		RequestServices.getRequest(urlContractSearh, params)
			.success(function (data) {
				console.log(data);
				if (data.result == "OK") {
					$scope.totalPages = data.totalPages;
					$scope.contracts = [];
					angular.forEach(data.contracts, function (value, key) {
						var item = new contractModel( value.CONTRACT_ID, value.CONTRACT_CODE, value.PROPERTY_ID, value.TENANT_NAME, value.TENANT_DOCUMENT, value.OWNER_NAME, value.OWNER_DOCUMENT, value.CONTRACT_START_DATE, value.CONTRACT_END_DATE );
						$scope.contracts.push(item);
					});
					if ($scope.totalPages == 1 && $scope.contracts.length == 1) {
						$scope.contracts[0].selected();
					};
				} else {
					alert(data.msg);
				}

			});
	};

	function clearFilter(){
		$scope.tenantFilter = null;
		$scope.tenantNroFilter = null;
		$scope.contractFilter = null;
		$scope.propFilter = null;
		$scope.ownerFilter = null;
		$scope.ownerNroFilter = null;
	}

	function contractModel( id, contract, propiedad, tenant, tenantId, owner, ownerId, fromC, toC) {
		this.id        = id        || "",
		this.contract  = contract  || "",
		this.propiedad = propiedad || "",
		this.tenant    = tenant    || "",
		this.tenantId  = tenantId  || "",
		this.owner     = owner     || "",
		this.ownerId   = ownerId   || "",
		this.from      = fromC     || "",
		this.to        = toC       || ""
	};

	contractModel.prototype.selected = function(){

	}

	function init(){
		AdminServices.validateSession(function(){
			$scope._session = AdminServices.getSession();
			$scope.page = 1;
			AdminServices.setUrlBack( $location.url() );
			AdminServices.setPropertiesViews( [] );
			$(document).foundation();
			clearFilter();
			search();
		});
	};

	init();
});
Controllers.controller('contractDetailCtrl', function( $scope, $sce, $location, $routeParams, AdminServices, RequestServices, EmployeeServices, DateServices){
	var mainPath = AdminServices.getPath(),
	urlContractSearh  = mainPath + "tenant/contracts/search",
	urlContractCode   = mainPath + "tenant/contracts/last",
	urlLinkReqCont    = mainPath + "tenant/contracts/link",
	urlTenantSave     = mainPath + "tenant/update",
	urlGetTenant      = mainPath + "tenant/get",
	urlsaveContract   = mainPath + "tenant/contracts/add",
	urlPropertyData   = mainPath + "property/list",
	urlPropRequest    = mainPath + "prop-request/list",
	urlTenant 		  = mainPath + "tenant/list";

	var tenantPerPage = 10;

	$scope._session = null;

	$scope.employees = {};

	$scope.saveContract = function () {

		console.log("Creando contrato....");

		validateContract(saveContract);
	};

	function createContract() {

		var nextYear = new Date();
		nextYear.setMonth(nextYear.getMonth() + 6);

		$scope._contract = {
			CODE: null,
			tenant: null,
			PROPERTY: null,
			START_DATE: DateServices.formatDate(new Date()),
			END_DATE: DateServices.formatDate(nextYear),
			EMPLOYEE_CONTACT: 55,
			EMPLOYEE: 55
		}
	}

	$scope.initSearchTenant = function(){
		$scope.pageTenant = 1;
		$scope.totalPages = 1;
		$scope.tenantFilter = {};
		$scope.tenants = [];
	}

	$scope.searchTenant = function(){
		$scope.pageTenant = 1;
		searchTenant();
	}

	$scope.nextPageTenant = function(){
		$scope.pageTenant++;
		searchTenant();
	}

	$scope.prevPageTenant = function(){
		$scope.pageTenant--;
		searchTenant();
	}

	function searchTenant(){
		var self = this;
		params = {
			page: $scope.pageTenant,
			size: tenantPerPage,
			token: $scope._session.token,
			documentFilter: $scope.tenantFilter.document || null,
			nameFilter: $scope.tenantFilter.name || null
		}
		RequestServices.getRequest(urlTenant, params)
			.success(function (data) {
			if (data.result == "OK") {
				$scope.tenants = data.tenants;
				$scope.totalPages = data.totalPages;
			} else {
				alert(data.msg);
			}

			});
	}

	$scope.tenantSelect = function(tenant){
		$scope._contract.tenant = tenant;
		console.log($scope.tenant);
		$('#searchTenant').foundation('reveal', 'close');
	}

	function init(){
		AdminServices.validateSession(function(){
			$scope._session = AdminServices.getSession();
			$scope.employees = EmployeeServices;
			AdminServices.setUrlBack( $location.url() );
			AdminServices.setPropertiesViews( [] );
			$(document).foundation();
			$(".date").fdatepicker();
			createContract();
			$scope.validateProperty = validateProperty;
			$scope.validateNroContract = validateNroContract;
		});
	};

	function validateNroContract(){
		if (!$scope.contractBox) {return};

        if (!$scope.contractBox) {
            return;
        }

        params = {
			page: $scope.page,
			size: 10,
			token: $scope._session.token,
			contractFilter: $scope.contractBox
		}

        RequestServices.getRequest( urlContractSearh, params)
          .success(function (data) {
            if (data.result == "OK") {
              if (!data.contracts.length) {
                $scope._contract.CODE = angular.copy($scope.contractBox);
              }else{
              	alertify.error("El numero de contrato existe.");
              }
            }
        });
	}

	function validateContract(cb){
		if (!$scope._contract) {
			return;
		};

		if (!$scope._contract.CODE) {
			alertify.error("Por favor ingresar Nro de Contrato");
			return;
		};

		if (!$scope._contract.tenant) {
			alertify.error("Por favor ingresar Inquilino");
			return;
		};

		if (!$scope._contract.PROPERTY) {
			alertify.error("Por favor ingresar consecutivo de la Propiedad");
			return;
		};
		cb();
	}


	function saveContract(){
		params = {
			token: $scope._session.token,
			CODE: $scope._contract.CODE,
			START_DATE: DateServices.dateAsText(DateServices.parseDate($scope._contract.START_DATE)),
			END_DATE: DateServices.dateAsText(DateServices.parseDate($scope._contract.END_DATE)),
			ID_TENANT: $scope._contract.tenant.ID,
			ID_PROPERTY: $scope._contract.PROPERTY,
			ID_EMPLOYEE: $scope._contract.EMPLOYEE,
			ID_EMPLOYEE_CONTACT: $scope._contract.EMPLOYEE_CONTACT
		}
		RequestServices.getRequest( urlsaveContract, params)
			.success(function (data) {
				if (data.result == "OK") {
					alertify.success("Contrato Creado");
					$scope.contractBox = null;
					$scope.propertytBox = null;
					createContract();
				}

		});
	}

	function validateProperty(){

        if (!$scope.propertytBox) {
            return;
        }

        params = {
          code: $scope.propertytBox,
          page: 1,
          size: 1
        }

        RequestServices.getRequest( urlPropertyData, params)
          .success(function (data) {
            if (data.result == "OK") {
              if (!data.properties.length) {
                alertify.error("Propiedad no existe");
                $scope._contract.PROPERTY_VALIDATE = false;
                $scope._contract.PROPERTY = null;
              }else{
              	$scope._contract.PROPERTY = angular.copy($scope.propertytBox);
              	$scope._contract.PROPERTY_VALIDATE = true;
              }
            }
        });
	}

	init();
});