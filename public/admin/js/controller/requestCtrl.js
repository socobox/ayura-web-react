Controllers.controller('contactRequestCtrl', function ($scope, $filter, $sce, $location, $routeParams, AdminServices, RequestServices, EmployeeServices, DateServices) {

  $scope.firstDate = '';
  $scope.lastDate = '';
  $scope.monthCounter = 0;
  $scope.currentDate = new Date();
  $scope.loading = true;

  $scope.contacts = {};
  $scope.appointments = {};
  $scope.quantity = {};
  $scope.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

  let ctx = document.getElementById('myChart').getContext('2d');

  $scope.data = {
    type: 'bar',
    labels: ["Agencia", "Avisos", "Página Web", "Finca Raíz", "Ciencuadras", "Referido", "Facebook", "Instagram", "Reubicados", "Otro"],
    datasets: [{
      label: 'medio',
      data: [0, 0, 0, 0, 0, 0, 0, 0, 0],
      backgroundColor: "#ff7f7f"
    }],
  };

  $scope.options = {
    scales: {
      xAxes: [{
        barPercentage: 1,
        barThickness: 50,
        maxBarThickness: 20,
        minBarLength: 4,
        gridLines: {
          offsetGridLines: true
        }
      }]
    }
  };

  $scope.myBarChart = new Chart(ctx, {
    type: 'bar',
    data: $scope.data,
    options: $scope.options
  });

  function init() {
    $scope.loading = true;
    getMonthDates(0);
    getMonthAppointments();
  }

  function getMonthDates(addDate){
    $scope.monthCounter += addDate;
    let date = new Date();
    date.setDate(1);
    date.setMonth(date.getMonth() - $scope.monthCounter);

    $scope.currentDate = date;
    let lastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    $scope.firstDate = date.getFullYear() + '-' + dateAddZeros(date.getMonth() + 1) + '-' + dateAddZeros(date.getDate());
    $scope.lastDate = lastDate.getFullYear() + '-' + dateAddZeros(lastDate.getMonth() + 1)  + '-' + dateAddZeros(lastDate.getDate());
  }

  function dateAddZeros(date){
    return ('0' + date).slice(-2);
  }

  $scope.changeDate = function(toAdd) {
    getMonthDates(toAdd);
    getMonthAppointments();
  };

  function getMonthAppointments() {
    $scope.quantity = {'AGENCIA': 0, 'AVISOS': 0, 'PAGINA WEB': 0, 'FINCA RAIZ': 0, 'CIENCUADRAS': 0, 'REFERIDO': 0, 'FACEBOOK': 0, 'INSTAGRAM': 0, 'REUBICADOS': 0, 'OTRO': 0};
    let params = {
      scheduledBefore: $scope.lastDate,
      scheduledAfter: $scope.firstDate
    };
    AdminServices.getContactRequestByDates(params, function (data) {
      data.requestList.forEach(function (contact) {
        var value = contact.medio ? contact.medio.toUpperCase() : '';
        if(value.match(/^(AGENCIA|AVISOS|PAGINA WEB|FINCA RAIZ|CIENCUADRAS|REFERIDO|FACEBOOK|INSTAGRAM|REUBICADOS)$/)){
          $scope.quantity[value] = $scope.quantity[value] +1;
        } else {
          $scope.quantity.OTRO = $scope.quantity.OTRO +1;
        }
      });
      $scope.medios = Object.entries($scope.quantity).map(([name, total]) => ({name,total}));
      $scope.myBarChart.data.datasets[0].data = Object.values($scope.quantity);
      $scope.myBarChart.update();
      $scope.loading = false;
    });
  }

  init();
});
