Controllers.controller('consultantsCtrl', function ($scope, $filter, $sce, $location, $routeParams, AdminServices, RequestServices, EmployeeServices, DateServices) {
  let mainPath = AdminServices.getPath();
  let urlGetAgenda = mainPath + "agenda/list";

  $scope.firstDate = '';
  $scope.lastDate = '';
  $scope.montCounter = 0;
  $scope.currentDate = new Date();
  $scope.loading = true;

  $scope.groupSelection = 'Asesores';
  $scope.consultants = {};
  $scope.promoters = {};
  $scope.appointments = {};

  $scope.months = ["Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre"];

  //Agents is array converted from the dictionary object associated with consultants or with promoters
  $scope.agents = [];
  $scope.agentTitle = '';

  let ctx = document.getElementById('myChart').getContext('2d');

  $scope.data = {
    labels: ["0s", "10s", "20s", "30s", "40s", "50s", "60s"],
    datasets: [{
      label: $scope.groupSelection,
      data: [0, 59, 75, 20, 20, 55, 40],
      backgroundColor: "#ff7f7f"
    }]
  };

  $scope.options = {
    scales: {
      xAxes: [{
        barPercentage: 1,
        barThickness: 50,
        maxBarThickness: 20,
        minBarLength: 4,
        gridLines: {
          offsetGridLines: true
        }
      }]
    }
  };

  $scope.myBarChart = new Chart(ctx, {
    type: 'bar',
    data: $scope.data,
    options: $scope.options
  });

  function init() {
    getMonthDates(0);
    getPromoters();
  }

  function getMonthDates(addDate){
    $scope.montCounter += addDate;
    let date = new Date();
    date.setDate(1);
    date.setMonth(date.getMonth() - $scope.montCounter);

    $scope.currentDate = date;
    let lastDate = new Date(date.getFullYear(), date.getMonth() + 1, 0);

    $scope.firstDate = date.getFullYear() + '-' + dateAddZeros(date.getMonth() + 1) + '-' + dateAddZeros(date.getDate());
    $scope.lastDate = lastDate.getFullYear() + '-' + dateAddZeros(lastDate.getMonth() + 1)  + '-' + dateAddZeros(lastDate.getDate());
  }

  function dateAddZeros(date){
    return ('0' + date).slice(-2);
  }

  function getPromoters() {
    $scope.loading = true;
    AdminServices.loadNewAgents(function (data) {
      $scope.promoters = data;
      getMonthAppointments();
    })
  }

  $scope.changeDate = function(toAdd) {
    getMonthDates(toAdd);
    getPromoters();
  };

  function getMonthAppointments() {
    let params = {
      scheduledBefore: $scope.lastDate,
      scheduledAfter: $scope.firstDate
    };
    AdminServices.getAppointmentsByDates(params, function (data) {
      $scope.consultants = {};
      $scope.promoters = {};
      $scope.appointments = {};

      data.appointments.forEach(function (appointment) {
        if(appointment.properties.length){
          if(!$scope.consultants[appointment.CONSULTANT_ID]){
            $scope.consultants[appointment.CONSULTANT_ID] = {
              name: appointment.CONSULTANT,
              confirmed: 0,
              scheduled: 0,
              cancelled: 0,
              quantity: appointment.STATUS !== 'CANCELLED' ? 1 : 0,
            }
          } else{
            if(appointment.STATUS !== 'CANCELLED'){
              $scope.consultants[appointment.CONSULTANT_ID].quantity++;
            }
          }

          if(!$scope.promoters[appointment.PROMOTER_ID]){
            $scope.promoters[appointment.PROMOTER_ID] = {
              name: appointment.PROMOTER,
              confirmed: 0,
              scheduled: 0,
              cancelled: 0,
              quantity: appointment.STATUS !== 'CANCELLED' ? 1 : 0,
            }
          } else{
            if(appointment.STATUS !== 'CANCELLED'){
              $scope.promoters[appointment.PROMOTER_ID].quantity++;
            }
          }

          switch(appointment.STATUS){
            case "CONFIRMED":
              $scope.promoters[appointment.PROMOTER_ID].confirmed++;
              $scope.consultants[appointment.CONSULTANT_ID].confirmed++;
              break;
            case "SCHEDULED":
              $scope.promoters[appointment.PROMOTER_ID].scheduled++;
              $scope.consultants[appointment.CONSULTANT_ID].scheduled++;
              break;
            case "CANCELLED":
              $scope.promoters[appointment.PROMOTER_ID].cancelled++;
              $scope.consultants[appointment.CONSULTANT_ID].cancelled++;
              break;
          }

          if(!$scope.appointments[appointment.ID]){
            $scope.appointments[appointment.ID] = {
              consultantId: appointment.CONSULTANT_ID,
              promoterId: appointment.PROMOTER_ID,
              status: appointment.STATUS,
              day: appointment.SCHEDULED,
              dateCreated: appointment.CREATED,
              timeStart: appointment.TIME_FROM,
              timeEnd: appointment.TIME_TO
            }
          }
        }
      });
      $scope.createAgentsArray();
    });
  }

  $scope.changeAgent = function () {
    $scope.createAgentsArray();
  };

  //Changes the agent array
  $scope.createAgentsArray = function () {

    switch ($scope.groupSelection) {
      case 'Asesores':
        $scope.agentTitle = 'Asesor';

        agentDictionaryToArray($scope.consultants);
        break;
      case 'Promotores':
        $scope.agentTitle = 'Promotor';
        agentDictionaryToArray($scope.promoters);
        break;
    }
  };

  function agentDictionaryToArray(agentsObject) {
    $scope.agents = [];
    Object.keys(agentsObject).forEach(agent => {
      $scope.agents.push(agentsObject[agent]);
    });

    $scope.agents = $filter('orderBy')($scope.agents, 'name');
    createChart($scope.agents);
  }

  function createData(labels, data, title) {
    return {
      labels: labels,
      datasets: [{
        label: title,
        data: data
      }]
    };
  }

  function createChart(agents) {
    let labels = [];
    let data = [];

    agents.forEach(agent => {
      labels.push(agent.name);
      data.push(agent.quantity - agent.cancelled);
    });

    $scope.data = createData(labels, data, $scope.groupSelection);

    $scope.myBarChart.data.datasets[0].data = data;
    $scope.myBarChart.data.datasets[0].label = $scope.groupSelection;
    $scope.myBarChart.data.labels = labels;

    $scope.myBarChart.update();
    $scope.loading = false;
  }

  init();
});
