var consultApp = angular.module('adminayuraConsult', ["ngCookies","services"]);

consultApp.directive('fileUpload', function () {
  return {
    scope: true,        //create a new scope
    link: function (scope, el, attrs) {
      el.bind('change', function (event) {
        var files = event.target.files;
        //iterate files since 'multiple' may be specified on the element
        for (var i = 0; i < files.length; i++) {
          //emit event upward
          scope.$emit("fileSelected", { file: files[i], control: event.target});

        }
      });
    }
  };
});
consultApp.directive("dropzone", function() {
  return {
    restrict: 'A',
    link: function (scope, elem, attrs) {
      elem.bind("dragover", function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        elem.addClass("hover")
      });
      elem.bind("dragenter", function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
      });
      elem.bind("dragleave", function(evt) {
        elem.removeClass("hover")
      });
      elem.bind("drop", function(evt) {
        evt.stopPropagation();
        evt.preventDefault();
        elem.removeClass("hover");
        evt.dataTransfer = evt.dataTransfer? evt.dataTransfer : evt.originalEvent.dataTransfer;
        var files = evt.dataTransfer.files;
        for (var i = 0, f; f = files[i]; i++) {
          var reader = new FileReader();
            reader.readAsArrayBuffer(f);

            reader.onload = (function(theFile) {
              return function(e) {
                scope.$emit("fileSelected", { file: theFile, control: evt.target});
              };
            })(f);
        }
      });
    }
  }
});

consultApp.controller("consultCtrl", function($scope, $cookies, RequestServices, AdminServices, DateServices){
  var mainPath = AdminServices.getPath(),
  urlLogin         = mainPath + "user/login",
  urlGetItems      = mainPath + "content/search",
  urlUpload        = mainPath + "content/upload",
  urlPropRequest   = mainPath + "prop-request/list",
  urlAddComment    = mainPath + "prop-request/comments/add",
  urlListComment   = mainPath + "prop-request/list/comments";

  var imageValidate = AdminServices.getImgValidate();
  var typeItems = {
    0: "folder",
    1: "file",
    2: "folder-o",
    3: "files-o",
    4: "folder-open",
    5: "folder-open-o",
    6: "file-text",
    7: "archive",
    8: "file-o",
    9: "file-text-o",
  },
  typeItemsMap = {
    "Folder": 0,
    "Document": 9,
  },
  folderId = 1;

  $scope.tabActive = true
  $scope.files = [];
  $scope.filesSend = [];
  $scope.nav = false;
  $scope.requestIdSelected = null;
  $scope._session = null;
  $scope.docType = null;
  $scope.view = 1;

  $scope._loginForm = {
    // MODEL
    login: null,
    password: null,
    errors: [],
    // METHODS
    validate: function (cb) {
      this.errors = [];

      if (!this.login) {
        this.errors.push("Correo electrónico inválido/requerido.");
      }

      if (!this.password) {
        this.errors.push("Clave inválida/requerida");
      }

      cb(this.errors.length == 0);
      return this;
    },
    clear: function (cb) {
      this.errors = [];
      if (cb)cb();
      return this;
    },
    doLogin: function () {
      var self = this;
      this.validate(function (valid) {
        if (valid) {
          params = {
            login: self.login,
            password: self.password,
            type: 'CUSTOMER'
          }
          RequestServices.getRequest(urlLogin, params)
            .success(function (data) {
              if (data.result == "OK") {
                AdminServices.setSessionToken( data.token );
                AdminServices.setSessionRole( data.role );
                AdminServices.setSessionId( data.id );
                $scope._session = AdminServices.getSession();
                AdminServices.validateSession(function(){
                  $cookies.consultasDoc = self.login;
                  $scope.view = 2;
                  $scope._consult.getRequestProp();
                });
              } else {
                $scope._loginForm.errors.push(data.msg);
              }
            });
        }
      });
    },
    logOut: function(){
      $scope.view = 1;
      this.password = null;
    }
  };

  function requestModel (id, createdDate, owner, status, propId){
    this.id = id || "",
    this.createdDate = DateServices.formatDate( new Date(createdDate) ) || "",
    this.status = status || "",
    this.owner = owner || "",
    this.propId = propId || ""
  };

  $scope._consult = {
    getRequestProp: function(){
      var self = this;
      var params = {
        token: $scope._session.token,
        stateFilter: null,  // (ID del estado a filtrar)
        nameFilter: null,   // (Filtro de nombre de la persona del contacto)
        requestFilter: $scope._loginForm.password,// (filtro por ID del contacto)
        contactFilter: null,
        docFilter: $scope._loginForm.login
      };
      RequestServices.getRequest( urlPropRequest, params)
        .success(function (data){
          console.log(data)
          if (data.result == "OK") {
            $scope.request = new requestModel(
              data.requests[0].id, 
              data.requests[0].created, 
              data.requests[0].contact_name, 
              data.requests[0].status, 
              data.requests[0].property_id
              );
            $scope.requestIdSelected = $scope.request.id;
            self.loadInfoRequest();
          };
        })
    },
    loadInfoRequest: function (){
      $scope._requestComments.getComments();
      getItems();
    }
  };

  function itemModel( name, type, metadata) {
    this.name = name || "",
    this.type = type || "",
    this.metadataInfo = metadata || []
  };

  itemModel.prototype.action = function(evt){
    if( this.metadataInfo.ICON_TYPE == "Folder"){
      if(this.metadataInfo.DOC_TYPE_ID){
        $scope.docType = this.metadataInfo.DOC_TYPE_ID;
      }
      $scope.mainPage = 1;
      if(!$scope.nav){
        $scope.nav = true
      }
      getItems();
    }
  };

  var getItems = function(){
    var params = {
      page:         $scope.mainPage,
      size:         21,
      token:        $scope._session.token,
      FOLDER_ID:    folderId,
      DOC_TYPE_ID:  $scope.docType,
      ENTITY_ID:    $scope.requestIdSelected
    };
    RequestServices.getRequest( urlGetItems, params )
      .success(function(data){
        if (data.result == "OK") {
          $scope.mainTotalPages = data.totalPages;
          $scope.collection = [];
          angular.forEach(data.items, function (value, key) {
            if(value.ICON_TYPE == "Document"){
              if (imageValidate.indexOf(value.CONTENT_TYPE) != -1) {
                value.isImage = true;
              }
            }
            var item = new itemModel(value.FOLDER_NAME,typeItems[typeItemsMap[value.ICON_TYPE]], value);
            $scope.collection.push(item);
          });
          $(document).foundation();
        }else{
          $scope.collection = [];
          $scope.mainTotalPages = 1;
        }
      })
  };

  $scope.levelUp = function(){
    $scope.nav = false;
    $scope.mainPage = 1;
    $scope.docType = null;
    getItems();      
  };

  $scope.showErrors = function(){
    alert("El Error se pudo presentar por los sigts erroes:\n\t-El tamaño del Archivo es mayor a 10Mb\n\t-El Archivo se encuentra dañado\n\t-Problemas con el servidor\nIntenta cargar de nuevo el Archivo o ingrese uno nuevo.");
  };

  var validate = function () {
    $scope.errors = [];
    if (!$scope.requestIdSelected) {
        $scope.errors.push("El 'Contrato' es requerido");
    }
    if (!$scope.files || $scope.files.length==0) {
        $scope.errors.push("'Documento' es requerido");
    }
  };

  $scope.save = function () {
    validate();
    if ($scope.errors.length) {
      return;
    }
    $($scope.fileControl).val('');
    $scope.submit = true;
    var length = $scope.files.length;
    for (var ic = 0; ic < length; ic++) {
      var index = $scope.filesSend.length;
      if($scope.files[0].size < 10000000){
        $scope.send($scope.files[0], index);
      }else{
        $scope.files[0].status = true;
        $scope.files[0].error = true;
      }
      $scope.filesSend.push($scope.files[0]);
      $scope.files.splice(0,1);
    }
  };

  $scope.send = function(file, index){
    var model = {
      DOC_TYPE_ID: $scope.docType,
      ENTITY_ID: $scope.requestIdSelected,
      FOLDER_ID: folderId,
      token: $scope._session.token
    }
    AdminServices.send(file, model, urlUpload, true)
      .success(function (data){
        $scope.filesSend[index].status=true;
        if(data.result = "OK"){
          $scope.filesSend[index].upload = true;
          getItems();
        }else{
          $scope.filesSend[index].error = true;
        }
      })
      .error(function (data){
        $scope.filesSend[index].status=true;
        $scope.filesSend[index].error = true;
      })
  };

  $scope._requestComments = {
    listComments: [],
    comment: null,
    getComments: function(){
      var self = this;
      var params = {
        request: $scope.requestIdSelected
      }
      RequestServices.getRequest( urlListComment, params)
        .success(function (data){
          if (data.result == "OK") {
            self.listComments = data.updates;
          };
        });
    },
    addComment: function(){
      var self = this;
      var params = {
        token: $scope._session.token, // (obligatorio porque de ahí se agrega el usuario que hizo el comment)
        comment:  self.comment, //  (el comentario)
        request: $scope.requestIdSelected //  (la solicitud asociada al comentario(
      }
      RequestServices.getRequest( urlAddComment, params)
        .success(function (data){
          if (data.result == "OK") {
            self.comment = null;
            self.getComments();
          }else{
            alert(data.msg)
          }
        })
    }
  }



  function init(){
    if($cookies.consultasDoc){
      $scope._loginForm.login = $cookies.consultasDoc;
    }
    $scope.$on("fileSelected", function (event, args) {
      $scope.$apply(function () {
        //add the file object to the scope's files collection
        $scope.files.push(args.file);
        $scope.fileControl = args.control;
      });
    }); 
  }

  init();
})