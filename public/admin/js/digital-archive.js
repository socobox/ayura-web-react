var DigitalArchive = angular.module('digitalArchive', []);

DigitalArchive.controller('digitalArchiveCtrl', function($scope, $routeParams, $filter, $location, AdminServices, RequestServices){
  $scope._session  = null;
  var mainPath     = AdminServices.getPath(),
      urlUpload        = mainPath + "content/upload",
      urlGetItems      = mainPath + "content/search",
      urlGetFolders    = mainPath + "content/folder/types",
      urlGetDocuments  = mainPath + "content/document/types",
      urlContractSearh = mainPath + "tenant/contracts/search",
      urlPropRequest   = mainPath + "prop-request/list";

  var urlDeleteItem = mainPath + "content/delete";

  var _format = 'dd/MM/yy';

  $scope.tenantFilter     = null;
  $scope.tenantNroFilter  = null;
  $scope.contractFilter   = null;
  $scope.propFilter       = null;
  $scope.ownerFilter      = null;
  $scope.ownerNroFilter   = null;
  $scope.filters = {
    folder: null,
    docType: null,
    filterId: '2016',
    entityId: null,
    magicBox: null
  };

  $scope.errors = [];
  $scope.fileControl = null;
  $scope.files = [];
  $scope.filesSend = [];

  $scope.collection = [];
  $scope.metadata = [];

  $scope.page = 1;
  $scope.mainPage = 1;
  $scope.totalPages = 10;
  $scope.mainTotalPages = 10;

  $scope.open = true;
  $scope.nav = false;
  $scope.uploadSw = false;
  $scope.backRequest = false;

  $scope.idRequestRefences = null;

  var imageValidate = AdminServices.getImgValidate();
  var typeItems = {
        0: "folder",
        1: "file",
        2: "folder-o",
        3: "files-o",
        4: "folder-open",
        5: "folder-open-o",
        6: "file-text",
        7: "archive",
        8: "file-o",
        9: "file-text-o",
      },
      typeItemsMap = {
        "Folder": 0,
        "Document": 9,
      };

  $scope._ContactFilters = {
    name: null,
    docId: null,
    requestId: null,
    contactId: null
  }

  // MODELS
  function itemModel( name, type, metadata) {
    this.name = name || "",
        this.type = type || "",
        this.metadataInfo = metadata || []
  };
  itemModel.prototype.action = function(evt){
    if( this.metadataInfo.ICON_TYPE == "Folder"){
      for(var i = 0; i<$scope.contentCategories.length;i++){
        if($scope.contentCategories[i].value == this.metadataInfo.FOLDER_ID){
          $scope.filters.folder = $scope.contentCategories[i];
        }
      }
      if(this.metadataInfo.DOC_TYPE_ID){
        for (var i = 0; i < $scope.documentType.length; i++) {
          if ($scope.documentType[i].value == this.metadataInfo.DOC_TYPE_ID) {
            $scope.filters.docType = $scope.documentType[i];
          };
        };
      }
      if(this.metadataInfo.ENTITY_ID && !this.metadataInfo.DOC_TYPE_ID){
        $scope.filters.entityId = {
          label: "#"+this.name,
          value: this.metadataInfo.ENTITY_ID,
          entity: this.name }
      };
      $scope.mainPage = 1;
      if(!$scope.nav){
        $scope.nav = true
      }
      getItems();
    }else{
      console.log(this.metadataInfo);
      console.log("descargando");
    }
  };
  function contractModel( id, contract, propiedad, tenant, tenantId, owner, ownerId, fromC, toC) {
    this.id        = id        || "",
        this.contract  = contract  || "",
        this.propiedad = propiedad || "",
        this.tenant    = tenant    || "",
        this.tenantId  = tenantId  || "",
        this.owner     = owner     || "",
        this.ownerId   = ownerId   || "",
        this.from      = fromC     || "",
        this.to        = toC       || ""
  };
  contractModel.prototype.selected = function(){
    $scope.filters.entityId = {
      label: "#"+this.contract,
      value: this.id,
      entity: this.contract
    }
    $scope.searchItem();
    $('#contractSearch').foundation('reveal', 'close');
  }
  function contactModel( id, name) {
    this.id     = id   || "",
        this.name   = name || ""
  };
  contactModel.prototype.selected = function(){
    $scope.filters.entityId = {
      label: "#"+this.id,
      value: this.id,
      entity: this.id
    }
    $scope.searchItem();
    $('#contactSearch').foundation('reveal', 'close');
  };
  // END MODELS

  $scope._digitalArchive = {
    init: function(){
      $scope.filters.folder   = null;
      $scope.filters.docType  = null;
      $scope.filters.entityId = null;
      getItems();
    },
    search: function(){
      $scope.mainPage = 1;
      getItems();
    }
  };

  $scope.uploadPanel = function (){
    $scope.uploadSw = true;
  };
  $scope.searchPanel = function (){
    $scope.uploadSw = false;
  };
  $scope.levelUp = function(){
    $scope.mainPage = 1;
    if($scope.uploadSw){
      $scope.uploadSw = false;
    }
    if($scope.filters.folder && !$scope.filters.docType.value && !$scope.filters.entityId){
      $scope.filters.folder = $scope.contentCategories[0];
      $scope.nav = false;
      getItems();
    }
    if($scope.filters.folder && $scope.filters.docType && !$scope.filters.docType.value && $scope.filters.entityId){
      $scope.filters.entityId = null;
      getItems();
    }
    if($scope.filters.folder && $scope.filters.docType && $scope.filters.docType.value && !$scope.filters.entityId){
      $scope.filters.docType = $scope.documentType[0];
      getItems();
    }
    if($scope.filters.folder && $scope.filters.docType && $scope.filters.docType.value && $scope.filters.entityId){
      $scope.filters.docType = $scope.documentType[0];
      getItems();
    }
  }
  $scope.next = function(){
    $scope.page += 1;
    $scope.search();
  };
  $scope.prev = function(){
    $scope.page -= 1;
    $scope.search();
  };
  $scope.nextContacts = function(){
    $scope.page += 1;
    $scope.searchContact();
  };
  $scope.prevContacts = function(){
    $scope.page -= 1;
    $scope.searchContact();
  };

  $scope.firstMain = function(){
    $scope.mainPage = 1;
    getItems();
  };
  $scope.nextMain = function(){
    $scope.mainPage += 1;
    getItems();
  };
  $scope.prevMain = function(){
    $scope.mainPage -= 1;
    getItems();
  };
  $scope.lastMain = function(){
    $scope.mainPage = $scope.mainTotalPages;
    getItems();
  };

  // SEARCH'S
  $scope.searchFilter = function(){
    $scope.page = 1;
    $scope.search();
  };
  $scope.seacrhConctactFilter = function(){
    $scope.page = 1;
    $scope.searchContact();
  };
  $scope.searchItem = function(){
    if($scope.filters.folder.value){
      $scope.nav = true;
    }else{
      $scope.nav = false;
    }
    $scope.mainPage = 1;
    getItems();
  };
  $scope.deteleItem = function(i){
    // console.log(i);
    var confirm = window.confirm("seguro que desea borrar el archivo?");
    // console.log(confirm)
    if (!confirm) {
      return
    };
    params = {
      token: $scope._session.token,
      id: i.metadataInfo.ID
    }
    RequestServices.getRequest(urlDeleteItem, params)
        .success(function (data) {
          //console.log(data);
          if (data.result == "OK") {
            getItems();
            alert(data.msg);
          } else {
            alert(data.msg);
          }

        });
  };

  $scope.search = function(){
    params = {
      page: $scope.page,
      size: 10,
      token: $scope._session.token,
      nameFilter: $scope.tenantFilter,
      documentFilter: $scope.tenantNroFilter,
      contractFilter: $scope.contractFilter,
      propertyFilter: $scope.propFilter,
      ownerNameFilter: $scope.ownerFilter,
      ownerDocFilter: $scope.ownerNroFilter
    }
    RequestServices.getRequest(urlContractSearh, params)
        .success(function (data) {
          //console.log(data);
          if (data.result == "OK") {
            $scope.totalPages = data.totalPages;
            $scope.contracts = [];
            angular.forEach(data.contracts, function (value, key) {
              var item = new contractModel( value.CONTRACT_ID, value.CONTRACT_CODE, value.PROPERTY_ID, value.TENANT_NAME, value.TENANT_DOCUMENT, value.OWNER_NAME, value.OWNER_DOCUMENT, value.CONTRACT_START_DATE, value.CONTRACT_END_DATE );
              $scope.contracts.push(item);
            });
            if ($scope.totalPages == 1 && $scope.contracts.length == 1) {
              $scope.contracts[0].selected();
            };
          } else {
            alert(data.msg);
          }

        });
  };
  $scope.searchContact = function(){
    var params = {
      token: $scope._session.token,
      page: $scope.page,
      size: 5,
      nameFilter: $scope._ContactFilters.name,   // (Filtro de nombre de la persona del contacto)
      requestFilter: $scope._ContactFilters.requestId,// (filtro por ID del contacto)
      contactFilter: $scope._ContactFilters.contactId,
      docFilter: $scope._ContactFilters.docId
    };
    RequestServices.getRequest( urlPropRequest, params)
        .success(function (data){
          $scope.contacts = [];
          $scope.requestIdSelected = null;
          if (data.result == "OK") {
            $scope.totalPagesContact = data.totalPages;
            angular.forEach( data.requests, function ( request, key){
              var req = new contactModel(request.id, request.contact_name);
              $scope.contacts.push(req);
            })
            if ($scope.totalPagesContact == 1 && $scope.contacts.length == 1) {
              $scope.contacts[0].selected();
            };
          };
        })
  };
  // END SEARCH'S

  // UPLOAD SECTION
  var validate = function () {
    $scope.errors = [];
    if ($scope.filters.folder.value ==1 || $scope.filters.folder.value ==2) {
      if (!$scope.filters.entityId) {
        $scope.errors.push("El 'Contrato' o La 'Solicitud' es requerida");
      }
    };
    if (!$scope.files || $scope.files.length==0) {
      $scope.errors.push("'Documento' es requerido");
    }
  }
  $scope.showErrors = function(){

    alert("El Error se pudo presentar por los sigts erroes:\n\t-El tamaño del Archivo es mayor a 10Mb\n\t-El Archivo se encuentra dañado\n\t-Problemas con el servidor\nIntenta cargar de nuevo el Archivo o ingrese uno nuevo.");
  };

  $scope.save = function () {
    validate();
    if ($scope.errors.length) {
      return;
    }
    $($scope.fileControl).val('');
    $scope.submit = true;
    var length = $scope.files.length;
    for (var ic = 0; ic < length; ic++) {
      var index = $scope.filesSend.length;
      if($scope.files[0].size < 15000000){
        $scope.send($scope.files[0], index);
      }else{
        $scope.files[0].status = true;
        $scope.files[0].error = true;
      }
      $scope.filesSend.push($scope.files[0]);
      $scope.files.splice(0,1);
    }
  };

  $scope.send = function(file, index){
    var model = {
      DOC_TYPE_ID: $scope.filters.docType.value,
      ENTITY_ID: ($scope.filters.folder.value == 1 || $scope.filters.folder.value == 2) ? $scope.filters.entityId.value : -1,
      FOLDER_ID: $scope.filters.folder.value,
      FILTER_VALUE: $scope.filters.filterId && ($scope.filters.docType.value == 308 || $scope.filters.docType.value == 309) ? $scope.filters.filterId:$scope.filters.magicBox,
      token: $scope._session.token
    }
    console.log(model);
    AdminServices.send(file, model, urlUpload, true)
        .success(function (data){
          $scope.filters.magicBox = "";
          $scope.filesSend[index].status=true;
          if(data.result = "OK"){
            $scope.filesSend[index].upload = true;
            getItems();
          }else{
            $scope.filesSend[index].error = true;
          }
        })
        .error(function (data){
          $scope.filesSend[index].status=true;
          $scope.filesSend[index].error = true;
        })
  };

  $scope.deleteFile = function(index){
    $scope.files.splice(index,1);
  }
  // END UPLOAD SECTION

  var loadFolders = function(){
    var params = {
      token: $scope._session.token
    }
    RequestServices.getRequest( urlGetFolders, params)
        .success(function (data){
          if (data.result == "OK") {
            // $scope.contentCategories = data.options;
            $scope.contentCategories = [];
            for (var i = 0; i < data.options.length; i++) {
              if(data.options[i].value != 7){
                $scope.contentCategories.push(data.options[i]);
              }else{
                if ($scope._session.role == 1 || $scope._session.id == 43725897) {
                  $scope.contentCategories.push(data.options[i]);
                };
              }
            };
            $scope.contentCategories.unshift({
              label: "TODOS",
              value: null
            });

            if($routeParams.folderId && $routeParams.folderId != 7){
              for (var i = 0; i < $scope.contentCategories.length; i++) {
                if($scope.contentCategories[i].value == $routeParams.folderId){
                  $scope.filters.folder = $scope.contentCategories[i];
                  if ($routeParams.folderId == 2) {
                    $scope.contractFilter = $routeParams.entity;
                    $scope.search();
                  };
                  if ($routeParams.folderId == 1) {
                    if ($routeParams.backRequest) {
                      $scope.backRequest = true;
                    };
                    $scope._ContactFilters.requestId = $routeParams.entity;
                    $scope.searchContact();
                  };
                }
              };
            }else if($routeParams.folderId == 7){
              if ($scope._session.role == 1 || $scope._session.id == 43725897) {
                $scope.contentCategories.push(data.options[i]);
              };
            }else{
              $scope.filters.folder = $scope.contentCategories[0];
            }
          } else {
            alert(data.msg);
          }
        })
  };
  var loadDocuments = function(){
    if($scope.filters.folder && $scope.filters.folder.value){
      var params = {
        folder: $scope.filters.folder.value,
        token: $scope._session.token
      }
      RequestServices.getRequest( urlGetDocuments, params)
          .success(function (data) {
            // console.warn("here")
            if (data.result == "OK") {
              $scope.documentType = [];
              $scope.documentType = data.options;
              $scope.documentType.unshift({
                label: "TODOS",
                value: null
              })
              $scope.filters.docType = $scope.documentType[0];
            } else {
              alert(data.msg);
            }
          });
    }else{
      $scope.documentType = [];
      $scope.documentType.unshift({
        label: "TODOS",
        value: null
      });
      $scope.filters.docType = $scope.documentType[0];
    }
  };
  var getItems = function(){
    $scope.idRequestRefences = null;
    var params = {
      page:         $scope.mainPage,
      size:         21,
      token:        $scope._session.token,
      FOLDER_ID:    $scope.filters.folder   ? $scope.filters.folder.value   : null,
      DOC_TYPE_ID:  $scope.filters.docType  ? $scope.filters.docType.value  : null,
      ENTITY_ID:    $scope.filters.entityId ? $scope.filters.entityId.value : null,
      FILTER_VALUE: $scope.filters.magicBox
    };
    RequestServices.getRequest( urlGetItems, params )
        .success(function(data){
          if (data.result == "OK") {
            $scope.mainTotalPages = data.totalPages;
            $scope.collection = [];
            if (data.ID_REQUEST) {
              $scope.idRequestRefences = data.ID_REQUEST;
            };
            angular.forEach(data.items, function (value, key) {

              if(value && value.CONTENT_PATH){
                value.CONTENT_PATH = value.CONTENT_PATH.replace('http:','https:');
              }
              if(value.ICON_TYPE == "Document"){
                if (imageValidate.indexOf(value.CONTENT_TYPE) != -1) {
                  value.isImage = true;
                }
              }

              var item = new itemModel(value.FOLDER_NAME,typeItems[typeItemsMap[value.ICON_TYPE]], value);

              if (value.FOLDER_ID != 7) {
                $scope.collection.push(item);

              }else{
                if ($scope._session.role == 1 || $scope._session.id == 43725897) {
                  $scope.collection.push(item);
                };
              }
            });
            $(document).foundation();
          }else{
            $scope.collection = [];
            $scope.mainTotalPages = 1;
          }
        })
  };

  $scope.showPhotos = function(){
    $scope.idRequestRefences = null;
    var params = {
      page:         $scope.mainPage,
      size:         500,
      token:        $scope._session.token,
      FOLDER_ID:    $scope.filters.folder   ? $scope.filters.folder.value   : null,
      DOC_TYPE_ID:  $scope.filters.docType  ? $scope.filters.docType.value  : null,
      ENTITY_ID:    $scope.filters.entityId ? $scope.filters.entityId.value : null,
      FILTER_VALUE: $scope.filters.magicBox
    };
    RequestServices.getRequest( urlGetItems, params )
        .success(function(data){

          var container = $('#imageGallery');
          container.empty();
          angular.forEach(data.items, function(item){
            if(imageValidate.indexOf(item.CONTENT_TYPE) != -1){

              container.append('<a href="'+item.CONTENT_PATH+'" data-lightbox="allGallery">'+item.FILE_NAME+'</a>');
            }
          });
          container.find('a').first().trigger('click')

        });
  }

  $scope.panorama = function (evt, url) {
    url = url.replace('http:','https:');
    evt.preventDefault();

    $(document).on('opened.fndtn.reveal', '[data-reveal]', function () {
      var modal = $(this);
      $('#js-panorama *').remove()

      // Loader
      var loader = document.createElement('div');
      loader.className = 'fa fa-2x fa-spinner fa-spin ';
      console.log(url)


      // Panorama display
      var div = document.getElementById('js-panorama')
      div.className = 'text-center';
      div.style.height = '30px';



      PSV = new PhotoSphereViewer({
        // Path to the panorama
        panorama: url,

        // Container
        container: div,

        // Deactivate the animation
        time_anim: false,
        mousewheel: false,

        // Display the navigation bar
        navbar: [
          'fullscreen'
        ],
        default_fov: 179,

        // Resize the panorama
        size: {
          width: '900px',
          height: '600px'
        },
        loading_txt: 'Loading'
      });
      $scope.$apply();
    });
  }

  init = function(){
    AdminServices.validateSession();
    $(document).foundation();
    $scope._session = AdminServices.getSession();
    loadFolders();
    $scope.$watch('filters.folder', function () {
      if($scope.filters.folder){
        $scope.filters.entityId = null;
        $scope.filters.docType  = null;
        $scope.filesSend = [];
        $scope.uploadSw = false;
        getItems();
        loadDocuments();
      }
    });
    $scope.$watch('filters.docType', function () {
      if($scope.filters.docType){
        $scope.filters.magicBox = null;
      }
    });
    $scope.$watch('filters.entityId', function () {
      $scope.filesSend = [];
    });
    $scope.$on("fileSelected", function (event, args) {
      $scope.$apply(function () {
        args.file.carpeta = $scope.filters.folder.label;
        args.file.documento = $scope.filters.docType.label;
        $scope.files.push(args.file);
        $scope.fileControl = args.control;
      });
    });
  }
  init();
});
