var Services = angular.module("services", []);
var url = document.URL;

var path = "https://www.arrendamientosayura.com/";

if (url.indexOf("localhost") != -1) {
    path = "https://www.arrendamientosayura.com/";
    // path = "https://192.168.2.202:3443/";
}

if (url.indexOf("192.168.2.") != -1) {
    path = "https://www.arrendamientosayura.com/";
    path = "https://192.168.2.202:3443/";
}

Services.factory('RequestServices',function($http){
  return {
    getRequest: function( urlRequest, params ){
      return $http({
        method: "GET",
        url: urlRequest,
        params: params
      }).success(function(data){
          return data;
      });
    },

    postRequest: function( urlRequest, params ){
      return $http({
        method: "POST",
        url: urlRequest,
        data: params
      }).success(function(data){
          return data;
      });
    }
  }
});



Services.factory('logoServices',function(){
  var logo = {};
  var mainLogoPath = "images/logo_square.jpg";
  var styleLogo = "padding: 5px 5px 5px 10px;max-height: 160px";

  logo.getLogoResource = function(){
    return mainLogoPath;
  }

  logo.getStyle = function(){
    return styleLogo;
  }
  return logo;
});

Services.factory('AdminServices',function($http, $location, $filter, RequestServices){
  var mainPath = path,
  urlCities       = mainPath + "address/city/options",
  urlListFeatures = mainPath + "features/list",
  urlListUser     = mainPath + "user/list",
  urlPropType     = mainPath + "property/type/options",
  urlGetAgents    = mainPath + "agent/list",
  urlGetDatesAppointments    = mainPath + "agenda/overview",
  urlGetDatesContactRequests = mainPath + "contact-request/list",
  adminServices = {},
  _session = {
    token: null,
    role: null,
    id: 0
  },
  features = [],
  cities = [],
  citiesMap = {},
  agents =[],
  agentsMap = {},
  _userList = [],
  _userMap = [],
  _propertyTypes = [],
  _propertyTypesMap = [],
  propMap = {},
  propertiesViews = {},
  lastLocation = "",
  _prop = null,
  _owner = null;

  var _statusMap = {
    "": "Todos",
    1: "Arrendado",
    0: "Disponible",
    99: "Disponible/Solicitud",
    4: "Liberado",
    2: "Retirado",
    3: "Solicitud",
    5: "Vendido"
  };

  var _statusList = [
    {value: "", label: "Todos"},
    {value: 1, label: "Arrendado"},
    {value: 99, label: "Disponible/Solicitud"},
    {value: 0, label: "Disponible"},
    {value: 4, label: "Liberado"},
    {value: 2, label: "Retirado"},
    {value: 3, label: "Solicitud"},
    {value: 5, label: "Vendido"}
  ];

  var imageValidate = [
    "image/bmp",
    "image/cis-cod",
    "image/gif",
    "image/ief",
    "image/jpeg",
    "image/pipeg",
    "image/png",
    "image/svg+xml",
    "image/tiff",
    "image/x-cmu-raster",
    "image/x-cmx",
    "image/x-icon",
    "image/x-portable-anymap",
    "image/x-portable-bitmap",
    "image/x-portable-graymap",
    "image/x-portable-pixmap",
    "image/x-rgb",
    "image/x-xbitmap",
    "image/x-xpixmap",
    "image/x-xwindow"
  ];

  adminServices.flash = {
    "employee": null,
    "property": null,
    "owner": null,
    copyMode: false,
    clear: function () {
      this.copyMode = false;
      $(".js-flash-mode").removeClass("hide");
    }
  };

  //an array of files selected
  var files = [],
  fileControl = null;

  adminServices.getImgValidate = function(){
    return imageValidate;
  }

  adminServices.getStatusMap = function(){
    return _statusMap;
  }

  adminServices.getStatusList = function(){
    return _statusList;
  }

  adminServices.saveProp = function( prop ){
    _prop = prop;
  }

  adminServices.getPropSaved = function(){
    return _prop;
  }

  adminServices.saveOwner = function( owner ){
    _owner = owner;
  }

  adminServices.getOwnerSaved = function(){
    return _owner;
  }

  adminServices.setSessionToken = function( token ){
    _session.token = token;
  }
  adminServices.setSessionRole = function( role ){
    _session.role = role;
  }
  adminServices.setSessionId = function( id ){
    _session.id = id;
  }

  adminServices.getSession = function(){
    return _session;
  }
  
  adminServices.getPath = function (){
    return mainPath;
  }

  adminServices.loadFeatures = function () {
    RequestServices.getRequest( urlListFeatures )
      .success(function (data) {
        if (data.result == "OK") {
          features = data.features;
        }

      });
  };

  adminServices.getFeatures = function (){
    return features;
  };

  adminServices.getAppointmentsByDates = function(params, callback){
    RequestServices.getRequest(urlGetDatesAppointments, params)
      .success(function (data) {
        callback(data);
      });
  };

  adminServices.getContactRequestByDates = function(params, callback){
    RequestServices.getRequest(urlGetDatesContactRequests, params)
        .success(function (data) {
          callback(data);
        });
  };

  adminServices.loadCities = function () {
    RequestServices.getRequest( urlCities )
      .success(function (data) {
        if (data.result == "OK") {
          cities = $filter('orderBy')(data.options, 'label');
          cities.splice(0,1);
          cities.unshift({value: null, label: "Todos"});
          angular.forEach(cities, function (value, key) {
            citiesMap[value.value] = key;
          });
        }
      });
  };

  adminServices.getCities = function (){
    return cities;
  };

  adminServices.getCitiesMap = function (){
    return citiesMap;
  };

  adminServices.loadAgents = function () {
    RequestServices.getRequest( urlGetAgents )
      .success(function (data) {
        if (data.result == "OK") {
          agents = data.agents;
          angular.forEach(agents, function (agent, key){
            agentsMap[agent.ID] = key;
          })
        } else {
          console.log(data.msg);
        }

      });
  };

  adminServices.loadNewAgents = function (callback) {
    RequestServices.getRequest( urlGetAgents )
      .success(function (data) {
        if (data.result == "OK") {
          callback(agents);
        } else {
          console.log(data.msg);
        }

      });
  };

  adminServices.getAgents = function (callback){
    return agents;
  };

  adminServices.getAgentsMap = function (){
    return agentsMap;
  };

  adminServices.loadListUser = function () {
    RequestServices.getRequest( urlListUser )
      .success(function (data) {
        if (data.result == "OK") {
          _userList = data.users;
          angular.forEach(_userList, function (value, key) {
            _userMap[value.ID] = value;
          });
        } else {
          alert(data.msg);
        }

      }
    );
  };

  adminServices.getListUser = function (){
    return _userList;
  };

  adminServices.getUserMap = function (){
    return _userMap;
  };

  adminServices.loadPropertyTypes = function () {
    RequestServices.getRequest( urlPropType )
      .success(function (data) {
        if (data.result == "OK") {
          _propertyTypes = data.options;
          _propertyTypes.unshift({value: "", label: "Todos"});

          angular.forEach(data.options, function (value, key) {
            _propertyTypesMap[value.value] = value.label;
            propMap[value.value] = key;
          });
        }
      });
  };

  adminServices.getPropertyTypes = function (){
    return _propertyTypes;
  };

  adminServices.getPropertyTypesMap = function (){
    return _propertyTypesMap;
  };

  adminServices.getPropMap = function (){
    return propMap;
  };

  adminServices.setPropertiesViews = function (properties){
    propertiesViews = properties;
  };

  adminServices.getPropertiesViews = function (){
    return propertiesViews;
  };

  adminServices.getLastPropSearch = function () {
    return lastLocation;
  }
  adminServices.setLastPropSearch = function (location) {
    lastLocation = location;
  }

  adminServices.getUrlBack = function () {
    return lastLocation;
  }
  adminServices.setUrlBack = function (location) {
    lastLocation = location;
  }

  adminServices.range = function (from, to) {
    var tmp = [];
    for (var i = from; i <= to; i++) {
      tmp.push(i);
    }
    return tmp;
  };

  adminServices.createToken = function () {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
      var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
      return v.toString(16);
    });
  };

  adminServices.send = function (file, prop, urlPath, sw,landscape) {
    return $http({
      method: 'POST',
      cache: false,
      url: urlPath,
      //IMPORTANT!!! You might think this should be set to 'multipart/form-data'
      // but this is not true because when we are sending up files the request
      // needs to include a 'boundary' parameter which identifies the boundary
      // name between parts in this multi-part request and setting the Content-type
      // manually will not set this boundary parameter. For whatever reason,
      // setting the Content-type to 'false' will force the request to automatically
      // populate the headers properly including the boundary parameter.
      headers: { 'Content-Type': undefined },
      //This method will allow us to change how the data is sent up to the server
      // for which we'll need to encapsulate the model data in 'FormData'
      transformRequest: function (data) {
        var formData = new FormData();
        //now add all of the assigned files
        var day = new Date();
        var name = "";
        if(sw){
          var point = data.file.name.lastIndexOf('.');
          name = data.file.name.substring(0,point)+"_"+$filter('date')(day, 'yyMMdd')+data.file.name.substring(point);
        }else{
          name = adminServices.createToken();
        }
        formData.append("file", data.file, name);
        //need to convert our json object to a string version of json otherwise
        // the browser will do a 'toString()' on the object which will result
        // in the value '[Object object]' on the server.
        console.log(data);
        data.model.landscape = landscape;
        formData.append("model", angular.toJson(data.model));

        return formData;
      },
      //Create an object that contains the model and files which will be transformed
      // in the above transformRequest method
      data: { model: prop, file: file }
    }).
      success(function (data, status, headers, config) {
        return data;
      }).
      error(function (data, status, headers, config) {
        return data;
      });
  };

  adminServices.validateSession = function( callback ){
    if( _session.token ){
      if( _session.role==1 || _session.role==6 || _session.role==7 || _session.role==5 || _session.role==13 || _session.role==8 || _session.id==1037571770 || _session.id==43630114 || _session.id==1037645596 ){
        $(".js-properties").addClass("show");
      }

      if( _session.role==1 || _session.role==4){
        $(".js-inquilinos").addClass("show");
      }

      if( _session.role==1 ){
        $(".js-solicitudes").addClass("show");
      }
      if( callback ) {callback()};
    }else{
      $location.url("/");
    }
  };

  return adminServices;
})

Services.factory('DateServices',function(){
  var dateServices = {},
  dayNames = ['Domingo', 'Lunes', 'Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado'],
  monthNames = [ "Enero", "Febrero", "Marzo", "Abril", "Mayo", "Junio", "Julio", "Agosto", "Septiembre", "Octubre", "Noviembre", "Diciembre" ];

  dateServices.formatDate = function (day) {
    return (dateServices.zeroPad(day.getMonth() + 1, 2) + "/" + dateServices.zeroPad(day.getDate(), 2) + "/" + dateServices.zeroPad(day.getFullYear()));
  };

  dateServices.dateAsText = function (d) {
    return d.getFullYear() + "-" + dateServices.zeroPad(d.getMonth() + 1, 2) + "-" + dateServices.zeroPad(d.getDate(), 2);
  };

  dateServices.getTime = function (date) {
    return dateServices.zeroPad(date.getHours(), 2) + ":" + dateServices.zeroPad(date.getMinutes(), 2) + ":00"
  };

  dateServices.parseDate = function (input) {
    var parts = [];
    if (input.indexOf('-') > -1) {
      parts = input.split('-');
      return new Date(parts[0], parts[1] - 1, parts[2]); // months are 0-based
    } else {
      parts = input.split('/');
      return new Date(parts[2], parts[0] - 1, parts[1]); // months are 0-based
    }
  };

  dateServices.zeroPad = function (num, places) {
    var zero = places - num.toString().length + 1;
    return new Array(+(zero > 0 && zero)).join("0") + num;
  };


  dateServices.timeText = function (date) {
    return dateServices.zeroPad(date.getHours(), 2)
      + ":" + dateServices.zeroPad(date.getMinutes(), 2);
  }


  dateServices.addMinutes = function (date, minutes) {
    return new Date(date.getTime() + minutes * 60000);
  };

  dateServices.getDayName = function (date) {
    return dayNames[ date.getDay()];
  };

  dateServices.getMonthName = function (date) {
    return monthNames[ date.getMonth()];
  };

  dateServices.addDays =function(date, days){
    var d = new Date();
    d.setDate(date.getDate() + days);
    return d;
  };

  dateServices.hourBasicFromText = function (milHour) {

    var tmp = milHour.split(":");
    var end=parseInt(tmp[0]);
    var suffix = " am";

    if(end>=12){
      suffix = " pm";
    }

    if (end > 12) {
      end = end - 12;

    }

    return  dateServices.zeroPad(end, 2)
      + ":" + dateServices.zeroPad(parseInt(tmp[1]), 2) + suffix
  };


  dateServices.hourText = function (date) {

    if (date == null) {
      return "";
    }

    var suffix = " am";

    var end = date.getHours();

    if (end > 12) {
      end = end - 12;
      suffix = " pm";
    }

    return  dateServices.zeroPad(end, 2)
      + ":" + dateServices.zeroPad(date.getMinutes(), 2) + suffix
  };

  return dateServices;
})

Services.factory('EmployeeServices',function($http){
  var urlEmployeeList = path + "employee/list",
  employeeServices  = {
    options: [],
    list: [],
    map: {},
    load: function () {
      var self = this;
      $http({
        method: "GET",
        url: this.getUrl(),
        params: {}
      }).success(function (data) {
        if (data.result == "OK") {
          self.options = data.employees.slice(0);
          self.list = data.employees;
          self.options.unshift({ID: "", NAME: "Todos"});
          angular.forEach(data.employees, function (value, key) {
            self.map[value.ID] = value;
          });
        }
      });
    },
    find: function (value) {
      var employee = null;
      angular.forEach(data.options, function (value, key) {
        if (value.value == value) {
          employee = value;
        }
      });
      return employee;
    }
  }

  employeeServices.getUrl = function(){
    return urlEmployeeList;
  }
  return employeeServices;
})

Services.factory('SettingServices',function($http){
  var urlEmployeeList = path + "/employee/list";
  var settingServices = {};
  
  return settingServices;
})  
