var path = "https://www.arrendamientosayura.com";

var url = document.URL;


if (url.indexOf("hansospina") != -1) {
    path = "https://hansospina:3443";
}



angular.module('mainApp', ["services"])
    .controller('MainCtrl', function ($rootScope, $scope, $http, $timeout, $sce, $location, $filter, logoServices) {
        $scope._target = 0;

        $scope.logo = logoServices.getLogoResource();
        $scope.styleLogo = logoServices.getStyle();

        $scope._property = null;

        $scope.tab = 1;

        $scope.searchMode = false;


        $scope.currentPage = 1;
        $scope.totalPages = 1;

        $scope.currentProperty = null;
        $scope.properties = null;
        $scope.contactMe = false;
        $scope._propertyCode = null;

        $scope.myProperties = [];


        $scope.extraOptions = false;

        $scope.started = false;

        $scope.hasMore = false;
        $scope.hasPrev = false;

        $scope._propertyCondo = null;

        $scope.propertyRooms = [
            { value: null, label: "Todos" },
            { value: 1, label: "0 a 1" },
            { value: 2, label: "2" },
            { value: 3, label: "3" },
            { value: 4, label: "4 o más" }
        ];

        $scope.parkingOptions = [
            { value: null, label: "Todos" },
            { value: "true", label: "Con Parqueadero" },
            { value: "false", label: "Sin Parqueadero" }
        ];

        $scope.condoOptions = [
            { value: null, label: "Todos" },
            { value: "true", label: "Sí" },
            { value: "false", label: "No" }
        ];


        $scope._propertyParking = $scope.parkingOptions[0];
        $scope._propertyCondo = $scope.condoOptions[0];
        $scope._propertyRoom = $scope.propertyRooms[0];

        $scope.validateDetail = validateDetail;

        $scope.showIntro = function () {
            $('.intro_link').trigger('click');
        }



        function validateDetail(detail, prop) {

            if (prop.SERVICE_TYPE == 0) {
                return true;
            };

            if (detail.name == 'Nombre Urbanización') {
                return false;
            };

            return true;
        }

        function loadBanners() {
            $http({
                method: "GET",
                url: "https://archivo.digital/api/data/v1/row/find/pre",
                params: {
                    token: "",
                }
            }).success(function (data) {
                if (data.success) {
                    $scope.banners = data.results;
                }
            });
        }

        var mySwiper;

        var PSV;

        $scope.$on('ngRepeatFinished', function () {
            console.warn("INIT SWIPER");
            var config = {
                mode: 'horizontal',
                speed: 1000,
                autoplay: 3000,
                autoplayDisableOnInteraction: false,
                loop: true,
                keyboardControl: true,
                mousewheelControl: false,
                paginationClickable: true,
                slidesPerView: 1,
                simulateTouch: false,
                pagination: '.swiper-pagination'
            }
            if ($scope.banners.length == 1) {
                config.loop = false;
            };
            mySwiper = new Swiper('.js-banner-primary', config);
        });

        // Commit asdasdasd;

        $(document).on('close.fndtn.reveal', '[data-reveal]', function () {
            if (PSV) {
                PSV.destroy();
                $('#js-panorama *').remove()
            }
        })

        $scope.demo = function (url) {
            console.log(url);
            var newlink = document.createElement('a');
            newlink.setAttribute('target', '_blank');
            newlink.setAttribute('href', url);
            document.body.appendChild(newlink);
            newlink.click();
            newlink.parentNode.removeChild(newlink)
            mySwiper.slideNext(true, 1000);
            mySwiper.startAutoplay();
        }
        $scope.panorama = function (evt, url) {
            evt.preventDefault();

            $(document).on('opened.fndtn.reveal', '[data-reveal]', function () {
                var modal = $(this);
                $('#js-panorama *').remove()

                // Loader
                var loader = document.createElement('div');
                loader.className = 'fa fa-2x fa-spinner fa-spin ';
                console.log(url)

                // Panorama display
                var div = document.getElementById('js-panorama')
                div.className = 'text-center';
                div.style.height = '30px';



                PSV = new PhotoSphereViewer({
                    // Path to the panorama
                    panorama: url,

                    // Container
                    container: div,

                    // Deactivate the animation
                    time_anim: false,
                    mousewheel:false,

                    // Display the navigation bar
                    navbar: [
                        'fullscreen'
                      ],
                    default_fov	:179,

                    // Resize the panorama
                    size: {
                        width: '900px',
                        height: '600px'
                    },
                    loading_txt: 'Loading'
                });
                $scope.$apply();
            });
        }


        $scope.preSearch = function () {

            $scope.currentPage = 1;
            $scope.totalPages = 0;


            $http({
                method: "GET",
                url: path + "/property/search",
                params: {
                    area: "51,100",
                    city: 1,
                    page: 1,
                    price: "500001,1000000",
                    size: 10,
                    target: 0,
                    type: 1,
                    status: 0
                }
            }).success(function (data) {

                if (data.result == "OK") {
                    $scope.properties = data.properties;
                    $scope.totalPages = data.total;

                    $scope.hasMore = ($scope.currentPage < $scope.totalPages);
                    $scope.hasPrev = ($scope.currentPage > 1);


                    $scope.tab = 0;
                    $scope.loadPhotos();
                }

            });
        }

        $scope._contactForm = {
            name: null,
            email: null,
            phone: null,
            message: null,
            clear: function () {
                this.name = null;
                this.email = null;
                this.phone = null;
                this.message = null;
            }
        };


        $scope.loadCities = function () {
            $http({
                method: "GET",
                url: path + "/address/city/options",
                params: {}
            }).success(function (data) {

                if (data.result == "OK") {
                    $scope.cities = $filter('orderBy')(data.options, 'label');
                    $scope.cities.splice(0, 1);
                    angular.forEach($scope.cities, function (city, key) {
                        if (city.value == 1) {
                            $scope.cityIndex = key;
                            $scope._city = $scope.cities[$scope.cityIndex];
                            $scope._cityDeposit = $scope.cities[$scope.cityIndex];
                        };
                    });
                }

            });
        };

        $scope.loadPropertyTypes = function () {


            $http({
                method: "GET",
                url: path + "/property/type/options",
                params: {}
            }).success(function (data) {

                if (data.result == "OK") {
                    $scope.propertyTypes = data.options;
                    $scope._propertyType = $scope.propertyTypes[0];

                }

            });

        };

        $scope.loadPropertyAreas = function () {

            $http({
                method: "GET",
                url: path + "/property/area/options",
                params: {}
            }).success(function (data) {

                if (data.result == "OK") {
                    $scope.propertyAreas = data.options;
                    $scope.propertyAreas.unshift({ value: null, label: "Todos" });
                    $scope._propertyArea = $scope.propertyAreas[2];
                }



            });

        };

        $scope.targetChanged = function () {

            $scope.loadPricing();

        };

        $scope._paths = {
            0: {
                price: path + "/property/rent/price/options"
            },

            1: {
                price: path + "/property/sell/price/options"
            }
        };

        $scope.clear = function () {
            $scope._propertyPrice = $scope.propertyPrices[2];
            $scope._city = $scope.cities[$scope.cityIndex];
            $scope._propertyArea = $scope.propertyAreas[2];
            $scope._propertyType = $scope.propertyTypes[0];
            $scope._propertyRoom = $scope.propertyRooms[0];
            $scope._propertyParking = $scope.parkingOptions[0];
            $scope._propertyCode = null;
            $scope._propertyCondo = null;
            $scope._target = 0;
            $scope.loadPricing();
        };


        $scope.loadPricing = function () {


            $http({
                method: "GET",
                url: $scope._paths[$scope._target].price,
                params: {}
            }).success(function (data) {

                if (data.result == "OK") {
                    $scope.propertyPrices = data.options;
                    $scope.propertyPrices.unshift({ value: null, label: "Todos" });
                    $scope._propertyPrice = $scope.propertyPrices[2];

                }

            });

        };

        $scope.loadNews = function () {
            $http({
                method: "GET",
                url: path + "/news/list",
                params: {}
            }).success(function (data) {

                if (data.result == "OK") {
                    $scope.news = data.options;

                    angular.forEach($scope.news, function (value, key) {
                        value.htmlBody = $sce.trustAsHtml(value.body);
                    });

                    $scope._newsItem = $scope.news[0];
                    $("#newsBody").html($scope._newsItem.body);
                }

            });
        };

        $scope.changeNews = function (idx) {
            $scope._newsItem = news[idx]
        };

        $scope.search = function () {
            $scope.currentPage = 1;
            $scope.totalPages = 0;
            $scope._search();
        };


        $scope.addContactRequest = function () {

            $http({
                method: "GET",
                url: path + "/contact-request/add",
                params: {
                    name: $scope._contactForm.name,
                    email: $scope._contactForm.email,
                    phone: $scope._contactForm.phone,
                    msg: $scope._contactForm.message,
                    type: "WEB"
                }
            }).success(function (data) {

                if (data.result == "OK") {
                    alert("Su solicitud ha sido enviada, prontamente lo estaremos contactando.");
                    $scope._contactForm.clear();
                } else {
                    alert(data.msg);
                }

            });

        };

        $scope.addPropertyContactRequest = function () {

            if (!$scope.contactPropertyName || !$scope.contactPropertyPhone) {
                alert("Por favor ingrese su nombre y su teléfono para poder contactarlo.");
                return;
            }

            $http({
                method: "GET",
                url: path + "/contact-request/add",
                params: {
                    type: "WEB",
                    name: $scope.contactPropertyName,
                    email: $scope.contactPropertyEmail,
                    phone: $scope.contactPropertyPhone,
                    msg: "Deseo que me contacten acerca de la propiedad con consecutivo: #" + $scope._property.ID + " cod: " + $scope._property.CODE
                }
            }).success(function (data) {

                if (data.result == "OK") {
                    alert("Su solicitud ha sido enviada, prontamente lo estaremos contactando.");
                    if (!$scope.webView) {
                        $scope._property = null;
                    };
                    contactMe = false;
                } else {
                    alert(data.msg);
                }

            });

        };


        $scope._search = function () {


            $scope.searchMode = true;

            $http({
                method: "GET",
                url: path + "/property/search",
                params: {
                    type: $scope._propertyType.value,
                    target: $scope._target,
                    price: $scope._propertyPrice.value,
                    area: $scope._propertyArea.value,
                    city: $scope._city.value,
                    code: $scope._propertyCode,
                    page: $scope.currentPage,
                    rooms: $scope._propertyRoom.value,
                    parking: $scope._propertyParking.value,
                    condo: $scope._propertyCondo.value,
                    status: 0,
                    size: 10
                }
            }).success(function (data) {

                if (data.result == "OK") {
                    $scope.properties = data.properties;
                    $scope.totalPages = data.total;

                    $scope.hasMore = ($scope.currentPage < $scope.totalPages);
                    $scope.hasPrev = ($scope.currentPage > 1);

                    $scope.tab = 0;
                    $scope.loadPhotos();
                }

            });
        };

        $scope.previousPage = function () {
            $scope.currentPage = $scope.currentPage - 1;
            $scope._search();
        };

        $scope.firstPage = function () {
            $scope.currentPage = 1;
            $scope._search();
        };

        $scope.lastPage = function () {
            $scope.currentPage = $scope.totalPages;
            $scope._search();
        };

        $scope.nextPage = function () {
            $scope.currentPage = $scope.currentPage + 1;
            $scope._search();
        };

        $scope.watchProperty = function (idx) {
            $scope._property = $scope.myProperties[idx];

            //console.log($scope._property.photos);
            $(document).foundation('clearing');
        };

        $scope.setProperty = function (idx) {
            $scope._property = $scope.properties[idx];

            var exists = false;
            angular.forEach($scope.myProperties, function (property, key) {
                if ($scope._property.ID == property.ID) {
                    exists = true;
                }
            });

            if (!exists) {
                $scope.myProperties.unshift($scope._property);
            }

            //console.log($scope._property.photos);
        };

        $scope.getProperty = function (propId) {
            $http({
                method: "GET",
                url: path + "/property/fetch",
                params: {
                    id: propId
                }
            }).success(function (data) {
                if (data.result == "OK") {
                    $scope._property = data.property;
                    $scope.loadPhotosProp($scope._property);

                    var exists = false;
                    angular.forEach($scope.myProperties, function (property, key) {
                        if ($scope._property.ID == property.ID) {
                            exists = true;
                        }
                    });

                    if (!exists) {
                        $scope.myProperties.unshift($scope._property);
                    }
                }
            });
        };

        $scope.loadPhotos = function () {

            angular.forEach($scope.properties, function (property, key) {

                if (property.COVER) {
                    property.COVER = "/property/photo?type=th&key=" + property.COVER;
                }

                $scope.loadPhotosProp(property)


            });
        };

        $scope.loadPhotosProp = function (property) {
            $http({
                method: "GET",
                url: path + "/property/photos",
                params: {
                    property: property.ID
                }
            }).success(function (data) {

                if (data.result == "OK") {

                    property.photos = [];
                    property.photos360 = [];
                    property.ths = [];
                    var preWasL = false;

                    angular.forEach(data.keys, function (photo, key) {
                        if (photo.endsWith("_360")) {
                            property.photos360.push("/property/photo?key=" + photo);
                        }
                        if (photo.endsWith("_l")) {
                            property.photos.push("/property/photo?key=" + photo);
                            preWasL = true;
                        } else if (preWasL) {
                            property.ths.push("/property/photo?key=" + photo);
                            preWasL = false;
                        }
                    });
                    console.log(property.photos360)

                    if (property.COVER) {

                    } else {
                        property.COVER = property.ths[0];
                    }


                }

            });
        }
        // test test

        $scope.currentProperty = function (p) {
            console.log(JSON.stringify(p));
        };


        $scope.loadPhoto = function (photoKey, property) {
            $http({
                method: "GET",
                url: path + "/property/photo",
                params: {
                    key: photoKey
                }
            }).success(function (data) {

                if (data.result == "OK") {
                    property.photos.push(data.url);
                }

            });
        };

        // INITIALIZE
        loadBanners();
        $scope.loadCities();
        $scope.loadPropertyTypes();
        $scope.loadPricing();
        $scope.loadPropertyAreas();
        if ($location.search().propId && $location.search().webView) {
            $scope.webView = true;
        } else {
            $scope.showIntro();
        }
        if ($location.search().propId) {
            $scope.getProperty($location.search().propId);
        } else {
            $scope._property = null;
            $scope.preSearch();
        };
        $rootScope.$on('$locationChangeSuccess', function (event) {
            if (!$location.search().propId) {
                if (!$scope.properties) { $scope.preSearch(); };
            }
        })
    })
    .directive('endRepeat', ['$timeout', function ($timeout) {
        return {
            restrict: 'A',
            link: function (scope, element, attr) {
                if (scope.$last === true) {
                    $timeout(function () {
                        scope.$emit('ngRepeatFinished');
                    });
                }
            }
        }
    }])
    .directive('render360', function ($timeout) {
        return {
            scope: true,
            link: function (scope, element, attrs) {

                console.log('hola')
                if (scope.$last) {
                    $('.test-popup').magnificPopup({
                        type: 'image',
                        mainClass: 'myPano',
                        callbacks: {
                            open: function () {
                                console.log($(".mfp-img"));
                                var img = $(".mfp-img").attr('src');

                                $(".js-myPano").pano({
                                    img: img
                                });
                                $(".mfp-img").css('display', 'none');

                                // Will fire when this exact popup is opened
                                // this - is Magnific Popup object
                            }
                        },
                        image: {
                            markup: '<div class="mfp-figure">' +
                            '<div class="mfp-close"></div>' +
                            '<photosphere src="https://www.arrendamientosayura.com/property/photo?key=properties/27602/275a0527-1e7a-4f5a-a87a-ffce932a5394_l"></photosphere>' +
                            '<div style="display:none;" class="mfp-img"></div>' +
                            '<div class="mfp-bottom-bar">' +
                            '<div class="mfp-title"></div>' +
                            '<div class="mfp-counter"></div>' +
                            '</div>' +
                            '</div>', // Popup HTML markup. `.mfp-img` div will be replaced with img tag, `.mfp-close` by close button
                            titleSrc: 'title', // Attribute of the target element that contains caption for the slide.
                            // Or the function that should return the title. For example:
                            // titleSrc: function(item) {
                            //   return item.el.attr('title') + '<small>by Marsel Van Oosten</small>';
                            // }

                            verticalFit: true, // Fits image in area vertically

                            tError: '<a href="%url%">The image</a> could not be loaded.' // Error message
                        }
                    });
                }
            }
        }
    });

