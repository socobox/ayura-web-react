SELECT p.*, n.NAME AS "NEIGHBORHOOD", c.ID AS "CITY_ID", c.NAME AS "CITY" FROM properties `p`
  LEFT JOIN neighborhoods `n` ON (n.ID=p.ID_NEIGHBORHOOD)
  LEFT JOIN cities `c` ON (c.ID=n.ID_CITY)
  LEFT JOIN property_features `pparking` ON (pparking.ID_PROPERTY=p.ID)
  LEFT JOIN property_features `pparking2` ON (pparking2.ID_PROPERTY=p.ID)
WHERE (p.STATUS= ?) AND (pparking.ID_FEATURE=43) AND (pparking2.ID_FEATURE=44)
      AND ((pparking.VALUE= ? OR pparking.VALUE is NULL OR pparking2.VALUE= ? OR pparking2.VALUE is NULL)) AND (p.ID_TYPE= ?) AND (p.SERVICE_TYPE=?) AND (p.PRICE >= ? ) AND (p.PRICE<= ? ) AND (p.AREA >= ?) AND (p.AREA<= ?) AND (c.id=?) ORDER BY LAST_AVAILABLE DESC LIMIT 10


:["0","false","false","1","0","500001","1000000","51","100","1"]